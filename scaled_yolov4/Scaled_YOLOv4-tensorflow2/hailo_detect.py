# This file hailo_detect.py was derived from scaled-Yolov4's detect.py and must be placed at the same level as detect.py to
# function correctly.
# It was modified to write out images with detected objects as jpeg files and for models in Tensorflow, ONNX and Hailo.
# Giving an easy way to validate the results through visual inspection. For a same model, the Tensorflow and ONNX versions
# should produce the same results. The Hailo version will produce different results due to quantization and this script
# can be used to see any accuracy loss.
#
# The changes are:
#
# 1) In addition to the original TF inference, added ONNX and Hailo Hef inference. Each of the inference can be selectively
#    enabled by passing the right parameters:
#          - TF inference: passing of parameter model-path
#          - ONNX inference: passing of parameter orig-onnx-path
#          - HEF (Hailo) inference: passing of parameter hef-path of the model running on Hailo-8, post-process-onnx-path (because post-processing is done on the host by ONNX runtime)
#   These parameters are mutually inclusive meaning it is possible to enable inference of multiple framework models if
#   the corresponding parameters are passed.
#
# 2) Replaced output image display with file saving. The destination directory must be provided by parameters
#    tf-output-dir, onnx-output-dir, hef-output-dir, depending on which inference was selected
#    By default the script will apply the model on every jpeg images found in the directory. But it is possible to limit
#    the number of images by passing the parameter max-num-pics.
#
# 3) Parameter class-names parameter's default value changed to dataset/license_plate.names, which include this single line:
#    lic_plate. This is to change the drawn label from pothole to lic_plate.
#
# Example of command line usage:
#
# For TF inference and max 10 pics: python hailo_detect.py --pic-dir ../lp_test_data --max-num-pics 10 --model-path ../lp_detector_model/model  --tf-output-dir ./tf_output
# For ONNX inference and max 10 pics: python hailo_detect.py --pic-dir ../lp_test_data --max-num-pics 10  --orig-onnx-path ../model.sim.onnx --onnx-output-dir ./onnx_output
# For HEF inference and max 10 pics: python hailo_detect.py --pic-dir ../lp_test_data --max-num-pics 10 --hef-path ../lp_detector.hef --post-process-onnx-path ../lp_detector_post.onnx   --hef-output-dir ./hef_output

import tensorflow as tf
import os
import cv2
import numpy as np
import argparse
from model.nms import yolov4_nms,NonMaxSuppression
from utils.preprocess import resize_img
import random
import albumentations as A

import onnxruntime
from hailo_platform import __version__
from multiprocessing import Process, Queue, Event, Lock, Value
from hailo_platform import (HEF, PcieDevice, HailoStreamInterface, ConfigureParams, InferVStreams,
 InputVStreamParams, OutputVStreamParams, InputVStreams, OutputVStreams, FormatType)


def parse_args(args):
    parser = argparse.ArgumentParser("test model")
    parser.add_argument('--class-names', default='dataset/license_plate.names')
    parser.add_argument('--pic-dir')
    parser.add_argument('--model-path')

    parser.add_argument('--img-size', default=(416, 416))
    parser.add_argument('--TTA', default=True)
    parser.add_argument('--nms', default='diou_nms', help="choices=['hard_nms','diou_nms']")
    parser.add_argument('--nms-max-box-num', default=300,type=int)
    parser.add_argument('--nms-iou-threshold', default=0.2,type=float)
    parser.add_argument('--nms-score-threshold', default=0.1,type=float)

    parser.add_argument('--max-num-pics', default= 0, type= int, help="Maximum number of pictures to process, default value of 0 means all pictures in the pic-dir are processed")
    parser.add_argument('--hef-path', help="path of the HEF file")
    parser.add_argument('--post-process-onnx-path', help="path of ONNX postprocessing file, when the output of the HEF is the input of the ONNX")
    parser.add_argument('--orig-onnx-path', help="path of the original ONNX file")
    parser.add_argument('--tf-output-dir', help="path to the directory where output images from tensorflow are stored")
    parser.add_argument('--onnx-output-dir', help="path to the directory where output images from onnx are stored")
    parser.add_argument('--hef-output-dir', help="path to the directory where output images from HEF are stored")

    return parser.parse_args(args)


# ---------------- Inferences threads functions -------------- #
def infer_model(network_group, network_group_params, hef, post_process_onnx, read_q, write_q, isRunningFlag, startEvent):

    # Create input and output virtual streams params
    # Quantized argument signifies whether or not the incoming data is already quantized.
    # Data is quantized by HailoRT if and only if quantized == False .
    input_vstreams_params = InputVStreamParams.make(network_group, quantized=True, format_type=FormatType.UINT8)
    output_vstreams_params = OutputVStreamParams.make(network_group, quantized=False, format_type=FormatType.FLOAT32)
    # Define dataset params
    input_vstream_info = hef.get_input_vstream_infos()[0]

    startEvent.wait()

    network_group.wait_for_activation(100)

    with InferVStreams(network_group, input_vstreams_params, output_vstreams_params) as infer_pipeline:
        while (True):
            if (read_q.empty() == False):
                image = read_q.get(0)
                data = np.expand_dims(image, axis=0)
                input_data = {input_vstream_info.name: data}
                infer_results = infer_pipeline.infer(input_data)
                values=[]
                for index in range(2):
                     data = infer_results[hef.get_output_vstream_infos()[index].name]
                     #print("******** Infer Image received")
                     switched_tensor = np.swapaxes(np.swapaxes(data, 1, 3), 2, 3)  # 1 <--> 2, then 2 <--> 3
                     values.append(switched_tensor)

                values.sort(key=lambda x: x.shape)
                inp = post_process_onnx.get_inputs()
                inp.sort(key=lambda x: x.shape)
                curr_vstream_data_dict = {i.name: v for i, v in zip(inp, values)}
                write_q.put(curr_vstream_data_dict)
                #print("******** Infer output sent to queue")
            with isRunningFlag.get_lock():
                if isRunningFlag.value == False:
                    #print("Running flag is false")
                    break

    #print("infer thread exited")

# The send and recv functions below are commented because they are used in streaming mode
# and we are unable to use them without causing HAILORT timeout errors because not enough frames
# are fed into the queue.

# def send(configured_network, read_q, isRunningFlag, startEvent):
#
#     startEvent.wait()
#
#     vstreams_params = InputVStreamParams.make_from_network_group(configured_network, quantized=True,
#                                                                  format_type=FormatType.UINT8, timeout_ms=10000)
#     #print('Performing inference on input images...\n')
#     configured_network.wait_for_activation(100)
#
#     with InputVStreams(configured_network, vstreams_params) as vstreams:
#         vstream_to_buffer = {vstream: np.ndarray([1] + list(vstream.shape), dtype=vstream.dtype) for vstream in
#                              vstreams}
#
#         while (True):
#             if (read_q.empty() == False):
#                 #print("******** Send Image received")
#                 image = read_q.get(0)
#                 for vstream, _ in vstream_to_buffer.items():
#                     data = np.expand_dims(image, axis=0)
#                     vstream.send(data)
#                     #print("******** Send Image sent")
#
#             with isRunningFlag.get_lock():
#                 if isRunningFlag.value == False:
#                     print("Running flag is false")
#                     break
#
#     #print("Send thread exited")
#
#
# def recv(configured_network, write_q, post_process_onnx, isRunningFlag, startEvent):
#
#     startEvent.wait()
#
#     vstreams_params = OutputVStreamParams.make_from_network_group(configured_network, quantized=False,
#                                                                   format_type=FormatType.FLOAT32, timeout_ms=10000)
#     configured_network.wait_for_activation(100)
#     with OutputVStreams(configured_network, vstreams_params) as vstreams:
#         while True:
#             #print("******** In Recv busy wait")
#             curr_vstream_data_dict = {}
#             values = []
#             for vstream in vstreams:
#                 data = vstream.recv()
#                 #print("******** Recv Image received")
#                 switched_tensor = np.swapaxes(np.swapaxes(np.expand_dims(data, axis=0), 1, 3), 2,
#                                               3)  # 1 <--> 2, then 2 <--> 3
#                 values.append(switched_tensor)
#             values.sort(key=lambda x: x.shape)
#             inp = post_process_onnx.get_inputs()
#             inp.sort(key=lambda x: x.shape)
#             curr_vstream_data_dict = {i.name: v for i, v in zip(inp, values)}
#             write_q.put(curr_vstream_data_dict)
#             #print("******** Recv output sent to queue")
#
#             with isRunningFlag.get_lock():
#                 if isRunningFlag.value == False:
#                     print("Running flag is false")
#                     break
#
#     #print("Rcv thread exited")


def do_post_process_onnx(read_q, write_q, post_process_onnx, isRunningFlag, startEvent):

    startEvent.wait()

    #print("********* do_post_process_onnx started")
    while True:
        if (read_q.empty() == False):
            inference_dict = read_q.get(0)
            #print("******** do_post_process_onnx Image received")
            inference_output = post_process_onnx.run(None, inference_dict)
            write_q.put(inference_output)
            #print("********* do_post_process_onnx output sent to queue")
        with isRunningFlag.get_lock():
            if isRunningFlag.value == False:
                #print("Running flag is false")
                break

    #print("Post_process thread exited")

def detect_batch_img(img,model,args, post_process_onnx=[], imageQueue=[], outQueue=[]):

    if "tensorflow.python.saved_model" in str(type(model)):
        img = img / 255
        img = tf.image.convert_image_dtype(img, tf.float32)
        pre_nms_decoded_boxes, pre_nms__scores = model(img, training=False)
        pre_nms_decoded_boxes = pre_nms_decoded_boxes.numpy()
        pre_nms__scores = pre_nms__scores.numpy()
        if (0):
            print(type(pre_nms_decoded_boxes))
            print(pre_nms_decoded_boxes.dtype)
            print(pre_nms_decoded_boxes.shape)
    elif "HEF" not in str(type(model)):
        #print("Processing the onnx model")
        #print(str(type(model)))
        numImages= len(img)
        pre_nms_decoded_boxes= []
        pre_nms__scores= []
        # Below processing is for onnx model
        img= img.astype(np.float32)/255.0
        inp = model.get_inputs()
        inp.sort(key=lambda x: x.shape)

        for idx in range(numImages):
            data_dict = {n.name: v for n, v in zip(inp, np.expand_dims(np.expand_dims(img[idx], axis=0), axis=0).astype(np.float32))}
            inference_output = model.run(None, data_dict)
            pre_nms_decoded_boxes.append(inference_output[0][0][:][:])
            pre_nms__scores.append(inference_output[1][0][:][:])
            if (0):
                print(inference_output)
                print(type(inference_output))
                print("Length of output list: " + str(len(inference_output)))
                print(type(inference_output[0]))
                print(inference_output[0].dtype)
                print(inference_output[0].shape)
                print(type(inference_output[1]))
                print(inference_output[1].dtype)
                print(inference_output[1].shape)

        pre_nms_decoded_boxes= np.array(pre_nms_decoded_boxes)
        pre_nms__scores= np.array(pre_nms__scores)

    elif post_process_onnx!=[]:
        #print("Processing the HEF model")
        #print(str(type(model)))
        numImages = len(img)
        pre_nms_decoded_boxes = []
        pre_nms__scores = []
        # Below processing is for onnx model

        for idx in range(numImages):
            data = np.expand_dims(np.expand_dims(img[idx], axis=0), axis=0).astype(np.uint8)
            imageQueue.put(data)
            #print("***** Input image sent from detect_batch_img")
        for idx in range(numImages):
            inference_output= outQueue.get()
            #print("***** Output received in detect_batch_img")
            pre_nms_decoded_boxes.append(inference_output[0][0][:][:])
            pre_nms__scores.append(inference_output[1][0][:][:])
            if (0):
                print(inference_output)
                print(type(inference_output))
                print("Length of output list: " + str(len(inference_output)))
                print(type(inference_output[0]))
                print(inference_output[0].dtype)
                print(inference_output[0].shape)
                print(type(inference_output[1]))
                print(inference_output[1].dtype)
                print(inference_output[1].shape)

        pre_nms_decoded_boxes = np.array(pre_nms_decoded_boxes)
        pre_nms__scores = np.array(pre_nms__scores)

    boxes, scores, classes, valid_detections = yolov4_nms(args)(pre_nms_decoded_boxes, pre_nms__scores, args)
    return boxes, scores, classes, valid_detections
    # pre_nms_decoded_boxes,pre_nms__scores = model(img)
    # pre_nms_decoded_boxes = pre_nms_decoded_boxes.numpy()
    # pre_nms__scores = pre_nms__scores.numpy()
    # return pre_nms_decoded_boxes, pre_nms__scores

def tta_nms(boxes,scores,classes,valid_detections,args):
    all_boxes = []
    all_scores = []
    all_classes = []
    batch_index = 0
    valid_boxes = boxes[batch_index][0:valid_detections[batch_index]]
    valid_boxes[:, (0, 2)] = (1.-valid_boxes[:,(2,0)])
    all_boxes.append(valid_boxes)
    all_scores.append(scores[batch_index][0:valid_detections[batch_index]])
    all_classes.append(classes[batch_index][0:valid_detections[batch_index]])
    for batch_index in range(1,boxes.shape[0]):
        all_boxes.append(boxes[batch_index][0:valid_detections[batch_index]])
        all_scores.append(scores[batch_index][0:valid_detections[batch_index]])
        all_classes.append(classes[batch_index][0:valid_detections[batch_index]])
    all_boxes = np.concatenate(all_boxes,axis=0)
    all_scores = np.concatenate(all_scores, axis=0)
    all_classes = np.concatenate(all_classes, axis=0)
    all_boxes,all_scores,all_classes = np.array(all_boxes), np.array(all_scores), np.array(all_classes)
    boxes, scores, classes, valid_detections = NonMaxSuppression.diou_nms_np_tta(np.expand_dims(all_boxes,0),np.expand_dims(all_scores,0),np.expand_dims(all_classes,0),args)
    boxes, scores, classes, valid_detections = np.squeeze(boxes), np.squeeze(scores), np.squeeze(classes), np.squeeze(valid_detections)
    return boxes[:valid_detections], scores[:valid_detections], classes[:valid_detections]


def plot_one_box(img, box, color=None, label=None, line_thickness=None):
    tl = line_thickness or round(0.002 * (img.shape[0] + img.shape[1]) / 2) + 1  # line/font thickness
    color = color or [random.randint(0, 255) for _ in range(3)]
    c1, c2 = (int(box[0]), int(box[1])), (int(box[2]), int(box[3]))
    cv2.rectangle(img, c1, c2, color, thickness=tl, lineType=cv2.LINE_AA)
    if label:
        tf = max(tl - 1, 1)  # font thickness
        t_size = cv2.getTextSize(label, 0, fontScale=tl / 7, thickness=tf)[0]
        c2 = c1[0] + t_size[0], c1[1] - t_size[1] - 3
        cv2.rectangle(img, c1, c2, color, -1, cv2.LINE_AA)  # filled
        cv2.putText(img, label, (c1[0], c1[1] - 2), 0, tl / 7, [225, 255, 255], thickness=tf, lineType=cv2.LINE_AA)

def plot_boxes(img,boxes,scores,classes,class_names,args):
    for i in range(len(boxes)):
            if scores[i] < args.nms_score_threshold:
                continue
            x1y1 = (boxes[i][0:2] * img.shape[0:2][::-1]).astype(np.int)
            x2y2 = (boxes[i][2:4] * img.shape[0:2][::-1]).astype(np.int)
            plot_one_box(img,[x1y1[0],x1y1[1],x2y2[0],x2y2[1]],(255,0,255),label=str(class_names[classes[i]]) + "," + str("%0.2f" % scores[i]))
def get_tta_tranform():
    out_list=[]
    tta_transform = A.Compose([
        A.HorizontalFlip(p=1.),
        A.RandomBrightnessContrast(p=0.2),
    ])
    out_list.append(tta_transform)
    tta_transform = A.Compose([
        A.RandomBrightnessContrast(p=0.2),
    ])
    out_list.append(tta_transform)
    return out_list

def mainProcess(args, hef, post_process_onnx, imageQueue, outQueue, isRunningFlag, startEvent):
    if (args.orig_onnx_path):
        orig_onnx = onnxruntime.InferenceSession(args.orig_onnx_path)

    # model = tf.keras.models.load_model(args.model_path)
    if (args.model_path):
        model = tf.saved_model.load(args.model_path)

    with open(args.class_names) as f:
        class_names = f.read().splitlines()
    img_list = os.listdir(args.pic_dir)

    if args.max_num_pics!=0 :
        img_list = img_list[0:min(args.max_num_pics, len(img_list))]

    for img_name in img_list:
        img = cv2.imread(os.path.join(args.pic_dir, img_name))
        img_ori, _, _ = resize_img(img, args.img_size)
        img_copy = img_ori.copy()
        img_copy_onnx = img_copy.copy()
        img_copy_hef = img_copy.copy()

        aug_imgs = []
        if args.TTA:
            tta_transforms = get_tta_tranform()
            aug_imgs.append(tta_transforms[0](image=img_copy)['image'])
            aug_imgs.append(tta_transforms[1](image=img_copy)['image'])
        aug_imgs.append(img_copy)
        batch_img = np.array(aug_imgs)
        if (args.model_path):
            # Execute the Keras model and save the output to file
            boxes, scores, classes, valid_detections = detect_batch_img(batch_img, model, args)
            boxes, scores, classes = tta_nms(boxes, scores, classes, valid_detections, args)
            plot_boxes(img_copy, boxes, scores, classes, class_names, args)
            filePath = os.path.join(args.tf_output_dir, img_name)
            cv2.imwrite(filePath, img_copy)
            print(">>>> Writing " + filePath)

        if (args.orig_onnx_path):
            # Execute the ONNX model and save the output to file
            boxes, scores, classes, valid_detections = detect_batch_img(batch_img, orig_onnx, args)
            boxes, scores, classes = tta_nms(boxes, scores, classes, valid_detections, args)
            plot_boxes(img_copy_onnx, boxes, scores, classes, class_names, args)
            filePath = os.path.join(args.onnx_output_dir, img_name)
            cv2.imwrite(filePath, img_copy_onnx)
            print(">>>> Writing " + filePath)

        if (args.hef_path) and (args.post_process_onnx_path):
            startEvent.set()
            # Execute the HEF model and save the output to file
            boxes, scores, classes, valid_detections = detect_batch_img(batch_img, hef, args, post_process_onnx,
                                                                        imageQueue=imageQueue, outQueue=outQueue)
            boxes, scores, classes = tta_nms(boxes, scores, classes, valid_detections, args)
            plot_boxes(img_copy_hef, boxes, scores, classes, class_names, args)
            filePath = os.path.join(args.hef_output_dir, img_name)
            cv2.imwrite(filePath, img_copy_hef)
            print(">>>> Writing " + filePath)

        # print("Next")
        # inference_output.sort(key=lambda x: x.shape)
        # curr_vstream_data_dict = {i.name: v for i, v in zip(inference_output, values)}

        # cv2.imshow("d", img_copy/255)
        # cv2.waitKey(0)
    with isRunningFlag.get_lock():
        isRunningFlag.value= False

    #print ("mainProcess exited")

def main(args):

    #pip install albumentations
    #pip install opencv-python==4.5.4.60

    isRunningFlag= Value('i', True)

    #if (not args.orig_onnx_path):
    #    raise ValueError(
    #        'You must define orig-onnx-path in the command line. Run with -h for additional info')

    #if (not args.tf_output_dir):
    #    raise ValueError(
    #        'You must define tf-output-dir in the command line. Run with -h for additional info')

    #if (not args.onnx_output_dir):
    #    raise ValueError(
    #        'You must define onnx-output-dir in the command line. Run with -h for additional info')
    if (args.tf_output_dir):
        if os.path.isdir(args.tf_output_dir)==False:
            os.mkdir(args.tf_output_dir)

    if (args.onnx_output_dir):
        if os.path.isdir(args.onnx_output_dir)==False:
            os.mkdir(args.onnx_output_dir)

    if (args.hef_path) and (args.post_process_onnx_path):

        if (not args.hef_output_dir):
            raise ValueError(
                'You must define hdef-output-dir in the command line. Run with -h for additional info')

        if os.path.isdir(args.hef_output_dir) == False:
            os.mkdir(args.hef_output_dir)

        post_process_onnx_path = args.post_process_onnx_path
        hef = HEF(args.hef_path)
        post_process_onnx = onnxruntime.InferenceSession(post_process_onnx_path)

        imageQueue = Queue()
        hefOutQueue = Queue()
        outQueue = Queue()

        startEvent= Event()

        main_process = Process(target=mainProcess, args=(args, hef, post_process_onnx, imageQueue, outQueue, isRunningFlag, startEvent))
        main_process.start()

        target= PcieDevice()
        configure_params = ConfigureParams.create_from_hef(hef, interface=HailoStreamInterface.PCIe)
        network_group = target.configure(hef, configure_params)[0]
        network_group_params = network_group.create_params()

        #send_process = Process(target=send, args=(network_group, imageQueue, isRunningFlag, startEvent))
        #recv_process = Process(target=recv, args=(network_group, hefOutQueue, post_process_onnx, isRunningFlag, startEvent))
        infer_process = Process(target=infer_model, args=(network_group, network_group_params, hef, post_process_onnx, imageQueue, hefOutQueue, isRunningFlag, startEvent))
        post_process = Process(target=do_post_process_onnx, args=(hefOutQueue, outQueue, post_process_onnx, isRunningFlag, startEvent))
        #send_process.start()
        #recv_process.start()
        infer_process.start()
        post_process.start()
        with network_group.activate(network_group_params):
            #recv_process.join()
            #send_process.join()
            infer_process.join()
            post_process.join()

    else:
        startEvent = Event()
        main_process = Process(target=mainProcess,args=(args, [], [], [], [], isRunningFlag, startEvent))
        main_process.start()

    main_process.join()

import sys
if __name__ == '__main__':
    args = parse_args(sys.argv[1:])
    main(args)
