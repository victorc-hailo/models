#!/usr/bin/env python3

##############################################################################
# This example is a generic Hailo + ONNXRuntime inference detection example.   
# In order for this example to work properly, please create the relevant 
# post-processing function. 
#
# Usage: 'scaled_yolov4_inference.py --hef lp_detector.hef --post-process-onnx lp_detector_post.onnx'
#
##############################################################################

from locale import normalize
import numpy as np
import onnxruntime
from hailo_platform import __version__
from multiprocessing import Process, Queue
from hailo_platform import (HEF, PcieDevice, HailoStreamInterface, ConfigureParams,
 InputVStreamParams, OutputVStreamParams, InputVStreams, OutputVStreams, FormatType)
from zenlog import log
import time
from PIL import Image, ImageDraw, ImageFont
import cv2
import os
import re
import argparse
from model.nms import yolov4_nms,NonMaxSuppression

parser = argparse.ArgumentParser(description='Running a Hailo + ONNXRUntime inference')
parser.add_argument('--hef', help="HEF file path")
parser.add_argument('--orig-onnx', help="path of the original ONNX file")
parser.add_argument('--post-process-onnx', help="path of ONNX postprocessing file, when the output of the HEF is the input of the ONNX")
parser.add_argument('--img-dir', help="Path to the image directory containing jpeg files")
parser.add_argument('--img-size', default=(416, 416))
args = parser.parse_args()


# ---------------- Post-processing functions ----------------- #

def post_processing(inference_output):
    print('Create your relevant post-processing functions')

# ------------------------------------------------------------ #

# ---------------- Inferences threads functions -------------- #

def send(configured_network, images_list, num_images):
    vstreams_params = InputVStreamParams.make_from_network_group(configured_network, quantized=True, format_type=FormatType.UINT8)
    configured_network.wait_for_activation(100)
    print('Performing inference on input images...\n')
    with InputVStreams(configured_network, vstreams_params) as vstreams:
        vstream_to_buffer = {vstream: np.ndarray([1] + list(vstream.shape), dtype=vstream.dtype) for vstream in vstreams}
        for i in range(num_images):           
            for vstream, _ in vstream_to_buffer.items():
                data = np.expand_dims(images_list[i], axis=0)
                vstream.send(data)

                
def recv(configured_network, write_q, num_images, post_process_onnx):
    vstreams_params = OutputVStreamParams.make_from_network_group(configured_network, quantized=False, format_type=FormatType.FLOAT32)
    configured_network.wait_for_activation(100)
    with OutputVStreams(configured_network, vstreams_params) as vstreams:
        for _ in range(num_images):
            curr_vstream_data_dict = {}
            values = []
            for vstream in vstreams:
                data = vstream.recv()
                switched_tensor = np.swapaxes(np.swapaxes(np.expand_dims(data, axis=0), 1, 3), 2, 3) # 1 <--> 2, then 2 <--> 3
                values.append(switched_tensor)
            values.sort(key=lambda x: x.shape)
            inp = post_process_onnx.get_inputs()
            inp.sort(key=lambda x: x.shape)
            curr_vstream_data_dict = {i.name : v for i, v in zip(inp, values)}
            write_q.put(curr_vstream_data_dict)
            
def do_nms(read_q, images, num_images, post_process_onnx):
    i = 0
    while (i < num_images):
        if(read_q.empty() == False):
            inference_dict = read_q.get(0)
            inference_output = post_process_onnx.run(None, inference_dict)
            image = images[i]
            post_processing(inference_output)
            i = i + 1

# ------------------------------------------------------------ #

# ---------------- Pre-processing functions ------------------ #

def resize_img(img,dst_size):
    img_wh = img.shape[0:2][::-1]
    dst_size = np.array(dst_size)
    scale = dst_size/img_wh
    min_scale = np.min(scale)
    img = cv2.resize(img, None, fx=min_scale, fy=min_scale)
    img_wh = img.shape[0:2][::-1]
    pad_size = dst_size - img_wh
    half_pad_size = pad_size//2
    img = np.pad(img,[(half_pad_size[1],pad_size[1]-half_pad_size[1]),(half_pad_size[0],pad_size[0]-half_pad_size[0]),(0,0)])
    return img, min_scale, pad_size

# ----------------------------------------------------------- #


# ---------------- Start of the example --------------------- #

if (not args.hef or not args.post_process_onnx):
    raise ValueError('You must define hef path and post processing onnx path in the command line. Run with -h for additional info')
orig_onnx_path= args.orig_onnx
post_process_onnx_path = args.post_process_onnx

orig_onnx= onnxruntime.InferenceSession(orig_onnx_path)
post_process_onnx = onnxruntime.InferenceSession(post_process_onnx_path)

img_list = os.listdir(args.img_dir)
img_list= img_list[0:1]
num_images= len(img_list)
images_float= np.zeros((num_images, args.img_size[0], args.img_size[1], 3), dtype=np.float32)
images = np.zeros((num_images, args.img_size[0], args.img_size[1], 3), dtype=np.uint8)

idx=0

for img_name in img_list:
    img = cv2.imread(os.path.join(args.img_dir, img_name))
    img_ori,_,_ = resize_img(img, args.img_size)
    images_float[idx,:,:,:] = img_ori/255
    images[idx,:,:,:] = img_ori.astype(np.uint8)
    #cv2.imshow("d", images_float[idx])
    #cv2.waitKey(0)
    idx=idx+1

# First Process all the image using original ONNX model
inp = orig_onnx.get_inputs()
inp.sort(key=lambda x: x.shape)

for idx in range(num_images):
    data_dict = {n.name: v for n, v in zip(inp, np.expand_dims(np.expand_dims(images_float[idx], axis=0), axis=0).astype(np.float32))}
    inference_output = orig_onnx.run(None, data_dict)

hef = HEF(args.hef)

with PcieDevice() as target:
        configure_params = ConfigureParams.create_from_hef(hef, interface=HailoStreamInterface.PCIe)
        network_group = target.configure(hef, configure_params)[0]
        network_group_params = network_group.create_params()
        queue = Queue()
                
        send_process = Process(target=send, args=(network_group, images, num_images))
        recv_process = Process(target=recv, args=(network_group, queue, num_images, post_process_onnx))
        nms_process = Process(target=do_nms, args=(queue, images, num_images, post_process_onnx)) 
        start_time = time.time()
        recv_process.start()
        send_process.start()
        nms_process.start()
        with network_group.activate(network_group_params):
            recv_process.join()
            send_process.join()
            nms_process.join()

        end_time = time.time()
print('Inference was successful!\n')
log.info('-------------------------------------')
log.info(' Infer Time:      {:.3f} sec'.format(end_time - start_time))
log.info(' Average FPS:     {:.3f}'.format(num_images/(end_time - start_time)))
log.info('-------------------------------------')



