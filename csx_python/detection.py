import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GLib
import os
import argparse
import multiprocessing
import numpy as np
# import setproctitle
# import cv2
import time

# Try to import hailo python module
try:
    import hailo
except ImportError:
    exit("Failed to import hailo python module. Make sure you are in hailo virtual environment.")

# -----------------------------------------------------------------------------------------------
# User defined class to be used in the callback function
# -----------------------------------------------------------------------------------------------
# a sample class to be used in the callback function alowwing to count the number of frames
class app_callback_class:
    def __init__(self):
        self.frame_count = 0
        self.running = True

    def increment(self):
        self.frame_count += 1

    def get_count(self):
        return self.frame_count 

# Create an instance of the class
user_data = app_callback_class()

# -----------------------------------------------------------------------------------------------
# User defined callback function
# -----------------------------------------------------------------------------------------------

# This is the callback function that will be called when data is available from the pipeline

def app_callback(pad, info, user_data):
    # Get the GstBuffer from the probe info
    buffer = info.get_buffer()
    # Check if the buffer is valid
    if buffer is None:
        return Gst.PadProbeReturn.OK
        
    # using the user_data to count the number of frames
    user_data.increment()
    string_to_print = f"Frame count: {user_data.get_count()}\n"
    
    # get the detections from the buffer
    roi = hailo.get_roi_from_buffer(buffer)
    detections = roi.get_objects_typed(hailo.HAILO_DETECTION)
    
    # parse the detections
    for detection in detections:
        label = detection.get_label()
        bbox = detection.get_bbox()
        confidence = detection.get_confidence()
        string_to_print += (f"Detection: {label} {confidence:.2f}\n")

    print(string_to_print)
    return Gst.PadProbeReturn.OK
    # Additional option is Gst.PadProbeReturn.DROP to drop the buffer not passing in to the rest of the pipeline
    # See more options in Gstreamer documentation

# -----------------------------------------------------------------------------------------------

# Default parameters

network_width = 640
network_height = 640
network_format = "RGB"
video_sink = "xvimagesink"
batch_size = 4

# these NMS values are used for both networks, change or seperate if required
nms_score_threshold=0.3 
nms_iou_threshold=0.45
thresholds_str=f"nms-score-threshold={nms_score_threshold} nms-iou-threshold={nms_iou_threshold} output-format-type=HAILO_FORMAT_TYPE_FLOAT32"

# Theses are used for debug to disable one of the networks
DISABLE_YOLOV8L = "false"
DISABLE_YOLOV8N = "false"
DISABLE_TRACKER = "false"
DISABLE_DISPLAY = "false"

def parse_arguments():
    parser = argparse.ArgumentParser(description="Detection App")
    parser.add_argument("--input", "-i", type=str, default="/dev/video0", help="Input source. Can be a file, USB camera \
                        Defaults to /dev/video0")
    parser.add_argument("--show-fps", "-f", action="store_true", help="Print FPS on sink")
    parser.add_argument("--dump-dot", action="store_true", help="Dump the pipeline graph to a dot file pipeline.dot")
    return parser.parse_args()

def QUEUE(name, max_size_buffers=3, max_size_bytes=0, max_size_time=0):
    return f"queue name={name} max-size-buffers={max_size_buffers} max-size-bytes={max_size_bytes} max-size-time={max_size_time} ! "

def get_source_type(input_source):
    # This function will return the source type based on the input source
    # return values can be "file", "usb"
    if input_source.startswith("/dev/video"):
        # check if the device is available
        return 'usb'
    elif "mp4" in input_source:
        return 'file'
    else:
    	return 'rtsp'
  

class GStreamerApp:
    def __init__(self, args):
        
        # Create an empty options menu
        self.options_menu = args
        
        # Initialize variables
        tappas_workspace = os.environ.get('TAPPAS_WORKSPACE', '')
        if tappas_workspace == '':
            print("TAPPAS_WORKSPACE environment variable is not set. Please set it to the path of the TAPPAS workspace.")
            exit(1)
        self.current_path = os.path.dirname(os.path.abspath(__file__))
        self.postprocess_dir = os.path.join(tappas_workspace, 'apps/h8/gstreamer/libs/post_processes')
        self.default_postprocess_so = os.path.join(self.postprocess_dir, 'libyolo_hailortpp_post.so')
        self.video_source = self.options_menu.input
        self.source_type = get_source_type(self.video_source)
        
        self.default_network_name = "yolov8"
        self.hef_path = os.path.join(self.current_path, './resources/yolov8l.hef')
        self.json_config_path = os.path.join(self.current_path, './resources/yolov8l_labels.json') # not used by hailortpp (bug)
        self.hef_path2 = os.path.join(self.current_path, './resources/yolov8n-gate-arm_bar.hef')
        self.json_config_path2 = os.path.join(self.current_path, './resources/yolov8n-gate-arm_bar_labels.json') # not used by hailortpp (bug) added labels map to postprocess
        
        # Set user data parameters
        self.sync = "false"
        
        if (self.options_menu.dump_dot):
            os.environ["GST_DEBUG_DUMP_DOT_DIR"] = self.current_path
        
        # Initialize GStreamer
        
        Gst.init(None)
        
        # Create a GStreamer pipeline 
        self.pipeline = self.create_pipeline()
        
        # connect to hailo_display fps-measurements
        if (self.options_menu.show_fps) and (DISABLE_DISPLAY == "false"):
            print("Showing FPS")
            self.pipeline.get_by_name("hailo_display").connect("fps-measurements", self.on_fps_measurement)

        # Create a GLib Main Loop
        self.loop = GLib.MainLoop()
    
    def on_fps_measurement(self, sink, fps, droprate, avgfps):
        print(f"FPS: {fps:.2f}, Droprate: {droprate:.2f}, Avg FPS: {avgfps:.2f}")
        return True

    def create_pipeline(self):
        pipeline_string = self.get_pipeline_string()
        try:
            pipeline = Gst.parse_launch(pipeline_string)
        except Exception as e:
            print(e)
            print(pipeline_string)
            exit(1)
        return pipeline

    def bus_call(self, bus, message, loop):
        t = message.type
        if t == Gst.MessageType.EOS:
            print("End-of-stream")
            loop.quit()
        elif t == Gst.MessageType.ERROR:
            err, debug = message.parse_error()
            print(f"Error: {err}, {debug}")
            loop.quit()
        return True
    
    
    def get_pipeline_string(self):
        if (self.source_type == "usb"):
            source_element = f"v4l2src device={self.video_source} name=src_0 ! "
            source_element += f"image/jpeg, width=1280, height=720, framerate=30/1 ! "
            source_element += f"decodebin ! "
        elif (self.source_type == "rtsp"):
            #source_element = f"udpsrc port=5000 address={self.video_source} ! "
            source_element = f"rtspsrc user-id=admin user-pw=admin location=rtsp://166.168.225.126/live1s1.sdp ! "
            source_element += f"application/x-rtp,encoding-name=H264 ! "
            source_element += QUEUE("queue_rtph264depay")
            source_element += f"rtph264depay ! "
            source_element += QUEUE("queue_h264parse") 
            source_element += f"h264parse ! "
            source_element += QUEUE("queue_vaapih264dec")
            source_element += f"vaapih264dec ! "
            source_element += QUEUE("queue_dec_convert")
            source_element += f"videoconvert ! "
        else:  
            source_element = f"filesrc location={self.video_source} name=src_0 ! "
            source_element += QUEUE("queue_dec264")
            source_element += f" qtdemux ! h264parse ! avdec_h264 max-threads=2 ! "
            source_element += f" video/x-raw,format=I420 ! "
        source_element += QUEUE("queue_scale")
        source_element += f" videoscale n-threads=2 ! "
        source_element += QUEUE("queue_src_convert")
        source_element += f" videoconvert name=src_convert ! "
        # source_element += f"video/x-raw, format={network_format}, pixel-aspect-ratio=1/1 ! "
        source_element += f"video/x-raw, format={network_format}, width={network_width}, height={network_height}, pixel-aspect-ratio=1/1 ! "
        
        
        pipeline_string = "hailomuxer name=hmux "
        pipeline_string += source_element
        pipeline_string += "tee name=t !"
        # yolov8n-gate-arm_bar
        pipeline_string += QUEUE("queue_hailonet2")
        pipeline_string += f"hailonet pass-through={DISABLE_YOLOV8N} vdevice-key=1 hef-path={self.hef_path2} batch-size={batch_size} {thresholds_str} force-writable=true ! "
        pipeline_string += QUEUE("queue_hailofilter2")
        pipeline_string += f"hailofilter so-path={self.default_postprocess_so} config-path={self.json_config_path2} qos=false ! "
        if DISABLE_TRACKER == "false":
            pipeline_string += QUEUE("queue_tracker2")
            pipeline_string += f"hailopython qos=false module=tracker.py function=gate_arm_bar_track ! "
        pipeline_string += QUEUE("queue_hmux2") + " hmux.sink_0 "
        # yolov8l
        pipeline_string += "t. ! " + QUEUE("queue_hailonet")
        pipeline_string += f"hailonet pass-through={DISABLE_YOLOV8L} vdevice-key=1 hef-path={self.hef_path} batch-size={batch_size} {thresholds_str} force-writable=true ! "
        pipeline_string += QUEUE("queue_hailofilter")
        pipeline_string += f"hailofilter so-path={self.default_postprocess_so} config-path={self.json_config_path} qos=false ! "
        if DISABLE_TRACKER == "false":
            pipeline_string += QUEUE("queue_tracker1")
            pipeline_string += f"hailopython qos=false module=tracker.py function=coco_track ! "
        pipeline_string += QUEUE("queue_hmux") + " hmux.sink_1 "
        pipeline_string += "hmux. ! " + QUEUE("queue_hailo_python")
        
        if DISABLE_TRACKER == "false":
            pipeline_string += f"hailopython qos=false module=tracker.py function=analyze_tracks ! "
        else:
            pipeline_string += QUEUE("queue_user_callback")
            pipeline_string += f"identity name=identity_callback ! "

        pipeline_string += QUEUE("queue_zmq")
        pipeline_string += f"hailoexportzmq dump_image=true ! "
        pipeline_string += QUEUE("queue_hailooverlay")
        pipeline_string += f"hailooverlay ! "
        pipeline_string += QUEUE("queue_videoconvert")
        pipeline_string += f"videoconvert n-threads=3 ! "
        pipeline_string += QUEUE("queue_hailo_display")
        if DISABLE_DISPLAY == "false":
            pipeline_string += f"fpsdisplaysink video-sink={video_sink} name=hailo_display sync={self.sync} text-overlay={self.options_menu.show_fps} signal-fps-measurements=true "
        else:
            pipeline_string += f"fakesink sync={self.sync} "
        print(pipeline_string)
        return pipeline_string
    
    def dump_dot_file(self):
        print("Dumping dot file...")
        Gst.debug_bin_to_dot_file(self.pipeline, Gst.DebugGraphDetails.ALL, "pipeline")
        return False
    
    def run(self):

        # Add a watch for messages on the pipeline's bus
        bus = self.pipeline.get_bus()
        bus.add_signal_watch()
        bus.connect("message", self.bus_call, self.loop)

        # Connect pad probe to the identity element
        if DISABLE_TRACKER == "true":
            identity = self.pipeline.get_by_name("identity_callback")
            identity_pad = identity.get_static_pad("src")
            identity_pad.add_probe(Gst.PadProbeType.BUFFER, app_callback, user_data)
        
        # # get xvimagesink element and disable qos
        # # xvimagesink is instantiated by fpsdisplaysink
        # hailo_display = self.pipeline.get_by_name("hailo_display")
        # xvimagesink = hailo_display.get_by_name("xvimagesink0")
        # xvimagesink.set_property("qos", False)
        
        # Set pipeline to PLAYING state
        self.pipeline.set_state(Gst.State.PLAYING)
        
        # dump dot file
        if (self.options_menu.dump_dot):
            GLib.timeout_add_seconds(3, self.dump_dot_file)
        
        # Run the GLib event loop
        try:
            self.loop.run()
        except:
            pass

        # Clean up
        self.pipeline.set_state(Gst.State.NULL)

# Example usage
if __name__ == "__main__":
    args = parse_arguments()
    app = GStreamerApp(args)
    app.run()

