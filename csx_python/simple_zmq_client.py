import sys
import zmq
import time
import json
import numpy as np

def extract_detections(hailo_output, threshold=0.0, extract_track_id=True):

    """Extract detections from the HailoRT-postprocess output."""

    xyxy = []
    confidence = []
    class_id = []
    label= []
    if extract_track_id:
        track_id = []
    num_detections = 0

    for detection in hailo_output:

        hailo_detection= detection['HailoDetection']
        score = hailo_detection['confidence']

        if score < threshold:
            continue

        xmin= hailo_detection['HailoBBox']['xmin']
        ymin= hailo_detection['HailoBBox']['ymin']
        xmax= hailo_detection['HailoBBox']['xmin'] + hailo_detection['HailoBBox']['width']
        ymax= hailo_detection['HailoBBox']['ymin'] + hailo_detection['HailoBBox']['height']
        bbox = [xmin, ymin, xmax, ymax]        
        xyxy.append(bbox)
        confidence.append(score)
        class_id.append(hailo_detection['class_id'])
        label.append(hailo_detection['label'])
        num_detections = num_detections + 1

        if extract_track_id:
            track_id_dict=hailo_detection['SubObjects']
            if len(track_id_dict) > 0:
                track_id.append(track_id_dict[0]['HailoUniqueID']['unique_id'])

    returnedObject = {'xyxy':       np.array(xyxy),
    'confidence': np.array(confidence),
    'class_id':   np.array(class_id),
    'label': label,
    'num_detections': num_detections}

    if extract_track_id:
        returnedObject['tracker_id'] = np.array(track_id)
        
    return returnedObject

port = "5555"
if len(sys.argv) > 1:
    port =  sys.argv[1]
    int(port)
    
# Socket to talk to server
context = zmq.Context()
socket = context.socket(zmq.SUB)
socket.setsockopt_string(zmq.SUBSCRIBE, "")

print ("Collecting updates from hailoexportzm...")

# Use try and except to handle any error
try:
    socket.connect (f"tcp://localhost:{port}")  
except Exception as e:
    print (e)
    print ("Error connecting to the server")
    exit()

print(f"Connection successful to port {port}")

while True:
    hailo_meta_dict = json.loads(socket.recv())

    frame_idx= hailo_meta_dict['buffer_offset']
    detections= hailo_meta_dict['HailoROI']['SubObjects']

    tracked_detections= extract_detections(detections)

    if tracked_detections['num_detections']>0:
        for idx in range(tracked_detections['num_detections']):
            class_id= tracked_detections['class_id'][idx]
            print("Frame #", frame_idx, "Detection: ", tracked_detections['label'][idx], "(", class_id, ")"," with confidence: ", tracked_detections['confidence'][idx], " tracker id: ", tracked_detections['tracker_id'][idx],  " and bbox:", tracked_detections['xyxy'][idx])

