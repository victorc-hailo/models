import hailo
from gsthailo import VideoFrame
from gi.repository import Gst
import supervision as sv
import numpy as np
import cv2

def extract_detections(video_frame, hailo_output, h, w, threshold=0.0, extract_track_id=False):

    """Extract detections from the HailoRT-postprocess output."""

    xyxy = []
    confidence = []
    class_id = []
    if extract_track_id:
        track_id = []
    num_detections = 0

    for detection in hailo_output:

        if extract_track_id== False:
            video_frame.roi.remove_object(detection)
        score = detection.get_confidence()

        if score < threshold:
            continue

        bbox = [detection.get_bbox().xmin(), detection.get_bbox().ymin(), detection.get_bbox().xmax(), detection.get_bbox().ymax()]        
        xyxy.append(bbox)
        confidence.append(score)
        class_id.append(detection.get_class_id())
        num_detections = num_detections + 1

        if extract_track_id:
            track_id_array= detection.get_objects_typed(hailo.HAILO_UNIQUE_ID)
            track_id.append(track_id_array[0].get_id())

    returnedObject = {'xyxy':       np.array(xyxy),
    'confidence': np.array(confidence),
    'class_id':   np.array(class_id),
    'num_detections': num_detections}

    if extract_track_id:
        returnedObject['tracker_id'] = np.array(track_id)
        
    return returnedObject

with open('resources/yolov8_labels.json','r') as f:
    coco_labels = eval(f.read())

with open('resources/yolov8n-gate-arm_bar_labels.json','r') as f:
    gate_arm_bar_labels = eval(f.read())

GATE_ARM_BAR_OFFSET_TO_ADD_TO_CLASSID= 1000

def track(video_frame: VideoFrame, labels=coco_labels, offet_to_add_to_classid=0):

    if not hasattr(track, "frame_idx"):
        track.frame_idx = 0  # it doesn't exist yet, so initialize it

    if not hasattr(track, "tracker"):
        track.tracker = sv.ByteTrack()  # it doesn't exist yet, so initialize it
        #print("Tracker initialized in frame #", track.frame_idx)

    if offet_to_add_to_classid== GATE_ARM_BAR_OFFSET_TO_ADD_TO_CLASSID:
        if not hasattr(track, "tracker_arm_bar"):
            track.tracker_arm_bar = sv.ByteTrack()  # it doesn't exist yet, so initialize it
            #print("Tracker arm bar initialized in frame #", track.frame_idx)

    detections = hailo.get_hailo_detections(video_frame.roi)
        
    width, height = video_frame.video_info.width, video_frame.video_info.height
    detections_to_track = extract_detections(video_frame, detections, height, width)

    if detections_to_track['num_detections']>0:
        sv_detections_orig = sv.Detections(xyxy=detections_to_track['xyxy'],
                                    confidence=detections_to_track['confidence'],
                                    class_id=detections_to_track['class_id'])    

        if offet_to_add_to_classid== GATE_ARM_BAR_OFFSET_TO_ADD_TO_CLASSID:
            sv_detections = track.tracker_arm_bar.update_with_detections(sv_detections_orig)
        else:
            sv_detections = track.tracker.update_with_detections(sv_detections_orig)

        trackedDet=[]

        for idx in range(len(sv_detections.confidence)):

            bbox = hailo.HailoBBox(sv_detections.xyxy[idx][0], sv_detections.xyxy[idx][1], sv_detections.xyxy[idx][2]-sv_detections.xyxy[idx][0], sv_detections.xyxy[idx][3]-sv_detections.xyxy[idx][1])

            if sv_detections.confidence[idx] > 1.0:
                sv_detections.confidence[idx] /= 100

            #if offet_to_add_to_classid== GATE_ARM_BAR_OFFSET_TO_ADD_TO_CLASSID:
            #    if sv_detections.class_id[idx]>3:
            #        print(f"Frame #{int(track.frame_idx/2)}!!error in class id ={sv_detections.class_id[idx]}, {sv_detections.xyxy[idx]}, {sv_detections.confidence[idx]}!!!!!!!!!!!!!!!")
            #        print(f"             error in class id ={sv_detections_orig.class_id[idx]}, {sv_detections_orig.xyxy[idx]}, {sv_detections_orig.confidence[idx]}")

            label_name= labels[str(sv_detections.class_id[idx])]
            detection= hailo.HailoDetection(bbox= bbox, label= label_name, confidence= sv_detections.confidence[idx], index= sv_detections.class_id[idx] + offet_to_add_to_classid)
            trackerId= hailo.HailoUniqueID(sv_detections.tracker_id[idx])
            detection.add_object(trackerId)
            trackedDet.append(detection)
                
            hailo.add_detections(video_frame.roi, trackedDet)

    track.frame_idx+=1
    
    return Gst.FlowReturn.OK

def coco_track(video_frame: VideoFrame):
    return track(video_frame, labels=coco_labels)

def gate_arm_bar_track(video_frame: VideoFrame):
    return track(video_frame, labels=gate_arm_bar_labels, offet_to_add_to_classid=GATE_ARM_BAR_OFFSET_TO_ADD_TO_CLASSID)

frame_idx=0 

def analyze_tracks(video_frame: VideoFrame):

    if not hasattr(analyze_tracks, "frame_idx"):
     analyze_tracks.frame_idx = 0  # it doesn't exist yet, so initialize it

    detections = hailo.get_hailo_detections(video_frame.roi)
        
    width, height = video_frame.video_info.width, video_frame.video_info.height
    
    tracked_detections = extract_detections(video_frame, detections, height, width, threshold=0.0, extract_track_id=True)

    gate_detections={'xyxy':[], 'confidence':[], 'class_id':[], 'tracker_id':[], 'num_detections':0}
    railxing_detections={'xyxy':[], 'confidence':[], 'class_id':[], 'tracker_id':[], 'num_detections':0}
    if tracked_detections['num_detections']>0:
        for idx in range(tracked_detections['num_detections']):
            class_id= tracked_detections['class_id'][idx]
            if (tracked_detections['class_id'][idx] >= GATE_ARM_BAR_OFFSET_TO_ADD_TO_CLASSID):
                class_id= class_id - GATE_ARM_BAR_OFFSET_TO_ADD_TO_CLASSID
                label= gate_arm_bar_labels[str(class_id)]
                gate_detections['xyxy'].append(tracked_detections['xyxy'][idx])
                gate_detections['confidence'].append(tracked_detections['confidence'][idx])
                gate_detections['class_id'].append(class_id)
                gate_detections['tracker_id'].append(tracked_detections['tracker_id'][idx])
                gate_detections['num_detections']+=1
            else:
                label= coco_labels[str(class_id)]
                railxing_detections['xyxy'].append(tracked_detections['xyxy'][idx])
                railxing_detections['confidence'].append(tracked_detections['confidence'][idx])
                railxing_detections['class_id'].append(class_id)
                railxing_detections['tracker_id'].append(tracked_detections['tracker_id'][idx])
                railxing_detections['num_detections']+=1
            #print("Frame #", analyze_tracks.frame_idx, " Detection: ", label, "(", class_id, ")"," with confidence: ", tracked_detections['confidence'][idx], " tracker id: ", tracked_detections['tracker_id'][idx], " and bbox:", tracked_detections['xyxy'][idx])

    analyze_tracks.frame_idx+=1

    # For demonstration purpose, we show below how you can add text to the frame using fake detection objects.
    # The displayed text is not related to the actual detections and will contain the confidence value in %.
    # To remove the confidence value, edit the function 'get_detection_text()' file in $TAPPAS_WORKSPACE/core/hailo/plugins/overlay/overlay.cpp
    # and recompile the library by running $TAPPAS_WORKSPACE/scripts/gstreamer/install_hailo_gstreamer.sh
    # Note that if you remove the display of the confidence value, it will be removed for all detections, even the real ones
    # You may want to implement something more sophisticated to display the confidence value only for the real detections
    # based may be on the class_id or the label.
    #detection_list=[]
    #detection_list.append(hailo.HailoDetection(bbox= hailo.HailoBBox(xmin=0.5, ymin=0.5, width=0.1, height=0.001), label= "Text 1", confidence= 0.0, index= 0))
    #detection_list.append(hailo.HailoDetection(bbox= hailo.HailoBBox(xmin=0.5, ymin=0.6, width=0.1, height=0.001), label= "Text 2", confidence= 0.0, index= 0))
    #hailo.add_detections(video_frame.roi, detection_list)
    
    '''
    with video_frame.map_buffer() as map_info:
        #  Create a NumPy array from the buffer data
        numpy_frame_ro = video_frame.numpy_array_from_buffer(map_info, video_info=video_frame.video_info)
        print("Data shape:", numpy_frame_ro.shape)
        print("Data type:", numpy_frame_ro.dtype)
        if analyze_tracks.frame_idx < 10:
            # Save the frame to a file
            file_name = "frame_" + str(analyze_tracks.frame_idx) + ".npy "
            print("Frame #", analyze_tracks.frame_idx, " shape: ", numpy_frame_ro.shape)
            #cv2.imwrite(file_name, numpy_frame_ro)
            np.save(file_name, numpy_frame_ro)
        # Note this is a read only copy of the frame
        # If you want to modify the frame you need to make a copy
        # numpy_frame = numpy_frame_ro.copy()
        # Note the modifing the frame will not change the original frame (for this you'll need to replave the buffer data)
    '''
    return Gst.FlowReturn.OK
