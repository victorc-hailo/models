import sys
import zmq
import time
import json
import numpy as np
import cv2

GATE_ARM_BAR_OFFSET_TO_ADD_TO_CLASSID= 1000

def extract_detections(hailo_output, threshold=0.0, extract_track_id=True):

    """Extract detections from the HailoRT-postprocess output."""

    xyxy = []
    confidence = []
    class_id = []
    label= []
    if extract_track_id:
        track_id = []
    num_detections = 0

    for detection in hailo_output:

        hailo_detection= detection['HailoDetection']
        score = hailo_detection['confidence']

        if score < threshold:
            continue

        xmin= hailo_detection['HailoBBox']['xmin']
        ymin= hailo_detection['HailoBBox']['ymin']
        xmax= hailo_detection['HailoBBox']['xmin'] + hailo_detection['HailoBBox']['width']
        ymax= hailo_detection['HailoBBox']['ymin'] + hailo_detection['HailoBBox']['height']
        bbox = [xmin, ymin, xmax, ymax]        
        xyxy.append(bbox)
        confidence.append(score)
        class_id.append(hailo_detection['class_id'])
        label.append(hailo_detection['label'])
        num_detections = num_detections + 1

        if extract_track_id:
            track_id_dict=hailo_detection['SubObjects']
            if len(track_id_dict) > 0:
                track_id.append(track_id_dict[0]['HailoUniqueID']['unique_id'])

    returnedObject = {'xyxy':       np.array(xyxy),
    'confidence': np.array(confidence),
    'class_id':   np.array(class_id),
    'label': label,
    'num_detections': num_detections}

    if extract_track_id:
        returnedObject['tracker_id'] = np.array(track_id)
        
    return returnedObject

port = "5555"
if len(sys.argv) > 1:
    port =  sys.argv[1]
    int(port)
    
# Socket to talk to server
context = zmq.Context()
socket = context.socket(zmq.SUB)
socket.setsockopt_string(zmq.SUBSCRIBE, "")

print ("Collecting updates from hailoexportzm...")

# Use try and except to handle the error
try:
    socket.connect (f"tcp://localhost:{port}")  
except Exception as e:
    print (e)
    print ("Error connecting to the server")
    exit()

print(f"Connection successful to port {port}")

while True:
    hailo_meta_dict = json.loads(socket.recv())
    image= socket.recv()
    # convert from 'byte' to numpy array
    image= np.frombuffer(image, dtype=np.uint8)

    #print(type(image))
    #print(len(image))

    frame_idx= hailo_meta_dict['buffer_offset']
    detections= hailo_meta_dict['HailoROI']['SubObjects']
    width= hailo_meta_dict['width']
    height= hailo_meta_dict['height']

    image= image.reshape(height, width, 3)
    # Convert to BGR image since it is in RGB and openCV uses BGR
    image= cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
    
    #print(f"frame_idx: {frame_idx}")
    #print(f"HailoROI: {detections}")
    tracked_detections= extract_detections(detections)

    gate_detections={'xyxy':[], 'confidence':[], 'class_id':[], 'tracker_id':[], 'num_detections':0}
    railxing_detections={'xyxy':[], 'confidence':[], 'class_id':[], 'tracker_id':[], 'num_detections':0}
    if tracked_detections['num_detections']>0:
        for idx in range(tracked_detections['num_detections']):
            class_id= tracked_detections['class_id'][idx]
            if (tracked_detections['class_id'][idx] >= GATE_ARM_BAR_OFFSET_TO_ADD_TO_CLASSID):
                class_id= class_id - GATE_ARM_BAR_OFFSET_TO_ADD_TO_CLASSID
                label= tracked_detections['label'][idx]
                gate_detections['xyxy'].append(tracked_detections['xyxy'][idx])
                gate_detections['confidence'].append(tracked_detections['confidence'][idx])
                gate_detections['class_id'].append(class_id)
                gate_detections['tracker_id'].append(tracked_detections['tracker_id'][idx])
                gate_detections['num_detections']+=1
            else:
                label= tracked_detections['label'][idx]
                railxing_detections['xyxy'].append(tracked_detections['xyxy'][idx])
                railxing_detections['confidence'].append(tracked_detections['confidence'][idx])
                railxing_detections['class_id'].append(class_id)
                railxing_detections['tracker_id'].append(tracked_detections['tracker_id'][idx])
                railxing_detections['num_detections']+=1
            print("Frame #", frame_idx, width, "x", height, "Detection: ", label, "(", class_id, ")"," with confidence: ", tracked_detections['confidence'][idx], " tracker id: ", tracked_detections['tracker_id'][idx],  " and bbox:", tracked_detections['xyxy'][idx])

    cv2.imshow("Frame", image)

    if (cv2.waitKey(30) == 27):
            break

cv2.destroyAllWindows()

