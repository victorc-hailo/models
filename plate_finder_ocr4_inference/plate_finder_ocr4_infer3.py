# Importing Libraries
import tensorflow as tf
import os
import cv2
import numpy as np
import argparse
#from model.nms import yolov4_nms,NonMaxSuppression
from model.nms_detect import yolov4_nms,NonMaxSuppression
from utils.preprocess import resize_img,resize_img_detect
import random
import albumentations as A
import time

import onnxruntime
from hailo_platform import __version__
from multiprocessing import Process, Queue, Event, Lock, Value
from hailo_platform import (HEF, PcieDevice, HailoStreamInterface, ConfigureParams, InferVStreams,
 InputVStreamParams, OutputVStreamParams, InputVStreams, OutputVStreams, FormatType)

import string


# -------------------------------- Inferences threads functions ------------------------------ #
def infer_model_send(network_group, network_group_params, hef, post_process_onnx, read_q, write_q, isRunningFlag,
                startEvent, syncEvent):
    # Create input and output virtual streams params
    # Quantized argument signifies whether or not the incoming data is already quantized.
    # Data is quantized by HailoRT if and only if quantized == False .
    input_vstreams_params = InputVStreamParams.make(network_group, quantized=True, format_type=FormatType.UINT8)
    # Define dataset params
    input_vstream_info = hef.get_input_vstream_infos()[0]

    startEvent.wait()

    # print("How about here? 2")
    with InputVStreams(network_group, input_vstreams_params) as vstreams:
        vstream_to_buffer = {vstream: np.ndarray([1] + list(vstream.shape), dtype=vstream.dtype) for vstream in
                             vstreams}
        # print("How about here? 3")
        while (True):
            # print("I come here for plate_finder")
            if (read_q.empty() == False):
                network_group.wait_for_activation(100)
                start_time = time.time()
                image = read_q.get(0)
                data = np.expand_dims(image, axis=0)
                for vstream, _ in vstream_to_buffer.items():
                    vstream.send(data)
                # Set sync event to signal to the infer_model_rcv thread 
                # that inference output data will be ready soon
                syncEvent.set()
            with isRunningFlag.get_lock():
                if isRunningFlag.value == False:
                    # print("Running flag is false")
                    break


def infer_model_recv(network_group, network_group_params, hef, post_process_onnx, read_q, write_q, isRunningFlag,
                startEvent, syncEvent):
    # Create input and output virtual streams params
    # Quantized argument signifies whether or not the incoming data is already quantized.
    # Data is quantized by HailoRT if and only if quantized == False .
    output_vstreams_params = OutputVStreamParams.make(network_group, quantized=False, format_type=FormatType.FLOAT32)

    startEvent.wait()

    # print("How about here? 2")
    with OutputVStreams(network_group, output_vstreams_params) as vstreams:        # print("How about here? 3")
        while (True):
            # print("I come here for plate_finder")
            # Wait from the sync event signal from the infer_model_send() thread
            if syncEvent.is_set():
                network_group.wait_for_activation(100)
                values = []
                for vstream in vstreams:
                    data= vstream.recv()
                    #print("******** Infer Image received")
                    switched_tensor = np.swapaxes(np.swapaxes(np.expand_dims(data, axis=0), 1, 3), 2, 3)  # 1 <--> 2, then 2 <--> 3
                    values.append(switched_tensor)

                values.sort(key=lambda x: x.shape)
                inp = post_process_onnx.get_inputs()
                inp.sort(key=lambda x: x.shape)
                curr_vstream_data_dict = {i.name: v for i, v in zip(inp, values)}
                write_q.put(curr_vstream_data_dict)
                # Clear the sync event so this thread can wait for it and resume when new data is available
                syncEvent.clear()

            #print("******** Infer output sent to queue")
            with isRunningFlag.get_lock():
                if isRunningFlag.value == False:
                    # print("Running flag is false")
                    break

# Plate_finder post process onnx function
def do_post_process_onnx(read_q, write_q, post_process_onnx, isRunningFlag, startEvent):

    startEvent.wait()

    #print("********* do_post_process_onnx started")
    while True:
        if (read_q.empty() == False):
            inference_dict = read_q.get(0)
            #print("******** do_post_process_onnx Image received")
            start_time = time.time()
            inference_output = post_process_onnx.run(None, inference_dict)
            end_time = (time.time() - start_time)
            print("---detection post-process time is: %s seconds ---" % end_time)
            write_q.put(inference_output)
            #print("********* do_post_process_onnx output sent to queue")
        with isRunningFlag.get_lock():
            if isRunningFlag.value == False:
                #print("Running flag is false")
                break

    #print("Post_process thread exited")

def ocr4_infer_model_send(network_group,network_group_params, hef, read_q, write_q, isRunningFlag, startEvent, syncEvent):

    # Create input and output virtual streams params
    # Quantized argument signifies whether or not the incoming data is already quantized.
    # Data is quantized by HailoRT if and only if quantized == False .
    input_vstreams_params = InputVStreamParams.make(network_group, quantized=True, format_type=FormatType.UINT8)
    # Define dataset params
    input_vstream_info = hef.get_input_vstream_infos()[0]

    startEvent.wait()
    print("The network_group is: " + str(network_group))

    with InputVStreams(network_group, input_vstreams_params) as vstreams:
        vstream_to_buffer = {vstream: np.ndarray([1] + list(vstream.shape), dtype=vstream.dtype) for vstream in
                             vstreams}
        while (True):
            #network_group.wait_for_activation(100)
            if (read_q.empty() == False):
                network_group.wait_for_activation(100)
                #print("Do I make it here? 4")
                #print(len(read_q))
                image = read_q.get(0)
                data = np.expand_dims(image, axis=0)
                for vstream, _ in vstream_to_buffer.items():
                    vstream.send(data)
                # Set sync event to signal to the ocr4_infer_model_recv thread 
                # that inference output data will be ready soon
                syncEvent.set()

            with isRunningFlag.get_lock():
                #print("Do I lock here?")
                if isRunningFlag.value == False:
                    #print("Running flag is false")
                    break

def ocr4_infer_model_recv(network_group,network_group_params, hef, read_q, write_q, isRunningFlag, startEvent, syncEvent):

    # Create input and output virtual streams params
    # Quantized argument signifies whether or not the incoming data is already quantized.
    # Data is quantized by HailoRT if and only if quantized == False .
    output_vstreams_params = OutputVStreamParams.make(network_group, quantized=True, format_type=FormatType.UINT8)

    startEvent.wait()

    with OutputVStreams(network_group, output_vstreams_params) as vstreams:
        while (True):
            #network_group.wait_for_activation(100)
            # Wait from the sync event signal from the ocr4_infer_model_send() thread
            if syncEvent.is_set():
                network_group.wait_for_activation(100)
                for vstream in vstreams:
                    data= vstream.recv()
                write_q.put(data)
                # Clear the sync event so this thread can wait for it and resume when new data is available
                syncEvent.clear()

            with isRunningFlag.get_lock():
                #print("Do I lock here?")
                if isRunningFlag.value == False:
                    #print("Running flag is false")
                    break

'''
def ocr4_infer_model_test(network_group,network_group_params, hef, read_q, write_q):

    # Create input and output virtual streams params
    # Quantized argument signifies whether or not the incoming data is already quantized.
    # Data is quantized by HailoRT if and only if quantized == False .
    input_vstreams_params = InputVStreamParams.make(network_group, quantized=True, format_type=FormatType.UINT8)
    output_vstreams_params = OutputVStreamParams.make(network_group, quantized=True, format_type=FormatType.UINT8)
    # Define dataset params
    input_vstream_info = hef.get_input_vstream_infos()[0]

    print("DO I COME HERE?")

    network_group.wait_for_activation(100)
    print("Do I make it here? 1")
    with InferVStreams(network_group, input_vstreams_params, output_vstreams_params) as infer_pipeline:
        print("Do I make it here? 2")
        while (True):
            #print("Do I make it here? 3")
            if (read_q.empty() == False):
                #print("Do I make it here? 4")
                #print(len(read_q))
                image = read_q.get(0)
                #data = np.expand_dims(image, axis=0)
                start_time = time.time()
                input_data = {input_vstream_info.name: image}
                infer_results = infer_pipeline.infer(input_data)
                end_time = (time.time() - start_time)
                print("---The ocr4 detection time is: %s seconds ---" % end_time)
                write_q.put(infer_results)
                
'''

# -------------------------------- Processing Data and sending Data to the inference threads -------------------------------- #

# Processing the image for Plate_finder inferencing, sending plate_finder data to the inferencing thread, and nms on the results.
def detect_batch_img(network_group, network_group_params, img,nms_max_box_num,nms_iou_threshold,nms_score_threshold, post_process_onnx=[], imageQueue=[], outQueue=[]):

    #boxes, scores, classes, valid_detections = detect_batch_img(batch_img, hef, post_process_onnx,imageQueue=imageQueue, outQueue=outQueue)
    #boxes, scores, classes = tta_nms(boxes, scores, classes, valid_detections)

    #print("Processing the HEF model")
    #time.sleep(1)
    #print(img)
    pre_nms_decoded_boxes = []
    pre_nms__scores = []
    # Below processing is for onnx model

    
    data = np.expand_dims(np.expand_dims(img[0], axis=0), axis=0).astype(np.uint8)
    
    start_detect_time = time.time()
    imageQueue.put(data)
    #print("***** Input image sent from detect_batch_img")
    inference_output= outQueue.get()

    start_time = time.time()
    #print("***** Output received in detect_batch_img")
    pre_nms_decoded_boxes.append(inference_output[0][0][:][:])
    pre_nms__scores.append(inference_output[1][0][:][:])

    pre_nms_decoded_boxes = np.array(pre_nms_decoded_boxes)
    pre_nms__scores = np.array(pre_nms__scores)

    boxes, scores, classes, valid_detections = yolov4_nms('diou_nms')(pre_nms_decoded_boxes, pre_nms__scores, nms_max_box_num,nms_iou_threshold,nms_score_threshold)
    end_time = (time.time() - start_time)
    
    print("---detection time yolov4_nms is: %s seconds ---" % end_time)

    end_time = (time.time() - start_detect_time)
    print("---Overall detection time (Hailo + post-process + yolov4_nms) is: %s seconds ---" % end_time)

    return boxes, scores, classes, valid_detections
    # pre_nms_decoded_boxes,pre_nms__scores = model(img)
    # pre_nms_decoded_boxes = pre_nms_decoded_boxes.numpy()
    # pre_nms__scores = pre_nms__scores.numpy()
    # return pre_nms_decoded_boxes, pre_nms__scores

# Processing Plate_finder crops for OCR4 inferencing, sending the crops to OCR4 Inference Thread, and parsing the inference to a plate string
def detech_ocr4(network_group, network_group_params, boxes,img_copy,ocr4_image_size,scores,nms_score_threshold,x_scale,y_scale,test_value,img_orig_size,ocr4_inQueue,ocr4_outQueue):
    crop_list = []
    SYMBOLS = string.digits + string.ascii_uppercase + '*'
    string_list = []
    #print("The length of bboxes are:" + str(len(boxes)))
    # Processing Plate_finder crops. It scales the crop bounding box coordinates back to the original image and crops it from there.
    if len(boxes) > 0:
        for i in range(len(boxes)):

            x1y1 = (boxes[i][0:2] * img_copy.shape[0:2][::-1]).astype(np.int)
            x2y2 = (boxes[i][2:4] * img_copy.shape[0:2][::-1]).astype(np.int)
            
            box = [x1y1[0],x1y1[1],x2y2[0],x2y2[1]]

            
            #print("The box is: " + str(box))
            orig_x1 = int(np.round(box[0] * (x_scale)))
            #print("The orig_x1 is: " + str(orig_x1))
            orig_y1 = int(np.round((box[1] * y_scale)-(test_value * (y_scale / 2))))
            #print("The orig_y1 is: " + str(orig_y1))
            if (orig_y1 < 0 ):
                orig_y1 = 0
            #print("The orig_y1 after check is: " + str(orig_y1))
            orig_x2 = int(np.round(box[2] * (x_scale)))
            #print("The orig_x2 is: " + str(orig_x2))
            orig_y2 = int(np.round((box[3] * y_scale)-(test_value * (y_scale / 2))))
            #print("The orig_y2 is: " + str(orig_y2))
            if(orig_y2 > img_orig_size.shape[0]):
                orig_y2 = img_orig_size.shape[0]
            #print("The orig_y2 after check is: " + str(orig_y2))

            # Crop the lp_finder detection on the original input image.
            crop = img_orig_size[orig_y1:orig_y2, orig_x1:orig_x2]
            #print("The crop is: " + str(crop.shape))
            
            start_time = time.time()
            crop = cv2.cvtColor(crop, cv2.COLOR_BGR2RGB)
            crop = cv2.resize(crop,dsize=(ocr4_image_size,ocr4_image_size))
            crop = np.array(crop, dtype=np.uint8)
            end_time = (time.time() - start_time)
            print("---cvtColor+resize time is: %s seconds ---" % end_time)

            start_time = time.time()
            # Sending the crops to OCR4 input queue
            ocr4_inQueue.put(crop)
            # Getting OCR4 results back from the OCR4 Inference Thread
            ocr4_inference_output= ocr4_outQueue.get()
            end_time = (time.time() - start_time)
            print("---OCR4 time is: %s seconds ---" % end_time)

            reshaped = tf.reshape(ocr4_inference_output,(10,37))
            final = tf.argmax(reshaped,1)
            #SYMBOLS = string.digits + string.ascii_uppercase + '*'
            final_string =''.join([SYMBOLS[x] for x in final])
            string_list.append(final_string)
            print("The OCR4 detection is: " + final_string)

    else:
        string_list = []
    return string_list

def tta_nms(boxes,scores,classes,valid_detections,nms,nms_max_box_num,nms_iou_threshold,nms_score_threshold):
    all_boxes = []
    all_scores = []
    all_classes = []
    batch_index = 0
    valid_boxes = boxes[batch_index][0:valid_detections[batch_index]]
    valid_boxes[:, (0, 2)] = (1.-valid_boxes[:,(2,0)])
    all_boxes.append(valid_boxes)
    all_scores.append(scores[batch_index][0:valid_detections[batch_index]])
    all_classes.append(classes[batch_index][0:valid_detections[batch_index]])
    for batch_index in range(1,boxes.shape[0]):
        all_boxes.append(boxes[batch_index][0:valid_detections[batch_index]])
        all_scores.append(scores[batch_index][0:valid_detections[batch_index]])
        all_classes.append(classes[batch_index][0:valid_detections[batch_index]])
    all_boxes = np.concatenate(all_boxes,axis=0)
    all_scores = np.concatenate(all_scores, axis=0)
    all_classes = np.concatenate(all_classes, axis=0)
    all_boxes,all_scores,all_classes = np.array(all_boxes), np.array(all_scores), np.array(all_classes)
    boxes, scores, classes, valid_detections = NonMaxSuppression.diou_nms_np_tta(np.expand_dims(all_boxes,0),np.expand_dims(all_scores,0),np.expand_dims(all_classes,0),nms_max_box_num,nms_iou_threshold,nms_score_threshold)
    boxes, scores, classes, valid_detections = np.squeeze(boxes), np.squeeze(scores), np.squeeze(classes), np.squeeze(valid_detections)
    ##print("The length of boxes are: {0}".format(len(boxes)))
    ##print("The shape of boxes are: {0}".format(boxes.shape))
    ##print("The boxes look like this: {0}".format(boxes))
    return boxes[:valid_detections], scores[:valid_detections], classes[:valid_detections]

def plot_one_box(img, box, color=None, label=None, line_thickness=None):
    tl = line_thickness or round(0.002 * (img.shape[0] + img.shape[1]) / 2) + 1  # line/font thickness
    color = color or [random.randint(0, 255) for _ in range(3)]
    c1, c2 = (int(box[0]), int(box[1])), (int(box[2]), int(box[3]))
    cv2.rectangle(img, c1, c2, color, thickness=tl, lineType=cv2.LINE_AA)
    if label:
        tf = max(tl - 1, 1)  # font thickness
        t_size = cv2.getTextSize(label, 0, fontScale=tl / 7, thickness=tf)[0]
        c2 = c1[0] + t_size[0], c1[1] - t_size[1] - 3
        cv2.rectangle(img, c1, c2, color, -1, cv2.LINE_AA)  # filled
        cv2.putText(img, label, (c1[0], c1[1] - 2), 0, tl / 7, [225, 255, 255], thickness=tf, lineType=cv2.LINE_AA)

def plot_boxes(img,boxes,scores,classes,class_names,args):
    for i in range(len(boxes)):
            if scores[i] < args.nms_score_threshold:
                continue
            x1y1 = (boxes[i][0:2] * img.shape[0:2][::-1]).astype(np.int)
            x2y2 = (boxes[i][2:4] * img.shape[0:2][::-1]).astype(np.int)
            plot_one_box(img,[x1y1[0],x1y1[1],x2y2[0],x2y2[1]],(255,0,255),label=str(class_names[classes[i]]) + "," + str("%0.2f" % scores[i]))
def get_tta_tranform():
   out_list=[]
   tta_transform = A.Compose([
       A.HorizontalFlip(p=1.),
       #A.RandomBrightnessContrast(p=0.2),
   ])
   out_list.append(tta_transform)
   #tta_transform = A.Compose([
   #    A.RandomBrightnessContrast(p=0.2),
   #])
   #out_list.append(
   return out_list



# -------------------------------- Main Process Thread -------------------------------- #
def mainProcess(plate_finder_network_group, plate_finder_network_group_params, ocr4_network_group, ocr4_network_group_params, max_num_pics,img_size,ocr4_image_size,pic_dir, post_process_onnx, imageQueue, outQueue, ocr4_inQueue,ocr4_outQueue, isRunningFlag, startEvent,startEvent_ocr4, twoDeviceFlag):
#max_num_pics, pic_dir,img_size,hef, post_process_onnx, imageQueue, outQueue, isRunningFlag, startEvent
    img_list = os.listdir(pic_dir)
    TTA = True
    nms = 'diou_nms'
    nms_max_box_num = 6
    nms_iou_threshold = 0.2
    nms_score_threshold = 0.2
    
    perf_log='perf_infer3.csv'

    if os.path.exists(perf_log):
        os.remove(perf_log)

    if max_num_pics!=0 :
        img_list = img_list[0:min(max_num_pics, len(img_list))]
    
    counter = 0
    averVal = 0
    for img_name in img_list:
        print("----------------------------------------------------------")
        print(str(counter) + ": " + img_name)
        img = cv2.imread(os.path.join(pic_dir, img_name))
        img_ori, _, _,new_y = resize_img_detect(img,img_size)
        img_copy = img_ori.copy()
        #img_copy_onnx = img_copy.copy()
        #img_copy_hef = img_copy.copy()

        aug_imgs = []
        if TTA:
            tta_transforms = get_tta_tranform()
            aug_imgs.append(tta_transforms[0](image=img_copy)['image'])
            #aug_imgs.append(tta_transforms[1](image=img_copy)['image'])
        
        
        aug_imgs.append(img_copy)
        batch_img = np.array(aug_imgs)


        start_time = time.time()
        startEvent.set()
        # Plate Finder Inference and Onnx Post Processing
        #If we have 2 devices, no need to activate the network at every frame as each network goes to a separate device
        if twoDeviceFlag == 1:
            boxes, scores, classes, valid_detections = detect_batch_img(plate_finder_network_group, plate_finder_network_group_params, batch_img, nms_max_box_num,nms_iou_threshold,nms_score_threshold, post_process_onnx,
                                                                            imageQueue=imageQueue, outQueue=outQueue)
        else:
            with plate_finder_network_group.activate(plate_finder_network_group_params):
                boxes, scores, classes, valid_detections = detect_batch_img(plate_finder_network_group, plate_finder_network_group_params, batch_img, nms_max_box_num,nms_iou_threshold,nms_score_threshold, post_process_onnx,
                                                                                imageQueue=imageQueue, outQueue=outQueue)

        #boxes,scores,classes,valid_detections,nms,nms_max_box_num,nms_iou_threshold,nms_score_threshold)                                                                    
        start_tta_nms_time = time.time()
        boxes, scores, classes = tta_nms(boxes, scores, classes, valid_detections,nms,nms_max_box_num,nms_iou_threshold,nms_score_threshold)
        end_time = (time.time() - start_tta_nms_time)
        print("---tta_nms is: %s seconds ---" % end_time)

        # Setting up variables for OCR4 Inferencing
        y_ = img.shape[0]
        x_ = img.shape[1]
        
        y_scale = y_ / (new_y)
        x_scale = x_ / img_size 
        
        test_value = img_size - new_y
        img_orig_size = img

        # OCR4 Inference. Note I tried using the original startEvent and I still get run time errors.
        startEvent_ocr4.set()
        if twoDeviceFlag == 1:
            string_list = detech_ocr4(ocr4_network_group, ocr4_network_group_params, boxes,img_copy,ocr4_image_size,scores,nms_score_threshold,x_scale,y_scale,test_value,img_orig_size,ocr4_inQueue,ocr4_outQueue)
        else:
            with ocr4_network_group.activate(ocr4_network_group_params):
                string_list = detech_ocr4(ocr4_network_group, ocr4_network_group_params, boxes,img_copy,ocr4_image_size,scores,nms_score_threshold,x_scale,y_scale,test_value,img_orig_size,ocr4_inQueue,ocr4_outQueue)
        
        counter = counter + 1

        end_time = (time.time() - start_time)
        print("---Processing time is: %s seconds ---" % end_time)

        with open(perf_log, 'a') as perf_file:
            perf_file.write(str(counter)+','+ img_name +','+str(string_list).replace(',',':') +','+str(end_time)+'\n')

        #plot_boxes(img_copy, boxes, scores, classes, class_names, args)
        #filePath = os.path.join(hef_output_dir, img_name)
        #cv2.imwrite(filePath, img_copy)
        #print(">>>> Writing " + filePath)

        # print("Next")
        # inference_output.sort(key=lambda x: x.shape)
        # curr_vstream_data_dict = {i.name: v for i, v in zip(inference_output, values)}

        # cv2.imshow("d", img_copy/255)
        # cv2.waitKey(0)

        averVal= averVal + end_time

    with isRunningFlag.get_lock():
        isRunningFlag.value= False

    averVal= averVal/counter
    print("------> Average fps is: %s fps ---" % (1/averVal))
    #print ("mainProcess exited")

# Main Function.
def main():

    #pip install albumentations
    #pip install opencv-python==4.5.4.60

    
    
    
    # I converted the arguments to variables, set them below.
    max_num_pics = 0
    plate_finder_hef = "./lp_finder_hailo/lp_finder_3.hef"
    post_process_onnx_path = "./lp_finder_hailo/lp_finder_3_post.onnx"
    hef_output_dir = "./lp_finder_3_results"
    pic_dir = "./lp_test_data"
    plate_finder_image_size = 320
    ocr4_image_size = 150

    ocr4_hef_path = "./ocr4_model_hailo/ResNet.hef"


    isRunningFlag= Value('i', True)
    if os.path.isdir(hef_output_dir) == False:
        os.mkdir(hef_output_dir)

    hef = HEF(plate_finder_hef)
    ocr4_hef = HEF(ocr4_hef_path)

    # Initializing Queues and Onnx runtime
    post_process_onnx = onnxruntime.InferenceSession(post_process_onnx_path)
    imageQueue = Queue()
    hefOutQueue = Queue()
    outQueue = Queue()
    ocr4_inQueue = Queue()
    ocr4_outQueue = Queue()
    
    # Start Events
    startEvent= Event()
    startEvent_ocr4 = Event()
    syncEvent_plate_finder= Event()
    syncEvent_ocr4= Event()
    
    device_infos=PcieDevice().scan_devices()
    target = [0,0]
    target[0] = PcieDevice(device_infos[0])
    twoDeviceFlag = 0
    if len(device_infos) > 1:
        target[1] = PcieDevice(device_infos[1])
        twoDeviceFlag = 1

    # Initializing OCR4 and Plate_finder Models
    ocr4_configure_params = ConfigureParams.create_from_hef(ocr4_hef, interface=HailoStreamInterface.PCIe)
    ocr4_network_group = target[0].configure(ocr4_hef, ocr4_configure_params)[0]
    ocr4_network_group_params = ocr4_network_group.create_params()

    configure_params = ConfigureParams.create_from_hef(hef, interface=HailoStreamInterface.PCIe)
    network_group = target[twoDeviceFlag].configure(hef, configure_params)[0]
    network_group_params = network_group.create_params()

    # Main Process
    main_process = Process(target=mainProcess, args=(network_group, network_group_params, ocr4_network_group, ocr4_network_group_params, max_num_pics, plate_finder_image_size, ocr4_image_size,pic_dir, post_process_onnx, imageQueue, outQueue, ocr4_inQueue,ocr4_outQueue, isRunningFlag, startEvent,startEvent_ocr4, twoDeviceFlag))

    # Plate Finder Inference and Post Process Threads
    infer_send_process = Process(target=infer_model_send, args=(
    network_group, network_group_params, hef, post_process_onnx, imageQueue, hefOutQueue, isRunningFlag, startEvent, syncEvent_plate_finder))
    infer_recv_process = Process(target=infer_model_recv, args=(
        network_group, network_group_params, hef, post_process_onnx, imageQueue, hefOutQueue, isRunningFlag,
        startEvent, syncEvent_plate_finder))

    post_process = Process(target=do_post_process_onnx, args=(hefOutQueue, outQueue, post_process_onnx, isRunningFlag, startEvent))

    #OCR4 Inference Thread
    ocr4_infer_send_process = Process(target=ocr4_infer_model_send, args=(ocr4_network_group, ocr4_network_group_params, ocr4_hef, ocr4_inQueue, ocr4_outQueue, isRunningFlag, startEvent_ocr4, syncEvent_ocr4))
    ocr4_infer_recv_process = Process(target=ocr4_infer_model_recv, args=(ocr4_network_group, ocr4_network_group_params, ocr4_hef, ocr4_inQueue, ocr4_outQueue, isRunningFlag, startEvent_ocr4, syncEvent_ocr4))

    # Starting Threads
    if twoDeviceFlag== 1:
        with ocr4_network_group.activate(ocr4_network_group_params):
            with network_group.activate(network_group_params):
                infer_send_process.start()
                infer_recv_process.start()
                post_process.start()
                ocr4_infer_send_process.start()
                ocr4_infer_recv_process.start()
                main_process.start()

                infer_send_process.join()
                infer_recv_process.join()
                post_process.join()
                ocr4_infer_send_process.join()
                ocr4_infer_recv_process.join()
                main_process.join()
    else:
                infer_send_process.start()
                infer_recv_process.start()
                post_process.start()
                ocr4_infer_send_process.start()
                ocr4_infer_recv_process.start()
                main_process.start()

                infer_send_process.join()
                infer_recv_process.join()
                post_process.join()
                ocr4_infer_send_process.join()
                ocr4_infer_recv_process.join()
                main_process.join()
import sys
if __name__ == '__main__':
    #args = parse_args(sys.argv[1:])
    
    main()
