# Importing Libraries
import tensorflow as tf
import os
import cv2
import numpy as np
from model.nms_detect import yolov4_nms,NonMaxSuppression
from utils.preprocess import resize_img_detect
import time
import onnxruntime
from hailo_platform import __version__
from multiprocessing import Process, Queue, Event, Lock, Value
from queue import Empty
from hailo_platform import (HEF, PcieDevice, HailoStreamInterface, ConfigureParams, InferVStreams,
 InputVStreamParams, OutputVStreamParams, InputVStreams, OutputVStreams, FormatType)

import string

def dprint(string):
  #__builtins__.print("%s" % string)
  pass

def ddprint(string):
  #__builtins__.print("%s" % string)
  pass

QUEUE_TIMEOUT=1 # Second

# -------------------------------- Inferences threads functions ------------------------------ #
def infer_model_send(network_group, network_group_params, hef, post_process_onnx, read_q, write_q, isRunningFlag,
                startEvent, syncEvent, numCameras):
    # Create input and output virtual streams params
    # Quantized argument signifies whether or not the incoming data is already quantized.
    # Data is quantized by HailoRT if and only if quantized == False .
    input_vstreams_params = InputVStreamParams.make(network_group, quantized=True, format_type=FormatType.UINT8)
    # Define dataset params
    input_vstream_info = hef.get_input_vstream_infos()[0]

    startEvent.wait()

    # print("How about here? 2")
    with InputVStreams(network_group, input_vstreams_params) as vstreams:
        vstream_to_buffer = {vstream: np.ndarray([1] + list(vstream.shape), dtype=vstream.dtype) for vstream in
                             vstreams}
        # print("How about here? 3")
        while (True):
            # print("I come here for plate_finder")
            #if (read_q.empty() == False):
            for i in range(numCameras):
                try:
                    image= read_q.get(timeout= QUEUE_TIMEOUT)
                except Empty:
                    with isRunningFlag.get_lock():
                        #print("Do I lock here?")
                        if isRunningFlag.value == False:
                            #print("Running flag is false")
                            return
                        else:
                            print ("Error infer_model_send timed-out")
                #ddprint("infer_model_send")
                
                network_group.wait_for_activation(100)
                
                data = np.expand_dims(image, axis=0)
                for vstream, _ in vstream_to_buffer.items():
                    vstream.send(data)
                # If last camera, set sync event to signal to the infer_model_rcv thread 
                # that inference output data will be ready soon
                if (i== numCameras-1):
                    syncEvent.set()


def infer_model_recv(network_group, network_group_params, hef, post_process_onnx, read_q, write_q, isRunningFlag,
                startEvent, syncEvent, numCameras):
    # Create input and output virtual streams params
    # Quantized argument signifies whether or not the incoming data is already quantized.
    # Data is quantized by HailoRT if and only if quantized == False .
    output_vstreams_params = OutputVStreamParams.make(network_group, quantized=False, format_type=FormatType.FLOAT32)

    startEvent.wait()

    # print("How about here? 2")
    with OutputVStreams(network_group, output_vstreams_params) as vstreams:        # print("How about here? 3")
        while (True):
            # print("I come here for plate_finder")
            # Wait from the sync event signal from the infer_model_send() thread
            #if syncEvent.is_set():
            syncEvent.wait(QUEUE_TIMEOUT)
            if syncEvent.is_set():

                network_group.wait_for_activation(100)
                
                for i in range(numCameras):
                    values = []

                    for vstream in vstreams:
                        data= vstream.recv()
                        #ddprint("infer_model_recv")
                        #print("******** Infer Image received")
                        switched_tensor = np.swapaxes(np.swapaxes(np.expand_dims(data, axis=0), 1, 3), 2, 3)  # 1 <--> 2, then 2 <--> 3
                        values.append(switched_tensor)

                    values.sort(key=lambda x: x.shape)
                    inp = post_process_onnx.get_inputs()
                    inp.sort(key=lambda x: x.shape)
                    curr_vstream_data_dict = {i.name: v for i, v in zip(inp, values)}
                    #ddprint("infer_model_recv q put")
                    write_q.put(curr_vstream_data_dict)
                # Clear the sync event so this thread can wait for it and resume when new data is available
                    if (i== numCameras-1):
                        syncEvent.clear()

            #print("******** Infer output sent to queue")
            with isRunningFlag.get_lock():
                if isRunningFlag.value == False:
                    # print("Running flag is false")
                    break

# Plate_finder post process onnx function
def do_post_process_onnx(read_q, write_q, post_process_onnx, isRunningFlag, startEvent):

    startEvent.wait()   

    #print("********* do_post_process_onnx started")
    while True:
        #if (read_q.empty() == False):
        try:
            inference_dict = read_q.get(timeout= QUEUE_TIMEOUT)
        except Empty:
            with isRunningFlag.get_lock():
                #print("Do I lock here?")
                if isRunningFlag.value == False:
                    #print("Running flag is false")
                    break
                else:
                    print ("Error do_post_process_onnx timed-out")
        #ddprint("******** do_post_process_onnx Image received")
        start_time = time.time()
        inference_output = post_process_onnx.run(None, inference_dict)
        end_time = (time.time() - start_time)
        #print("---detection post-process time is: %s seconds ---" % end_time)
        write_q.put(inference_output)

    #print("Post_process thread exited")

def ocr4_infer_model_send(network_group,network_group_params, hef, read_q, write_q, isRunningFlag, startEvent, syncEvent, numCameras):

    # Create input and output virtual streams params
    # Quantized argument signifies whether or not the incoming data is already quantized.
    # Data is quantized by HailoRT if and only if quantized == False .
    input_vstreams_params = InputVStreamParams.make(network_group, quantized=True, format_type=FormatType.UINT8)
    # Define dataset params
    input_vstream_info = hef.get_input_vstream_infos()[0]

    startEvent.wait()
    #dprint("The network_group is: " + str(network_group))

    with InputVStreams(network_group, input_vstreams_params) as vstreams:
        vstream_to_buffer = {vstream: np.ndarray([1] + list(vstream.shape), dtype=vstream.dtype) for vstream in
                             vstreams}
        while (True):
            #ddprint("ocr4_infer_model_send")
            try:
                crop_list= read_q.get(timeout= QUEUE_TIMEOUT)
            except Empty:
                with isRunningFlag.get_lock():
                    #print("Do I lock here?")
                    if isRunningFlag.value == False:
                        #print("Running flag is false")
                        break
                    else:
                        print ("Error ocr4_infer_model_send timed-out")
            
            network_group.wait_for_activation(100)
            
            length=len(crop_list);
            for i in range(length):
                
                data = np.expand_dims(crop_list[i], axis=0)
                #data = crop_list[i]
                #data = np.moveaxis(crop_list[i], 1,-1)
                for vstream, _ in vstream_to_buffer.items():
                    vstream.send(data)
                # Set sync event to signal to the ocr4_infer_model_recv thread 
                # that inference output data will be ready soon
                if i== length-1:
                    write_q.put(length)

def ocr4_infer_model_recv(network_group,network_group_params, hef, read_q, write_q, isRunningFlag, startEvent, syncEvent, numCameras):

    # Create input and output virtual streams params
    # Quantized argument signifies whether or not the incoming data is already quantized.
    # Data is quantized by HailoRT if and only if quantized == False .
    output_vstreams_params = OutputVStreamParams.make(network_group, quantized=True, format_type=FormatType.UINT8)

    startEvent.wait()

    with OutputVStreams(network_group, output_vstreams_params) as vstreams:
        while (True):
            
            # Wait from the queue from the  ocr4_infer_model_send() thread
            try:
                num_crops= read_q.get(timeout= QUEUE_TIMEOUT)
            except Empty:
                with isRunningFlag.get_lock():
                    #print("Do I lock here?")
                    if isRunningFlag.value == False:
                        #print("Running flag is false")
                        return
                    else:
                        print ("Error ocr4_infer_model_recv timed-out")

            #ddprint("received OCR4")

            network_group.wait_for_activation(100)
            
            for i in range(num_crops):
                for vstream in vstreams:
                    data= vstream.recv()
                    write_q.put(data)


# -------------------------------- Processing Data and sending Data to the inference threads -------------------------------- #

# Processing the image for Plate_finder inferencing, sending plate_finder data to the inferencing thread, and nms on the results.
def detect_batch_img(batch_img,nms_max_box_num,nms_iou_threshold,nms_score_threshold, post_process_onnx=[], imageQueue=[], outQueue=[]):

    #boxes, scores, classes, valid_detections = detect_batch_img(batch_img, hef, post_process_onnx,imageQueue=imageQueue, outQueue=outQueue)
    #boxes, scores, classes = tta_nms(boxes, scores, classes, valid_detections)

    #print("Processing the HEF model")
    #time.sleep(1)
    #print(img)
    inference_output= []
    batch_boxes=[]
    batch_scores=[]
    batch_classes=[]
    batch_valid_detections=[]

    # Below processing is for onnx model
    start_detect_time = time.time()
    for img in batch_img:
        data =  img.astype(np.uint8)
        imageQueue.put(data)
    
    #for img in batch_img:
        #print("***** Input image sent from detect_batch_img")
    #    inference_output.append(outQueue.get())
    
    #start_time = time.time()
    
    for i in range(len(batch_img)):
        inference_output = outQueue.get()
        pre_nms_decoded_boxes = []
        pre_nms__scores = []
        #print("***** Output received in detect_batch_img")
        pre_nms_decoded_boxes.append(inference_output[0][0][:][:])
        pre_nms__scores.append(inference_output[1][0][:][:])

        pre_nms_decoded_boxes = np.array(pre_nms_decoded_boxes)
        pre_nms__scores = np.array(pre_nms__scores)

        boxes, scores, classes, valid_detections = yolov4_nms('diou_nms')(pre_nms_decoded_boxes, pre_nms__scores, nms_max_box_num,nms_iou_threshold,nms_score_threshold)
        batch_boxes.append(boxes)
        batch_scores.append(scores)
        batch_classes.append(classes)
        batch_valid_detections.append(valid_detections)    
    
    #end_time = (time.time() - start_time)/len(batch_img)
    
    #dprint("---detection time yolov4_nms is: %s seconds ---" % end_time)

    #end_time = (time.time() - start_detect_time)/len(batch_img)
    #dprint("---Overall detection time (Hailo + post-process + yolov4_nms) is: %s seconds ---" % end_time)

    return batch_boxes, batch_scores, batch_classes, batch_valid_detections
    # pre_nms_decoded_boxes,pre_nms__scores = model(img)
    # pre_nms_decoded_boxes = pre_nms_decoded_boxes.numpy()
    # pre_nms__scores = pre_nms__scores.numpy()
    # return pre_nms_decoded_boxes, pre_nms__scores

# Processing Plate_finder crops for OCR4 inferencing, sending the crops to OCR4 Inference Thread, and parsing the inference to a plate string
def detech_ocr4(batch_boxes, batch_img_copy, ocr4_image_size, batch_scores,nms_score_threshold,batch_x_scale,batch_y_scale,batch_test_value, batch_img_orig_size,ocr4_inQueue,ocr4_outQueue):
    
    SYMBOLS = string.digits + string.ascii_uppercase + '*'
    batch_string_list= []
    crop_list=[]
    #start_time_ocr  = time.time()
    #print("The length of bboxes are:" + str(len(boxes)))
    # Processing Plate_finder crops. It scales the crop bounding box coordinates back to the original image and crops it from there.
    for box_idx in range(len(batch_boxes)):
        
        string_list = []

        boxes= batch_boxes[box_idx]
        img_copy= batch_img_copy[box_idx]
        img_orig_size= batch_img_orig_size[box_idx]
        test_value= batch_test_value[box_idx]
        x_scale= batch_x_scale[box_idx]
        y_scale= batch_y_scale[box_idx]

        if len(boxes) > 0:
            for i in range(len(boxes)):

                x1y1 = (boxes[i][0:2] * img_copy.shape[0:2][::-1]).astype(np.int)
                x2y2 = (boxes[i][2:4] * img_copy.shape[0:2][::-1]).astype(np.int)
                
                box = [x1y1[0],x1y1[1],x2y2[0],x2y2[1]]

                
                #print("The box is: " + str(box))
                orig_x1 = int(np.round(box[0] * (x_scale)))
                #print("The orig_x1 is: " + str(orig_x1))
                orig_y1 = int(np.round((box[1] * y_scale)-(test_value * (y_scale / 2))))
                #print("The orig_y1 is: " + str(orig_y1))
                if (orig_y1 < 0 ):
                    orig_y1 = 0
                #print("The orig_y1 after check is: " + str(orig_y1))
                orig_x2 = int(np.round(box[2] * (x_scale)))
                #print("The orig_x2 is: " + str(orig_x2))
                orig_y2 = int(np.round((box[3] * y_scale)-(test_value * (y_scale / 2))))
                #print("The orig_y2 is: " + str(orig_y2))
                if(orig_y2 > img_orig_size.shape[0]):
                    orig_y2 = img_orig_size.shape[0]
                #print("The orig_y2 after check is: " + str(orig_y2))

                # Crop the lp_finder detection on the original input image.
                crop = img_orig_size[orig_y1:orig_y2, orig_x1:orig_x2]
                #print("The crop is: " + str(crop.shape))

                start_time = time.time()
                crop = cv2.cvtColor(crop, cv2.COLOR_BGR2RGB)
                crop = cv2.resize(crop,dsize=(ocr4_image_size,ocr4_image_size))
                crop = np.array(crop, dtype=np.uint8)
                end_time = (time.time() - start_time)
                #print("---cvtColor+resize time is: %s seconds ---" % end_time)
                crop_list.append(crop)

    # Sending the crop list to OCR4 input queue
    #cv2.waitKey(0)
    ocr4_inQueue.put(crop_list)
    count= 0

    for box_idx in range(len(batch_boxes)):
            
        string_list = []

        boxes= batch_boxes[box_idx]
        img_copy= batch_img_copy[box_idx]
        img_orig_size= batch_img_orig_size[box_idx]
        test_value= batch_test_value[box_idx]
        x_scale= batch_x_scale[box_idx]
        y_scale= batch_y_scale[box_idx]

        if len(boxes) > 0:
            for i in range(len(boxes)):
                
                # Getting OCR4 results back from the OCR4 Inference Thread
                ocr4_inference_output= ocr4_outQueue.get()
                reshaped = tf.reshape(ocr4_inference_output,(10,37))
                final = tf.argmax(reshaped,1)
                #SYMBOLS = string.digits + string.ascii_uppercase + '*'
                final_string =''.join([SYMBOLS[x] for x in final])
                string_list.append(final_string)
                #dprint("The OCR4 detection is: " + final_string)
                count= count + 1

        else:
            string_list = []

        batch_string_list.append(string_list)
    
    #end_time = (time.time() - start_time_ocr)
    #dprint("---OCR4 time is: %s seconds  for %d plates---" % (end_time, count))

    return batch_string_list

def tta_nms(batch_boxes,batch_scores,batch_classes,batch_valid_detections,nms,nms_max_box_num,nms_iou_threshold,nms_score_threshold):
    out_batch_boxes=[]
    out_batch_scores=[]
    out_batch_classes=[]
    for i in range(len(batch_boxes)):
        all_boxes = []
        all_scores = []
        all_classes = []
        batch_index = 0
        valid_boxes = batch_boxes[i][batch_index][0:batch_valid_detections[i][batch_index]]
        #valid_boxes[:, (0, 2)] = (1.-valid_boxes[:,(2,0)])
        all_boxes.append(valid_boxes)
        all_scores.append(batch_scores[i][batch_index][0:batch_valid_detections[i][batch_index]])
        all_classes.append(batch_classes[i][batch_index][0:batch_valid_detections[i][batch_index]])
        for batch_index in range(1,batch_boxes[i].shape[0]):
            all_boxes.append(batch_boxes[i][batch_index][0:batch_valid_detections[i][batch_index]])
            all_scores.append(batch_scores[i][batch_index][0:batch_valid_detections[i][batch_index]])
            all_classes.append(batch_classes[i][batch_index][0:batch_valid_detections[i][batch_index]])
        all_boxes = np.concatenate(all_boxes,axis=0)
        all_scores = np.concatenate(all_scores, axis=0)
        all_classes = np.concatenate(all_classes, axis=0)
        all_boxes,all_scores,all_classes = np.array(all_boxes), np.array(all_scores), np.array(all_classes)
        boxes, scores, classes, valid_detections = NonMaxSuppression.diou_nms_np_tta(np.expand_dims(all_boxes,0),np.expand_dims(all_scores,0),np.expand_dims(all_classes,0),nms_max_box_num,nms_iou_threshold,nms_score_threshold)
        boxes, scores, classes, valid_detections = np.squeeze(boxes), np.squeeze(scores), np.squeeze(classes), np.squeeze(valid_detections)
        out_batch_boxes.append(boxes[:valid_detections])
        out_batch_scores.append(scores[:valid_detections])
        out_batch_classes.append(classes[:valid_detections])
        ##print("The length of boxes are: {0}".format(len(boxes)))
        ##print("The shape of boxes are: {0}".format(boxes.shape))
        ##print("The boxes look like this: {0}".format(boxes))

    return out_batch_boxes, out_batch_scores, out_batch_classes



# -------------------------------- Main Process Thread -------------------------------- #
def mainProcess(plate_finder_network_group, plate_finder_network_group_params, ocr4_network_group, ocr4_network_group_params, max_num_pics,img_size,ocr4_image_size,pic_dir,loaded_img_queue, post_process_onnx, imageQueue, outQueue, ocr4_inQueue,ocr4_outQueue, isRunningFlag, startEvent,startEvent_ocr4, twoDeviceFlag, numCameras):
#max_num_pics, pic_dir,img_size,hef, post_process_onnx, imageQueue, outQueue, isRunningFlag, startEvent
    img_list = os.listdir(pic_dir)
    #actual_img_list = []
    #for filename in img_list:
    #    loaded_img = cv2.imread(os.path.join(pic_dir,filename))
    #    actual_img_list.append(loaded_img)
    
    write_csv = True
    nms = 'diou_nms'
    nms_max_box_num = 6
    nms_iou_threshold = 0.2
    nms_score_threshold = 0.2
    
    if write_csv:
        perf_log='perf_infer5.csv'

        if os.path.exists(perf_log):
            os.remove(perf_log)
    
    if max_num_pics!=0 :
        img_list = img_list[0:min(max_num_pics, len(img_list))]
    
    counter = 0
    averVal = 0
    #loaded_img_list = loaded_img_queue.get()

    for img_idx in range(0, len(img_list), numCameras):
        #dprint("----------------------------------------------------------")
        
        batch_new_y= []
        batch_img_copy= []
        batch_img_orig_size=[]
        batch_img_from_disk=[]
        y_ = []
        x_ = []
        batch_test_value = []
        aug_imgs_test = []

        for cam_idx in range(numCameras):
            img = cv2.imread(os.path.join(pic_dir, img_list[img_idx + cam_idx]))
            #img = loaded_img_list[img_idx + cam_idx]
            batch_img_from_disk.append(img)
        
        start_time = time.time()
        for cam_idx in range(numCameras):
            ##dprint(str(counter + cam_idx) + ": " + img_list[img_idx + cam_idx])
            img = batch_img_from_disk[cam_idx]
            batch_img_orig_size.append(img)
            y_.append(img.shape[0])
            x_.append(img.shape[1])
            img_ori, _, _,new_y = resize_img_detect(img,img_size)
            batch_new_y.append(new_y)
            #img_copy = img_ori
            batch_img_copy.append(img_ori)
            batch_test_value.append((img_size - new_y))
            aug_imgs_test.append(img_ori)

        batch_img = np.array(aug_imgs_test)


        


        startEvent.set()
        # Plate Finder Inference and Onnx Post Processing
        #If we have 2 devices, no need to activate the network at every frame as each network goes to a separate device
        if twoDeviceFlag == 1:
            batch_boxes, batch_scores, batch_classes, batch_valid_detections = detect_batch_img(batch_img, nms_max_box_num,nms_iou_threshold,nms_score_threshold, post_process_onnx,
                                                                            imageQueue=imageQueue, outQueue=outQueue)
        else:
            with plate_finder_network_group.activate(plate_finder_network_group_params):
                batch_boxes, batch_scores, batch_classes, batch_valid_detections = detect_batch_img(batch_img, nms_max_box_num,nms_iou_threshold,nms_score_threshold, post_process_onnx,
                                                                                imageQueue=imageQueue, outQueue=outQueue)
                                                               
        #start_tta_nms_time = time.time()
        batch_boxes, batch_scores, batch_classes = tta_nms(batch_boxes, batch_scores, batch_classes, batch_valid_detections,nms,nms_max_box_num,nms_iou_threshold,nms_score_threshold)
        #end_time = (time.time() - start_tta_nms_time)/numCameras
        ##dprint("---tta_nms is: %s seconds ---" % end_time)

        batch_y_scale = np.divide(y_ , batch_new_y)
        batch_x_scale = np.divide(x_ , img_size) 
        
        #batch_test_value = [(img_size - new_y) for new_y in batch_new_y]

        # OCR4 Inference. Note I tried using the original startEvent and I still get run time errors.
        startEvent_ocr4.set()
        if twoDeviceFlag == 1:
            batch_string_list = detech_ocr4(batch_boxes,batch_img_copy,ocr4_image_size,batch_scores,nms_score_threshold,batch_x_scale,batch_y_scale,batch_test_value,batch_img_orig_size,ocr4_inQueue,ocr4_outQueue)
        else:
            with ocr4_network_group.activate(ocr4_network_group_params):
                batch_string_list = detech_ocr4(batch_boxes,batch_img_copy,ocr4_image_size,batch_scores,nms_score_threshold,batch_x_scale,batch_y_scale,batch_test_value,batch_img_orig_size,ocr4_inQueue,ocr4_outQueue)
        
        counter = counter + 1

        end_time = (time.time() - start_time)/numCameras
        #dprint("---Processing time is: %s seconds ---" % end_time)

        
        if write_csv:
            with open(perf_log, 'a') as perf_file:
                for cam_idx in range(numCameras):
                    perf_file.write(str(counter)+','+ img_list[img_idx + cam_idx] +','+str(batch_string_list[cam_idx]).replace(',',':') +','+str(end_time)+'\n')
        
        averVal= averVal + end_time

    with isRunningFlag.get_lock():
        isRunningFlag.value= False

    averVal= averVal/counter
    print("------> Average fps is: %s fps ---" % (1/averVal))
    #print ("mainProcess exited")


def image_load_here(image_path,loaded_img_queue):
    actual_img_list = []
    img_path_list = os.listdir(image_path)
    for filename in img_path_list:
        loaded_img = cv2.imread(os.path.join(image_path,filename))
        actual_img_list.append(loaded_img)
    
    loaded_img_queue.put(actual_img_list)


# Main Function.
def main():
    #pip install opencv-python==4.5.4.60

    # I converted the arguments to variables, set them below.
    pic_dir = "./lp_test_data"
    max_num_pics = 0
    numCameras = 4
    plate_finder_hef = "./lp_finder_hailo/lp_finder_3.hef"
    post_process_onnx_path = "./lp_finder_hailo/lp_finder_3_post.onnx"
    hef_output_dir = "./lp_finder_3_results"
    
    plate_finder_image_size = 320
    ocr4_image_size = 150

    ocr4_hef_path = "./ocr4_model_hailo/ResNet.hef"


    isRunningFlag= Value('i', True)
    if os.path.isdir(hef_output_dir) == False:
        os.mkdir(hef_output_dir)

    hef = HEF(plate_finder_hef)
    ocr4_hef = HEF(ocr4_hef_path)

    # Initializing Queues and Onnx runtime
    post_process_onnx = onnxruntime.InferenceSession(post_process_onnx_path)
    imageQueue = Queue()
    hefOutQueue = Queue()
    outQueue = Queue()
    ocr4_inQueue = Queue()
    ocr4_infoQueue= Queue()
    ocr4_outQueue = Queue()
    #loaded_img_queue = Queue()
    loaded_img_queue = []
    
    # Start Events
    startEvent= Event()
    startEvent_ocr4 = Event()
    syncEvent_plate_finder= Event()
    syncEvent_ocr4= Event()
    
    device_infos=PcieDevice().scan_devices()
    target = [0,0]
    target[0] = PcieDevice(device_infos[0])
    twoDeviceFlag = 0
    if len(device_infos) > 1:
        target[1] = PcieDevice(device_infos[1])
        twoDeviceFlag = 1

    # Initializing OCR4 and Plate_finder Models
    ocr4_configure_params = ConfigureParams.create_from_hef(ocr4_hef, interface=HailoStreamInterface.PCIe)
    ocr4_network_group = target[0].configure(ocr4_hef, ocr4_configure_params)[0]
    ocr4_network_group_params = ocr4_network_group.create_params()

    configure_params = ConfigureParams.create_from_hef(hef, interface=HailoStreamInterface.PCIe)
    network_group = target[twoDeviceFlag].configure(hef, configure_params)[0]
    network_group_params = network_group.create_params()

    #load_images_to_memory_process = Process(target=image_load_here,args=([pic_dir,loaded_img_queue]))
    
    #loaded_img_list = loaded_img_queue.get()
    
    # Main Process
    main_process = Process(target=mainProcess, args=(network_group, network_group_params, ocr4_network_group, ocr4_network_group_params, max_num_pics, plate_finder_image_size, ocr4_image_size,pic_dir,loaded_img_queue, post_process_onnx, imageQueue, outQueue, ocr4_inQueue,ocr4_outQueue, isRunningFlag, startEvent,startEvent_ocr4, twoDeviceFlag, numCameras))

    # Plate Finder Inference and Post Process Threads
    infer_send_process = Process(target=infer_model_send, args=(
    network_group, network_group_params, hef, post_process_onnx, imageQueue, hefOutQueue, isRunningFlag, startEvent, syncEvent_plate_finder, numCameras))
    infer_recv_process = Process(target=infer_model_recv, args=(
        network_group, network_group_params, hef, post_process_onnx, imageQueue, hefOutQueue, isRunningFlag,
        startEvent, syncEvent_plate_finder, numCameras))

    post_process = Process(target=do_post_process_onnx, args=(hefOutQueue, outQueue, post_process_onnx, isRunningFlag, startEvent))

    #OCR4 Inference Thread
    ocr4_infer_send_process = Process(target=ocr4_infer_model_send, args=(ocr4_network_group, ocr4_network_group_params, ocr4_hef, ocr4_inQueue, ocr4_infoQueue, isRunningFlag, startEvent_ocr4, syncEvent_ocr4, numCameras))
    ocr4_infer_recv_process = Process(target=ocr4_infer_model_recv, args=(ocr4_network_group, ocr4_network_group_params, ocr4_hef, ocr4_infoQueue, ocr4_outQueue, isRunningFlag, startEvent_ocr4, syncEvent_ocr4, numCameras))

    # Starting Threads
    if twoDeviceFlag== 1:
        with ocr4_network_group.activate(ocr4_network_group_params):
            with network_group.activate(network_group_params):
                infer_send_process.start()
                infer_recv_process.start()
                post_process.start()
                ocr4_infer_send_process.start()
                ocr4_infer_recv_process.start()
                #load_images_to_memory_process.start()
                main_process.start()

                infer_send_process.join()
                #dprint("Infer send process exited")
                infer_recv_process.join()
                #dprint("Infer recv process exited")
                post_process.join()
                #dprint("Post process exited")
                ocr4_infer_send_process.join()
                #dprint("ocr4 send process exited")
                ocr4_infer_recv_process.join()
                #dprint("ocr4 recv process exited")
                #load_images_to_memory_process.join()
                main_process.join()
                #dprint("main process exited")

    else:
                infer_send_process.start()
                infer_recv_process.start()
                post_process.start()
                ocr4_infer_send_process.start()
                ocr4_infer_recv_process.start()
                #load_images_to_memory_process.start()
                main_process.start()

                infer_send_process.join()
                #dprint("Infer send process exited")
                infer_recv_process.join()
                #dprint("Infer recv process exited")
                post_process.join()
                #dprint("Post process exited")
                ocr4_infer_send_process.join()
                #dprint("ocr4 send process exited")
                ocr4_infer_recv_process.join()
                #dprint("ocr4 recv process exited")
                #load_images_to_memory_process.join()
                main_process.join()
                #dprint("main process exited")

if __name__ == '__main__':

    main()
