#!/bin/bash

declare -A COMPILER=( [x86_64]=/usr/bin/gcc
		      [aarch64]=/usr/bin/aarch64-linux-gnu-g++ )
		      
# for ARCH in aarch64
# do
#     echo "-I- Building ${ARCH}"
#     mkdir -p build/${ARCH}
#     cmake -H. -Bbuild/${ARCH} -DCMAKE_CXX_COMPILER=${COMPILER[${ARCH}]}
#     cmake --build build/${ARCH}
# done

for ARCH in x86_64 
do
    echo "-I- Building ${ARCH}"
    mkdir -p build/${ARCH}
    cmake -H. -Bbuild/${ARCH}
    cmake --build build/${ARCH}
done

if [[ -f "hailort.log" ]]; then
    rm hailort.log
fi
