#include "hailo/hailort.hpp"

#include <iostream>
#include <chrono>
#include <mutex>
#include <condition_variable>
#include <filesystem>

extern "C"
{
#include "common.h"
#include "hailo/hailort.h"
#include <sys/stat.h>
}

#define MAX_EDGE_LAYERS (4)
#define MAX_DEVICES (16)
#define MAX_NUM_PICS (0)
#define NUM_CAMERAS (4)
#define PLATE_FINDER_HEF_PATH "./lp_finder_hailo/lp_finder_3.hef"
#define PLATE_FINDER_HEF_IDX 1
#define POST_PROCESS_ONNX_PATH "./lp_finder_hailo/lp_finder_3_post.onnx"
#define HEF_OUTPUT_DIR "./lp_finder_3_results"
#define PIC_DIR "./lp_test_data"
#define PLATE_FINDER_IMAGE_SIZE 320
#define OCR4_IMAGE_SIZE 150
#define OCR4_HEF_PATH "./ocr4_model_hailo/ResNet.hef"
#define OCR4_HEF_IDX 0

#define MSEC (1)
#define SEC (1000*MSEC)
#define QUEUE_TIMEOUT (1*SEC)
#define TIMEOUT_SLICE (1*MSEC)

#define RESET   "\033[0m"
#define BLACK   "\033[30m"      /* Black */
#define RED     "\033[31m"      /* Red */
#define GREEN   "\033[32m"      /* Green */
#define YELLOW  "\033[33m"      /* Yellow */
#define BLUE    "\033[34m"      /* Blue */
#define MAGENTA "\033[35m"      /* Magenta */
#define CYAN    "\033[36m"      /* Cyan */
#define WHITE   "\033[37m"      /* White */
#define BOLDBLACK   "\033[1m\033[30m"      /* Bold Black */
#define BOLDRED     "\033[1m\033[31m"      /* Bold Red */
#define BOLDGREEN   "\033[1m\033[32m"      /* Bold Green */
#define BOLDYELLOW  "\033[1m\033[33m"      /* Bold Yellow */
#define BOLDBLUE    "\033[1m\033[34m"      /* Bold Blue */
#define BOLDMAGENTA "\033[1m\033[35m"      /* Bold Magenta */
#define BOLDCYAN    "\033[1m\033[36m"      /* Bold Cyan */
#define BOLDWHITE   "\033[1m\033[37m"      /* Bold White */

using namespace hailort;
using ThreadsVector = std::vector<std::unique_ptr<std::thread>>;
using StatusVector = std::vector<std::shared_ptr<hailo_status>>;
using hailort::Device;
using hailort::Hef;
using hailort::Expected;
using hailort::make_unexpected;
using hailort::ConfiguredNetworkGroup;
using hailort::VStreamsBuilder;
using hailort::InputVStream;
using hailort::OutputVStream;
using hailort::MemoryView;

class SyncObject final {

public:
    explicit SyncObject(void) : isRunningFlag(false), plate_finder_send_queue_ready(false),\
    plate_finder_post_process_read_queue_ready(false), plate_finder_post_process_write_queue_ready(false),\
    startEvent_plate_finder(false), startEvent_ocr4(false), syncEvent_plate_finder(false), syncEvent_ocr4(false)
        {};
    
    std::atomic_bool isRunningFlag;
    std::atomic_bool plate_finder_send_queue_ready;
    std::atomic_bool plate_finder_post_process_read_queue_ready;
    std::atomic_bool plate_finder_post_process_write_queue_ready;
    std::atomic_bool startEvent_plate_finder;
    std::atomic_bool startEvent_ocr4;
    std::atomic_bool syncEvent_plate_finder;
    std::atomic_bool syncEvent_ocr4;
};


void plate_finder_send(
    hailo_configured_network_group &plate_finder_network_group,\
    hailo_hef &hef, 
    std::vector<std::unique_ptr<uint8_t>> &imageQueue, std::vector<std::unique_ptr<uint8_t>> &hefOutQueue, \
    SyncObject &syncObj, \
    uint32_t numCameras
) {
    hailo_status status = HAILO_UNINITIALIZED;
    /*
    Create input and output virtual streams params
    Quantized argument signifies whether or not the incoming data is already quantized.
    Data is quantized by HailoRT if and only if quantized == False .
    */
    hailo_input_vstream_params_by_name_t input_vstream_params[MAX_EDGE_LAYERS] = {0};
    hailo_input_vstream input_vstreams[MAX_EDGE_LAYERS] = {NULL};
    size_t input_vstreams_size = MAX_EDGE_LAYERS;

    status = hailo_make_input_vstream_params(plate_finder_network_group, true, HAILO_FORMAT_TYPE_UINT8,
        input_vstream_params, &input_vstreams_size);
    REQUIRE_SUCCESS(status, l_quit0, "plate_finder_send failed making input virtual stream params");
    REQUIRE_ACTION(input_vstreams_size <= MAX_EDGE_LAYERS, status = HAILO_INVALID_ARGUMENT, l_quit0, 
            "Unexpected number of input vstreams for plate_finder_send");

    status = hailo_create_input_vstreams(plate_finder_network_group, input_vstream_params, input_vstreams_size, input_vstreams);
    REQUIRE_SUCCESS(status, l_quit0, "plate_finder_send failed creating virtual input streams\n");

    /* Implement synchronization scheme in https://www.modernescpp.com/index.php/the-atomic-boolean
    to block this thread until the shared variable isRunningFlag is true */
    
    // Wait until main() sets isRunningFlag to true
    while ( !syncObj.startEvent_plate_finder.load() ) {
        std::this_thread::yield();
    }
    
    while (true) {
        for (uint32_t i=0; i < numCameras; i++) {

            // Implement queue read with TIMEOUT
            int32_t remaining_ms= QUEUE_TIMEOUT;
            while (remaining_ms>0) {
                if (syncObj.plate_finder_send_queue_ready.load()== true)
                    break;
                std::this_thread::sleep_for(std::chrono::milliseconds(TIMEOUT_SLICE));
                remaining_ms -= TIMEOUT_SLICE;
            }

            if (syncObj.plate_finder_send_queue_ready.load()== false) {
                if (syncObj.isRunningFlag.load()== false) {
                    goto l_quit0_release_vstreams;
                }
                else {
                    std::cerr << BOLDRED << "Error infer_model_send timed-out" << std::endl << RESET;
                }
            }
            else {
                syncObj.plate_finder_send_queue_ready= false;

                std::cout << "In plate_finder_send() camera #" << i << std::endl;

                // Wait for hef to be activated to send data
                hailo_wait_for_network_group_activation(plate_finder_network_group, 100);
                
                // Write data to Hailo-8
                // .... <--- Insert mutex lock before the queue is read
                status= hailo_vstream_write_raw_buffer(input_vstreams[0], imageQueue.data(), imageQueue.size()); // This line is incorrect as imageQueue.data() will return a pointer to an array of pointer, not an array of pixels
                REQUIRE_SUCCESS(status, l_quit0_release_vstreams, "plate_finder_send failed sending to vstream");
                // .... <--- Insert mutex unlock after the queue is read

                // If last camera, set sync event to signal to the infer_model_rcv thread 
                // that inference output data will be ready soon
                if (i== numCameras-1)
                    syncObj.syncEvent_plate_finder= true;
            }
        }
    }

l_quit0_release_vstreams:
    (void)hailo_release_input_vstreams(input_vstreams, input_vstreams_size);
l_quit0:
    return;
}

void plate_finder_recv(
    hailo_configured_network_group &plate_finder_network_group,\
    hailo_hef &hef, 
    std::vector<std::unique_ptr<uint8_t>> &imageQueue, std::vector<std::unique_ptr<uint8_t>> &hefOutQueue, \
    SyncObject &syncObj, \
    uint32_t numCameras
) {
    std::vector<std::vector<uint8_t>> arr_buff;
    hailo_status status = HAILO_UNINITIALIZED;
    /*
    Create input and output virtual streams params
    Quantized argument signifies whether or not the incoming data is already quantized.
    Data is quantized by HailoRT if and only if quantized == False .
    */
    hailo_output_vstream_params_by_name_t output_vstream_params[MAX_EDGE_LAYERS] = {0};
    hailo_output_vstream output_vstreams[MAX_EDGE_LAYERS] = {NULL};
    size_t output_vstreams_size = MAX_EDGE_LAYERS;

    status = hailo_make_output_vstream_params(plate_finder_network_group, false, HAILO_FORMAT_TYPE_FLOAT32,
        output_vstream_params, &output_vstreams_size);
    REQUIRE_SUCCESS(status, l_quit1, "plate_finder_send failed making output virtual stream params");
    REQUIRE_ACTION(output_vstreams_size <= MAX_EDGE_LAYERS, status = HAILO_INVALID_ARGUMENT, l_quit1, 
            "Unexpected number of output vstreams for plate_finder_recv");

    status = hailo_create_output_vstreams(plate_finder_network_group, output_vstream_params, output_vstreams_size, output_vstreams);
    REQUIRE_SUCCESS(status, l_quit1, "plate_finder_send failed creating virtual outputstreams\n");

    /* Create buffers that will hold the data written by Hailo-8 */
    for (int32_t vstreamIdx= 0; vstreamIdx < output_vstreams_size; vstreamIdx++) {
        size_t output_frame_size;
        hailo_get_output_vstream_frame_size(output_vstreams[vstreamIdx], &output_frame_size);
        std::vector<uint8_t> buff(output_frame_size);
        arr_buff.emplace_back(buff);
    }

    /* Implement synchronization scheme in https://www.modernescpp.com/index.php/the-atomic-boolean
    to block this thread until the shared variable isRunningFlag is true */
    
    // Wait until main() sets isRunningFlag to true
    while ( !syncObj.startEvent_plate_finder.load() ) {
        std::this_thread::yield();
    }
    
    while (true) {

        // Wait from the sync event signal from the plate_finder_send() thread
        int32_t remaining_ms= QUEUE_TIMEOUT;
        while (remaining_ms>0) {
            if (syncObj.syncEvent_plate_finder.load()== true)
                break;
            std::this_thread::sleep_for(std::chrono::milliseconds(TIMEOUT_SLICE));
            remaining_ms -= TIMEOUT_SLICE;
        }

        if (syncObj.syncEvent_plate_finder.load()== true) {
            // Wait for hef to be activated to send data
            hailo_wait_for_network_group_activation(plate_finder_network_group, 100);
                
            for (uint32_t i=0; i < numCameras; i++) {

                std::cout << "In plate_finder_recv() camera #" << i << std::endl;

                // Read data from Hailo-8
                for (int32_t vstreamIdx= 0; vstreamIdx < output_vstreams_size; vstreamIdx++) {
                    status= hailo_vstream_read_raw_buffer(output_vstreams[vstreamIdx], arr_buff[vstreamIdx].data(), arr_buff[vstreamIdx].size()); // This line is incorrect as imageQueue.data() will return a pointer to an array of pointer, not an array of pixels
                    REQUIRE_SUCCESS(status, l_quit1_release_vstreams, "plate_finder_recv failed reading from vstream");
                    // ... <--- Insert statement that appends the data just read into a format compatible with ONNX-RT
                }

                // Send data to the post processing queue
                // .... <--- Insert mutex lock before the queue is written
                // ...  <--- Insert function that fills the post processing queue
                // .... <--- Insert mutex unlock after the queue is written
                syncObj.plate_finder_post_process_read_queue_ready= true;

                // If last camera, set sync event to signal to the infer_model_rcv thread 
                // that inference output data will be ready soon
                if (i== numCameras-1)
                    syncObj.syncEvent_plate_finder= false;
            }
        }

        if (syncObj.isRunningFlag.load()== false) {
            break;
        }
    }

l_quit1_release_vstreams:
    (void)hailo_release_output_vstreams(output_vstreams, output_vstreams_size);
l_quit1:
    return;
}

void plate_finder_post(
    std::vector<std::unique_ptr<uint8_t>> &hefOutQueue, std::vector<std::unique_ptr<uint8_t>> &outQueue, \
    SyncObject &syncObj
) {
    /* Implement synchronization scheme in https://www.modernescpp.com/index.php/the-atomic-boolean
    to block this thread until the shared variable isRunningFlag is true */
    
    // Wait until main() sets isRunningFlag to true
    while ( !syncObj.startEvent_plate_finder.load() ) {
        std::this_thread::yield();
    }
    
    // Implement queue read with TIMEOUT
    int32_t remaining_ms= QUEUE_TIMEOUT;
    while (remaining_ms>0) {
        if (syncObj.plate_finder_post_process_read_queue_ready.load()== true)
            break;
        std::this_thread::sleep_for(std::chrono::milliseconds(TIMEOUT_SLICE));
        remaining_ms -= TIMEOUT_SLICE;
    }

    if (syncObj.plate_finder_post_process_read_queue_ready.load()== false) {
        if (syncObj.isRunningFlag.load()== false) {
            goto l_quit_post;
        }
        else {
            std::cerr << BOLDRED << "Error do_post_process_onnx timed-out" << std::endl << RESET;
        }
    }
    else {
        syncObj.plate_finder_post_process_read_queue_ready= false;
        // ...  <--- Insert mutex lock before the post processing hefOutQueue queue is read
        // ...  <--- Insert function that reads the post processing hefOutQueue queue
        // ...  <--- Insert mutex unlock after the post processing hefOutQueue queue is read
        // ...  <--- Insert function that calls ONNX_RT post_process_onnx.run(
    
        // ...  <--- Insert mutex lock before the post processing outQueue queue is written
        // ...  <--- Insert function that writes the post processing outQueuequeue
        // ...  <--- Insert mutex unlock after the post processing outQueue queue is written
        syncObj.plate_finder_post_process_write_queue_ready= true;
    }

    std::cout << "In plate_finder_post()" << std::endl;

l_quit_post:
    return;
}

void ocr4_infer_model_send(
    hailo_configured_network_group &ocr4_network_group,\
    hailo_hef &hef, 
    std::vector<std::unique_ptr<uint8_t>> &ocr4_inQueue, std::vector<std::unique_ptr<uint8_t>> &ocr4_infoQueue, \
    SyncObject &syncObj, \
    uint32_t num_cameras
) {
    hailo_status status = HAILO_UNINITIALIZED;
    /*
    Create input and output virtual streams params
    Quantized argument signifies whether or not the incoming data is already quantized.
    Data is quantized by HailoRT if and only if quantized == False .
    */
    hailo_input_vstream_params_by_name_t input_vstream_params[MAX_EDGE_LAYERS] = {0};
    hailo_input_vstream input_vstreams[MAX_EDGE_LAYERS] = {NULL};
    size_t input_vstreams_size = MAX_EDGE_LAYERS;

    status = hailo_make_input_vstream_params(ocr4_network_group, true, HAILO_FORMAT_TYPE_UINT8,
        input_vstream_params, &input_vstreams_size);
    REQUIRE_SUCCESS(status, l_quit2, "ocr4_infer_model_send failed making input virtual stream params");
    REQUIRE_ACTION(input_vstreams_size <= MAX_EDGE_LAYERS, status = HAILO_INVALID_ARGUMENT, l_quit2, 
            "Unexpected number of input vstreams for ocr4_infer_model_send");

    status = hailo_create_input_vstreams(ocr4_network_group, input_vstream_params, input_vstreams_size, input_vstreams);
    REQUIRE_SUCCESS(status, l_quit2, "ocr4_infer_model_send failed creating virtual input streams\n");

    /* Implement synchronization scheme in https://www.modernescpp.com/index.php/the-atomic-boolean
    to block this thread until the shared variable isRunningFlag is true */
    
    // Wait until main() sets isRunningFlag to true
    while ( !syncObj.startEvent_ocr4.load() ) {
        std::this_thread::yield();
    }
    
    std::cout << "In ocr4_infer_model_send()" << std::endl;

l_quit2_release_vstreams:
    (void)hailo_release_input_vstreams(input_vstreams, input_vstreams_size);
l_quit2:
    return;
}

void ocr4_infer_model_recv(
    hailo_configured_network_group &ocr4_network_group,\
    hailo_hef &hef, 
    std::vector<std::unique_ptr<uint8_t>> &ocr4_infoQueue, std::vector<std::unique_ptr<uint8_t>> &ocr4_outQueue, \
    SyncObject &syncObj, \
    uint32_t num_cameras
) {
    hailo_status status = HAILO_UNINITIALIZED;
    /*
    Create input and output virtual streams params
    Quantized argument signifies whether or not the incoming data is already quantized.
    Data is quantized by HailoRT if and only if quantized == False .
    */
    hailo_output_vstream_params_by_name_t output_vstream_params[MAX_EDGE_LAYERS] = {0};
    hailo_output_vstream output_vstreams[MAX_EDGE_LAYERS] = {NULL};
    size_t output_vstreams_size = MAX_EDGE_LAYERS;

    status = hailo_make_output_vstream_params(ocr4_network_group, true, HAILO_FORMAT_TYPE_UINT8,
        output_vstream_params, &output_vstreams_size);
    REQUIRE_SUCCESS(status, l_quit3, "ocr4_infer_model_recv failed making output virtual stream params");
    REQUIRE_ACTION(output_vstreams_size <= MAX_EDGE_LAYERS, status = HAILO_INVALID_ARGUMENT, l_quit3, 
            "Unexpected number of output vstreams for ocr4_infer_model_recv");

    status = hailo_create_output_vstreams(ocr4_network_group, output_vstream_params, output_vstreams_size, output_vstreams);
    REQUIRE_SUCCESS(status, l_quit3, "ocr4_infer_model_send failed creating virtual ouptut streams\n");

    /* Implement synchronization scheme in https://www.modernescpp.com/index.php/the-atomic-boolean
    to block this thread until the shared variable isRunningFlag is true */
    
    // Wait until main() sets isRunningFlag to true
    while ( !syncObj.startEvent_ocr4.load() ) {
        std::this_thread::yield();
    }
    
    std::cout << "In ocr4_infer_model_recv()" << std::endl;

l_quit3_release_vstreams:
    (void)hailo_release_output_vstreams(output_vstreams, output_vstreams_size);
l_quit3:
    return;
}

// Dummy function to be implemented
void detect_batch_img() {
    return;
}

// Dummy function to be implemented
void detech_ocr4() {
    return;
}

void mainProcess(
    hailo_configured_network_group &plate_finder_network_group,\
    hailo_configured_network_group &ocr4_network_group,\
    uint32_t max_num_pics, uint32_t plate_finder_image_size, uint32_t ocr4_image_size, std::string pic_dir,\
    std::vector<std::unique_ptr<uint8_t>> &imageQueue, std::vector<std::unique_ptr<uint8_t>> &outQueue, std::vector<std::unique_ptr<uint8_t>> &ocr4_inQueue, std::vector<std::unique_ptr<uint8_t>> &ocr4_outQueue, \
    SyncObject &syncObj, \
    uint8_t twoDeviceFlag, uint32_t num_cameras
    ) {
        hailo_status status = HAILO_UNINITIALIZED;

        hailo_activated_network_group activated_plate_finder_network_group = NULL;
        hailo_activated_network_group activated_ocr4_network_group = NULL;

        /* Implement synchronization scheme in https://www.modernescpp.com/index.php/the-atomic-boolean
        to block this thread until the shared variable isRunningFlag is true */
        
        // Wait until main() sets isRunningFlag to true
        while ( !syncObj.isRunningFlag.load() ) {
            std::this_thread::yield();
        }
        
        syncObj.startEvent_plate_finder= true;

        // Plate Finder Inference and Onnx Post Processing
        // If we have 1 device, need to activate the network at every frame as each network goes to a separate device
        if (twoDeviceFlag== 0) {
            status = hailo_activate_network_group(plate_finder_network_group, NULL, &activated_plate_finder_network_group);
            REQUIRE_SUCCESS(status, l_quit_mainProcess, "Failed activate plate finder network group");
        }

        detect_batch_img(); // Replace this dummy function with the real one

        if (twoDeviceFlag== 0) {
            (void)hailo_deactivate_network_group(activated_plate_finder_network_group);
        }   
        
        syncObj.startEvent_ocr4= true;

        // OCR4 processing
        // If we have 1 device, need to activate the network at every frame as each network goes to a separate device
        if (twoDeviceFlag== 0) {
            status = hailo_activate_network_group(ocr4_network_group, NULL, &activated_ocr4_network_group);
            REQUIRE_SUCCESS(status, l_quit_mainProcess, "Failed activate ocr4 network group");
        }

        detech_ocr4(); // Replace this dummy function with the real one

        if (twoDeviceFlag== 0) {
            (void)hailo_deactivate_network_group(activated_ocr4_network_group);
        }  

        std::cout << "In mainProcess()" << std::endl;

l_quit_mainProcess:
        syncObj.isRunningFlag= false;
        return;
}

int main(void) {

    hailo_status status = HAILO_UNINITIALIZED;
    hailo_device_id_t device_ids[MAX_DEVICES];
    hailo_device devices[2]={NULL,NULL};
    hailo_hef hef[2]={NULL,NULL};
    size_t actual_count = MAX_DEVICES;

    SyncObject syncObj;

    std::chrono::duration<double> total_time;
    std::chrono::time_point<std::chrono::system_clock> t_end;
    std::chrono::time_point<std::chrono::system_clock> t_start = std::chrono::high_resolution_clock::now();

    if (std::filesystem::is_directory(HEF_OUTPUT_DIR)==false) {
        mkdir(HEF_OUTPUT_DIR, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    }

    // Initializing Queues and Onnx runtime
    //post_process_onnx = onnxruntime.InferenceSession(post_process_onnx_path)
    std::vector<std::unique_ptr<uint8_t>> imageQueue;
    std::vector<std::unique_ptr<uint8_t>> hefOutQueue;
    std::vector<std::unique_ptr<uint8_t>> outQueue;
    std::vector<std::unique_ptr<uint8_t>> ocr4_inQueue;
    std::vector<std::unique_ptr<uint8_t>> ocr4_infoQueue;
    std::vector<std::unique_ptr<uint8_t>> ocr4_outQueue;

    std::unique_ptr<std::thread> main_process;
    std::unique_ptr<std::thread> plate_finder_send_process;
    std::unique_ptr<std::thread> plate_finder_recv_process;
    std::unique_ptr<std::thread> plate_finder_post_process;
    std::unique_ptr<std::thread> ocr4_infer_send_process;
    std::unique_ptr<std::thread> ocr4_infer_recv_process;

    hailo_configured_network_group ocr4_network_group = NULL;
    hailo_activated_network_group activated_ocr4_network_group = NULL;
    size_t ocr4_network_group_size= 1;
    hailo_configure_params_t ocr4_configure_params;

    hailo_configured_network_group plate_finder_network_group = NULL;
    hailo_activated_network_group activated_plate_finder_network_group = NULL;
    size_t plate_finder_network_group_size= 1;
    hailo_configure_params_t plate_finder_configure_params;

    uint8_t twoDeviceFlag = 0;

    status = hailo_scan_devices(NULL, device_ids, &actual_count);
    REQUIRE_SUCCESS(status, l_exit, "Failed to scan devices");

    if (actual_count > 1)
        twoDeviceFlag = 1;
    else
        device_ids[1]= device_ids[0];

    // Initializing OCR4 Model
    status = hailo_create_device_by_id(&device_ids[0], &devices[0]);
    REQUIRE_SUCCESS(status, l_exit, "Failed to create device 0"); 

    status = hailo_create_hef_file(&hef[OCR4_HEF_IDX], OCR4_HEF_PATH);
    REQUIRE_SUCCESS(status, l_release_device, "Failed creating OCR4 hef file %s", OCR4_HEF_PATH);

    status = hailo_init_configure_params(hef[OCR4_HEF_IDX], HAILO_STREAM_INTERFACE_PCIE, &ocr4_configure_params);
    REQUIRE_SUCCESS(status, l_release_hef, "Failed init configure OCR4 params");

    status = hailo_configure_device(devices[0], hef[OCR4_HEF_IDX], &ocr4_configure_params, &ocr4_network_group, &ocr4_network_group_size);
    REQUIRE_SUCCESS(status, l_release_hef, "Failed configuring device for OCR4");
    REQUIRE_ACTION(ocr4_network_group_size == 1, status = HAILO_INVALID_ARGUMENT, l_release_hef, 
        "Unexpected network group size for OCR4");

    /* Create an additional device if we have two Hailo-8 in the system */
    if (twoDeviceFlag) {
        status = hailo_create_device_by_id(&device_ids[1], &devices[1]);
        REQUIRE_SUCCESS(status, l_exit, "Failed to create device 1"); 
    }
    else {
        devices[1]= devices[0];
    }

    // Initializing Plate_finder Model
    status = hailo_create_hef_file(&hef[PLATE_FINDER_HEF_IDX], PLATE_FINDER_HEF_PATH);
    REQUIRE_SUCCESS(status, l_release_device, "Failed creating Plate finder hef file %s", PLATE_FINDER_HEF_PATH);

    status = hailo_init_configure_params(hef[PLATE_FINDER_HEF_IDX], HAILO_STREAM_INTERFACE_PCIE, &plate_finder_configure_params);
    REQUIRE_SUCCESS(status, l_release_hef, "Failed init configure plate finder params");

    status = hailo_configure_device(devices[1], hef[PLATE_FINDER_HEF_IDX], &plate_finder_configure_params, &plate_finder_network_group, &plate_finder_network_group_size);
    REQUIRE_SUCCESS(status, l_release_hef, "Failed configuring device for plate finder");
    REQUIRE_ACTION(plate_finder_network_group_size == 1, status = HAILO_INVALID_ARGUMENT, l_release_hef, 
        "Unexpected network group size for plate finder");

    // Main Process
    main_process= std::make_unique<std::thread>(mainProcess, \
        std::ref(plate_finder_network_group), \
        std::ref(ocr4_network_group), \
        MAX_NUM_PICS, PLATE_FINDER_IMAGE_SIZE, OCR4_IMAGE_SIZE, PIC_DIR, /*post_process_onnx, */\
        std::ref(imageQueue), std::ref(outQueue), std::ref(ocr4_inQueue), std::ref(ocr4_outQueue),\
        std::ref(syncObj),\
        twoDeviceFlag, NUM_CAMERAS);

    // Plate Finder Inference and Post Process Threads
    plate_finder_send_process = std::make_unique<std::thread>(plate_finder_send, \
        std::ref(plate_finder_network_group), \
        std::ref(hef[PLATE_FINDER_HEF_IDX]), /*post_process_onnx, */
        std::ref(imageQueue), std::ref(hefOutQueue),\
        std::ref(syncObj),\
        NUM_CAMERAS);

    plate_finder_recv_process = std::make_unique<std::thread>(plate_finder_recv, \
        std::ref(plate_finder_network_group),\
        std::ref(hef[PLATE_FINDER_HEF_IDX]), /*post_process_onnx, */
        std::ref(imageQueue), std::ref(hefOutQueue),\
        std::ref(syncObj),\
        NUM_CAMERAS);
    
    plate_finder_post_process = std::make_unique<std::thread>(plate_finder_post, \
        std::ref(hefOutQueue),\
        std::ref(outQueue),\
        /*post_process_onnx, */
        std::ref(syncObj));

    // OCR4 Inference Thread
    ocr4_infer_send_process = std::make_unique<std::thread>(ocr4_infer_model_send, \
        std::ref(ocr4_network_group),\
        std::ref(hef[OCR4_HEF_IDX]),
        std::ref(ocr4_inQueue), std::ref(ocr4_infoQueue),\
        std::ref(syncObj),\
        NUM_CAMERAS);

    ocr4_infer_recv_process = std::make_unique<std::thread>(ocr4_infer_model_recv, \
        std::ref(ocr4_network_group),\
        std::ref(hef[OCR4_HEF_IDX]),
        std::ref(ocr4_infoQueue), std::ref(ocr4_outQueue),\
        std::ref(syncObj),\
        NUM_CAMERAS);
    
    if (twoDeviceFlag== 1) {
        status = hailo_activate_network_group(ocr4_network_group, NULL, &activated_ocr4_network_group);
        REQUIRE_SUCCESS(status, l_release_hef, "Failed activate ocr4 network group");
        status = hailo_activate_network_group(plate_finder_network_group, NULL, &activated_plate_finder_network_group);
        REQUIRE_SUCCESS(status, l_release_hef, "Failed activate plate finder network group");
    }

    /* Setting isRunningFlag to true will start the mainProcess thread, which in turn starts the other threads */
    syncObj.isRunningFlag= true;

    main_process->join();
    plate_finder_send_process->join();
    plate_finder_recv_process->join();
    plate_finder_post_process->join();
    ocr4_infer_send_process->join();
    ocr4_infer_recv_process->join();

    if (twoDeviceFlag== 1) {
        (void)hailo_deactivate_network_group(activated_ocr4_network_group);
        (void)hailo_deactivate_network_group(activated_plate_finder_network_group);
    }

    t_end = std::chrono::high_resolution_clock::now();
    total_time = t_end - t_start;

    std::cout << BOLDBLUE << "\n-I- Inference finished successfully" << RESET << std::endl;
    std::cout << BOLDBLUE << "-I- Total inference run time: " << (double)total_time.count() << " sec" << RESET << std::endl;

l_release_hef:
    (void) hailo_release_hef(hef[OCR4_HEF_IDX]);
    (void) hailo_release_hef(hef[PLATE_FINDER_HEF_IDX]);

l_release_device:
    (void) hailo_release_device (devices[0]);

    if (twoDeviceFlag==1)
        (void) hailo_release_device (devices[1]);

l_exit:
    return status;
}
