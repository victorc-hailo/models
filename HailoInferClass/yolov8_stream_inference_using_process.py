#!/usr/bin/env python3

import cv2
import hailo_infer_class
import os, random, time
import numpy as np

import yolox_stream_report_detections as report

# Loading compiled HEFs to device:
model_name = 'yolov8s'
hef_path = 'resources/hefs/{}.hef'.format(model_name)
video_dir = './resources/video/'

mp4_files = [f for f in os.listdir(video_dir) if os.path.isfile(os.path.join(video_dir, f)) and f.endswith('.mp4')]

# Instantiate the HailoInferClass
hailoInfer = hailo_infer_class.HailoInferClass()
# Initialize the Hailo Infer class with a Yolo model and return the input resolution
width, height= hailoInfer.yolo_init(hef_path)

source = 'camera'
cap = cv2.VideoCapture(0)

# check if the camera was opened successfully
if not cap.isOpened():
    print("Could not open camera")
    exit()

#Start the yolo inference process
hailoInfer.yolo_start()

while True:
    # read a frame from the video source
    ret, frame = cap.read()

    # Get height and width from capture
    orig_w = cap.get(cv2.CAP_PROP_FRAME_WIDTH)  
    orig_h = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)        

    # check if the frame was successfully read
    if not ret:
        print("Could not read frame")
        break

    # loop if video source
    if source == 'video' and not cap.get(cv2.CAP_PROP_POS_FRAMES) % cap.get(cv2.CAP_PROP_FRAME_COUNT):
        cap.set(cv2.CAP_PROP_POS_FRAMES, 0)

    # resize image for yolox_s_leaky input resolution and infer it
    resized_img = cv2.resize(frame, (height, width), interpolation = cv2.INTER_AREA)

    # Perform the Yolo inference
    detections= hailoInfer.yolo_infer(resized_img)

    num_detections = int(np.array(detections['num_detections'])[0])
    scores = np.array(detections['detection_scores'])[0]
    classes = np.array(detections['detection_classes'])[0].astype(int)
    boxes = np.array(detections['detection_boxes'])[0]
    # if scores is an empty array, set num_detections to 0
    if len(scores) == 0:
        num_detections = 0
    preds_dict = {'scores': scores, 'classes': classes, 'boxes': boxes, 'num_detections': num_detections}
    frame = report.report_detections(preds_dict, frame, scale_factor_x = orig_w, scale_factor_y = orig_h, nms=True)
    cv2.imshow('frame', frame)
    
    # wait for a key event
    key = cv2.waitKey(1)

    # switch between camera and video source
    if key == ord('c'):
        source = 'camera'
        cap.release()
        cap = cv2.VideoCapture(0)
    elif key == ord('v'):
        source = 'video'
        cap.release()
        random_mp4 = random.choice(mp4_files)
        cap = cv2.VideoCapture(video_dir+random_mp4)
    elif key == ord('q'):
        print('Exit key pressed, exiting ...')
        break

# Stop the yolo inference process
hailoInfer.yolo_stop()

# release the video source and destroy all windows
cap.release()
cv2.destroyAllWindows()