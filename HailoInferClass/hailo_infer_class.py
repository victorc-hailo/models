import os
from multiprocessing import Process, Queue, Event, Lock, Value
from queue import Empty
import numpy as np

from hailo_platform import (HEF, VDevice, HailoStreamInterface, ConfigureParams, InferVStreams, InputVStreamParams,
                            OutputVStreamParams, HailoSchedulingAlgorithm, FormatType)

import psutil

class HailoInferClass:

    def __check_if_service_enabled(self, process_name):
        '''
        Check if there is any running process that contains the given name processName.
        '''
        #Iterate over the all the running process
        for proc in psutil.process_iter():
            try:
                if process_name.lower() in proc.name().lower():
                    #print('HailoRT Scheduler service is enabled!')
                    return
            except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
                pass
        
        print('HailoRT Scheduler service is disabled. Enabling service...')
        os.system('sudo systemctl disable hailort.service --now  && sudo systemctl daemon-reload && sudo systemctl enable hailort.service --now')
        
    def __create_vdevice_params(self):
        params = VDevice.create_params()
        params.scheduling_algorithm = HailoSchedulingAlgorithm.ROUND_ROBIN
        params.group_id = "SHARED"
        return params

    def __init__(self):
        # Set the timeout for the queue.get() function and Hailo VStream send() and recv() functions
        # If there is a gap between frames that exceeds the timeout, then queue.get() will throw an exception or Hailo device will throw HAILO_TIMEOUT error 
        self.QUEUE_TIMEOUT=1 # Second        
        self.__check_if_service_enabled('hailort_service')
        params = self.__create_vdevice_params()
        self.target= VDevice(params)
        self.yoloState= "Uninitialized"

    def __extract_detections(self, input, boxes, scores, classes, num_detections, threshold=0.5):   
        for i, detection in enumerate(input):
            if len(detection) == 0:
                continue
            for j in range(len(detection)):
                bbox = np.array(detection)[j][:4]
                score = np.array(detection)[j][4]
                if score < threshold:
                    continue
                else:
                    boxes.append(bbox)
                    scores.append(score)
                    classes.append(i)
                    num_detections = num_detections + 1
        return {'detection_boxes': [boxes], 
                'detection_classes': [classes], 
                'detection_scores': [scores],
                'num_detections': [num_detections]}

    def __post_nms_infer(self, raw_detections, input_name):
        boxes = []
        scores = []
        classes = []
        num_detections = 0
        
        detections = self.__extract_detections(raw_detections[input_name][0], boxes, scores, classes, num_detections)
        
        return detections

    def __inferYolo(self):

        input_vstreams_params = InputVStreamParams.make(self.yolo_network_group, quantized=False, format_type=FormatType.FLOAT32)
        output_vstreams_params = OutputVStreamParams.make(self.yolo_network_group, quantized=False, format_type=FormatType.FLOAT32)
        
        with InferVStreams(self.yolo_network_group, input_vstreams_params, output_vstreams_params) as infer_pipeline:
            while (True):
                try:
                    image= self.yoloInputQueue.get(timeout= self.QUEUE_TIMEOUT)
                except Empty:
                    print ("Error __inferYolo() timed-out, exiting")
                    return

                #if image is empty, it means that the main process has finished and we should stop the send process by returning
                if image.size==0:
                    #print("inferYolo() exiting")
                    return
                
                infer_results = infer_pipeline.infer(np.expand_dims(image, axis=0).astype(np.float32))
                detections = self.__post_nms_infer(infer_results, self.yolo_outputs[0].name)
                self.yoloOutputQueue.put(detections)

    def yolo_init(self, hef_path):
        hef = HEF(hef_path)
        self.yolo_outputs = hef.get_output_vstream_infos()
        configure_params = ConfigureParams.create_from_hef(hef, interface=HailoStreamInterface.PCIe)  
        self.yolo_network_group = self.target.configure(hef, configure_params)[0]
        height, width, channels = hef.get_input_vstream_infos()[0].shape

        # yoloInputQueue is used to pass the images to the Yolo process
        self.yoloInputQueue = Queue()
        # yoloOutputQueue is used to get the detection output from the Yolo process
        self.yoloOutputQueue = Queue()

        self.yoloState= "Initialized"
        return width, height

    def yolo_start(self):
        if (self.yoloState!= "Initialized"):
            print("Yolo is not initialized")
            return
        self.yolo_process= Process(target=self.__inferYolo)
        self.yolo_process.start()
        self.yoloState= "Running"

    def yolo_infer(self, image):
        if (self.yoloState!= "Running"):
            print("Yolo is not running")
            return
        self.yoloInputQueue.put(image)
        try:
            detections= self.yoloOutputQueue.get(timeout= self.QUEUE_TIMEOUT)
        except Empty:
            print ("Error yolo_infer() receiving detection timed-out")
            detections=[]
        
        return detections
    
    def yolo_stop(self):
        if (self.yoloState!= "Running"):
            print("Yolo is not running")
            return
        # Send empty image to signal the end of the inference
        self.yoloInputQueue.put(np.array([]))
        self.yolo_process.join()
        self.yoloState= "Initialized"

    # Deleting (Calling destructor)
    def __del__(self):
 
        if self.yoloState== "Running":
            self.yolo_process.terminate()
            self.yolo_process.join()
            self.yoloState= "Initialized"
        if self.yoloState== "Initialized":
            self.yoloInputQueue.close()
            self.yoloOutputQueue.close()
            self.yoloState= "Uninitialized"

        self.target.release()
