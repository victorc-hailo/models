# This python test code can be used to visually validate Yolov5 class model
# The default input is set to camera with the variable CAMERA_INPUT set to True
# Set it to False to enable file video input and fill the OUT_VIDEO_PATH accordingly
# The HEF file is specified by the variable HEF_MODEL_NAME
# Set the variable NUMBER_OF_CLASSES to the number of classes the model was trained to detect.
# Anchor definitions are taken from file base_yolo.yaml which is a copy of hailo_sw_suite/sources/model_zoo/hailo_model_zoo/cfg/base/yolo.yaml
# This file defines anchor for the p5-640 configuration as specified here: https://github.com/ultralytics/yolov5/blob/master/models/hub/anchors.yaml
# Please modify the file if you use different anchors.

# Note that this python script is not optimized for best fps. It should just be used for visual validation of the output.
# The following aspects are not optimized for performance:
# - Use of API infer_pipeline.infer is not the optimal way of infering on Hailo-8 due to the blocking nature of the APUI.
#   Preferred way is to use multi-process approach for send and recv. Refer to inference tutorial.
# - Post-processing is done by Python Hailo model zoo's function YoloPostProc. This is unoptimized Python code.
# - No multi-process, everything runs sequentially to each other. But code is compact and easier to understand.

import numpy as np
import cv2
from hailo_platform import (HEF, PcieDevice, HailoStreamInterface, InferVStreams, ConfigureParams,
                            InputVStreamParams, OutputVStreamParams, InputVStreams, OutputVStreams, FormatType)
from detection_tools.utils.visualization_utils import visualize_boxes_and_labels_on_image_array
from hailo_model_zoo.core.postprocessing.detection.yolo import YoloPostProc
from hailo_model_zoo.core.preprocessing.detection_preprocessing import yolo_v5, letterbox
import os
import yaml
import time
#import torch


CAMERA_INPUT= True
VIDEO_NAME = 'test.mp4'
VIDEO_PATH = os.path.join('.', VIDEO_NAME)
YOLO_BASE_CONFIG_PATH = os.path.join('.', 'base_yolo.yaml')

HEF_MODEL_NAME = 'yolov5s'
HEF_MODEL_PATH = os.path.join('.',f'{HEF_MODEL_NAME}.hef')
NUMBER_OF_CLASSES = 80
OUT_VIDEO_PATH = os.path.join('videos', 'outputs', 'output.mp4')

LABELS_PATH = os.path.join('.', 'yolov5m_labels.txt')


def preprocess(frm, img_sz=(640, 640)):

    meta = {'original_shape': frm.shape,
            'resized_shape': img_sz}
    new_image, new_image_info = yolo_v5(frm, height=640, width=640, centered=True,  image_info={})
    just_resized, new_width, new_height = letterbox(frm, height=640, width=640, centered=True)
    batched_new_image = new_image.numpy()[np.newaxis]
    output = {'image': batched_new_image}
    output["just_resized"] = just_resized[:, :, ::-1]

    return output, meta

def clip_coords(boxes, shape):
    # Clip bounding xyxy bounding boxes to image shape (height, width)
    #if isinstance(boxes, torch.Tensor):  # faster individually
    #    boxes[:, 0].clamp_(0, shape[1])  # x1
    #    boxes[:, 1].clamp_(0, shape[0])  # y1
    #    boxes[:, 2].clamp_(0, shape[1])  # x2
    #    boxes[:, 3].clamp_(0, shape[0])  # y2
    #else:  # np.array (faster grouped)
    boxes[:, [0, 2]] = boxes[:, [0, 2]].clip(0, shape[0])  # x1, x2
    boxes[:, [1, 3]] = boxes[:, [1, 3]].clip(0, shape[1])  # y1, y2
    return boxes

def scale_coords(normalized_coordinates, original_image_shape):
    ''' Converts normalized coordinates to image pixel indices'''
    scaled_coordinates = np.zeros(normalized_coordinates.shape)
    scaled_coordinates[:, [0, 2]] = normalized_coordinates[:, [0, 2]] * original_image_shape[0]
    scaled_coordinates[:, [1, 3]] = normalized_coordinates[:, [1, 3]] * original_image_shape[1]
    scaled_coordinates = clip_coords(scaled_coordinates, original_image_shape)
    return scaled_coordinates.astype(int)

def restructure_nms_output(nms_output, score_threshold=0):
    ''' nms_output members are converted to numpy. 
    It also filters out boxes with score lesser than score_threshold'''
    restructured_nms_output = {
        'detection_boxes': None,
        'detection_scores': None,
        'detection_classes': None,
        'num_detections': None}
    good_boxes = nms_output['detection_scores'].numpy()[0, :] > score_threshold
    restructured_nms_output['detection_scores'] = nms_output['detection_scores'].numpy()[0, good_boxes]
    restructured_nms_output['detection_boxes'] = nms_output['detection_boxes'].numpy()[0, good_boxes, :]
    restructured_nms_output['detection_classes'] = nms_output['detection_classes'].numpy()[0, good_boxes]
    restructured_nms_output['num_detections'] = nms_output['num_detections'].numpy()[0]

    return restructured_nms_output

def get_labels():
    with open(LABELS_PATH, 'r') as f:
        labels = eval(f.read()) 
        return labels

class VideoManager():
    def __init__(self):
        self.fps = None
        self.cols = None
        self.rows = None
        self.frame_count = None
        self.start_frame = None
        self.end_frame = None
        self.video_writer = None
        self.video_obj = None

    def load_video(self, video_path):
        
        if CAMERA_INPUT== False:
            self.video_obj = cv2.VideoCapture(video_path)
        else:
            self.video_obj = cv2.VideoCapture(0, cv2.CAP_ANY)

        if not self.video_obj.isOpened():
            msg = 'Failed at loading video.'
            # logging.error(msg)
            raise IOError(msg)
        self.fps = self.video_obj.get(cv2.CAP_PROP_FPS)
        self.frame_count = int(self.video_obj.get(cv2.CAP_PROP_FRAME_COUNT))
        self.cols = int(self.video_obj.get(cv2.CAP_PROP_FRAME_WIDTH))
        self.rows = int(self.video_obj.get(cv2.CAP_PROP_FRAME_HEIGHT))
        self.start_frame, self.end_frame = 0, self.frame_count
    
    def set_video_writer(self, video_out_path, output_shape, fps):
        self.video_writer = cv2.VideoWriter(
            video_out_path,
            cv2.VideoWriter_fourcc(*'mp4v'),
            fps,
            output_shape
        )

    def add_boxes_to_frame(self, frame, boxes, labels, label_names):
        colors = [
            (0, 255, 255), (0, 255, 0), (0, 0, 255), (255, 0, 0)]
        # font
        font = cv2.FONT_HERSHEY_SIMPLEX
        # fontScale
        fontScale = 0.5
        
        line_thickness = 1
        image = np.ascontiguousarray(frame, dtype=np.uint8)
        if len(boxes) > 0:
            for idx, box in enumerate(boxes):
                
                if labels[idx] <= NUMBER_OF_CLASSES:
                    label_name= label_names[labels[idx]]
                else:
                    label_name= "N/A"

                color_idx= labels[idx] % len(colors)

                new_frame = cv2.rectangle(
                    image, 
                    (box[1], box[0]), (box[3], box[2]), 
                    color=colors[color_idx],
                    thickness=line_thickness
                )
                
                new_frame= cv2.putText(new_frame, label_name, (box[1], box[0] - 4), font, fontScale, colors[color_idx], line_thickness, cv2.LINE_AA)

        else:
            new_frame = frame.copy()
        return new_frame

    def write_frame(self, frame):
        self.video_writer.write(frame)
    
    def close_writer(self):
        self.video_writer.release()

def get_anchors_dict(yolo_base_config_path):
    with open(yolo_base_config_path) as file:
        config = yaml.load(file, Loader=yaml.FullLoader)
        anchors = config["postprocessing"]["anchors"]
        return anchors

def main():
    # The target can be used as a context manager ("with" statement) to ensure it's released on time.
    # Here it's avoided for the sake of simplicity
    target = PcieDevice()
    hef = HEF(HEF_MODEL_PATH)
    # Get the "network groups" (connectivity groups, aka. "different networks") information from the .hef
    configure_params = ConfigureParams.create_from_hef(hef=hef, interface=HailoStreamInterface.PCIe)
    network_groups = target.configure(hef, configure_params)
    network_group = network_groups[0]
    network_group_params = network_group.create_params()

    # Create input and output virtual streams params
    # Quantized argument signifies whether or not the incoming data is already quantized.
    # Data is quantized by HailoRT if and only if quantized == False .
    input_vstreams_params = InputVStreamParams.make(network_group, quantized=False,
                                                                    format_type=FormatType.FLOAT32)
    output_vstreams_params = OutputVStreamParams.make(network_group, quantized=False,
                                                                        format_type=FormatType.FLOAT32)
    # Define dataset params
    input_vstream_info = hef.get_input_vstream_infos()[0]
    output_vstream_info = hef.get_output_vstream_infos()[0]
    endondes_info = hef.get_output_vstream_infos()
    image_height, image_width, channels = input_vstream_info.shape
    video_manager = VideoManager()
    video_manager.load_video(VIDEO_PATH)
    video_manager.set_video_writer(OUT_VIDEO_PATH, (image_height, image_width), video_manager.fps)
    
    anchors = get_anchors_dict(YOLO_BASE_CONFIG_PATH)
    labels= get_labels()

    device_pre_post_layer = {"softmax": False, "argmax": False, "bilinear": False, "nms": False, "max_finder": False}
    yolo_post_proc = YoloPostProc(
        img_dims=(image_height, image_width),
        anchors=anchors,
        nms_iou_thresh=0.45,
        score_threshold=0.1,
        labels_offset=1,
        meta_arch="yolo_v5",
        device_pre_post_layers=device_pre_post_layer,
        classes=NUMBER_OF_CLASSES)

    with InferVStreams(network_group, input_vstreams_params, output_vstreams_params) as infer_pipeline:
        with network_group.activate(network_group_params):
            while 1:
                ret, frameBGR = video_manager.video_obj.read()
                frame= cv2.cvtColor(frameBGR, cv2.COLOR_BGR2RGB); 
                frame_dict, meta = preprocess(frame)
                frame, new_image_info = yolo_v5(frame, height=640, width=640, centered=True,  image_info={})
                # Wrapping frame into a dict, as expected by hailo
                batched_new_image = frame.numpy()[np.newaxis]
                input_data = {input_vstream_info.name: batched_new_image}
                infer_results = infer_pipeline.infer(input_data)
                # Pulling all 3 output endnodes from dictionary into list
                endnodes = [infer_results[endnode_info.name] for endnode_info in endondes_info]
                nms_output = yolo_post_proc.yolo_postprocessing(endnodes)
                nms_output = restructure_nms_output(nms_output)
                nms_output['detection_boxes'] = scale_coords(nms_output['detection_boxes'], meta['resized_shape'])
                annotated_frame = video_manager.add_boxes_to_frame(frame_dict["just_resized"], nms_output['detection_boxes'], nms_output['detection_classes'], labels)
                cv2.imshow('frm', annotated_frame)
                cv2.waitKey(60)
                video_manager.write_frame(annotated_frame)
    video_manager.close_writer()
    video_manager.video_obj.release()

if __name__ == "__main__":
    main()