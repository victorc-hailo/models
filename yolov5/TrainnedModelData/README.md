
This example uses Yolov5m V2.0 pretrainned model to convert to ONNX before parsing, quantization, and compile


1. Download YOLOv5 v2.0 pretrained model

    ```shell
    curl -LO https://github.com/ultralytics/yolov5/releases/download/v2.0/yolov5m.pt
    ```

2. Download yolov5 v2.0
    ```shell
    git clone --single-branch --branch v2.0 https://github.com/ultralytics/yolov5
    ```

3. Convert yolov5m.pt to yolov5m.onnx

    ```shell
    cd ./yolov5
    virtualenv env
    source env/bin/activate
    pip install -r requirements.txt
    pip install onnx
    sed -i 's/opset_version=12/opset_version=11/g' models/export.py      #Hailo Dataflow compiler support opset 11 only
    export PYTHONPATH="$PWD" && python ./models/export.py --weights ../yolov5m.pt --img 640 --batch 1
    cd ../
    ls yolov5m.onnx # check file, convert successfully
    ```

4. Run notebook to start conversion

    ```shell
    cd ./Notebook/
    jupyter-notebook
    ```

