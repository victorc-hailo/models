** Last HailoRT version checked - 4.8.1 **
This is a hailort C++ API multi-hef scheduler example for running inference on multiple HEF files using the HailoRT scheduler with vstreams.
The example does the following:

1. creates a device (pcie)
2. reads the network configuration from a HEF files
3. prepares the application for inference
4. runs inference on all the given networks using the HailoRT scheduler
5. prints statistics

NOTE: Currently support only devices connected on a PCIe link.

Prequisites:
0. For compiling for ARM: 
   sudo apt install -y gcc-aarch64-linux-gnu


To Compile all targets to all architectures (x86, armv7l, aarch64):
Please updadte the HAILORT-VERSION variable in `build.sh` (for example: HAILORT_VER=4.9.0), and then execute the script

To run the compiled example:

./build/x86_64/switch_network_groups_example.<HAILORT-VERSION> -hefs=HEF1.hef,HEF2.hef,... -num=NUM_OF_FRAMES_FOR_HEF1,NUM_OF_FRAMES_FOR_HEF2,...

NOTE: The number of HEF files given should match the number of frames counts given.
NOTE: When giving the input HEFs\frames count, there should be NO spaces between the arguments.
