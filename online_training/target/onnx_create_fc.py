import numpy as np
import onnx

# This function creates an initializer tensor (TensorProto) from a numpy array.
def create_initializer_tensor(
        name: str,
        tensor_array: np.ndarray,
        data_type: onnx.TensorProto = onnx.TensorProto.FLOAT
) -> onnx.TensorProto:

    # (TensorProto)
    initializer_tensor = onnx.helper.make_tensor(
        name=name,
        data_type=data_type,
        dims=tensor_array.shape,
        vals=tensor_array.flatten().tolist())

    return initializer_tensor

def create_fc_model(weights, bias, model_name) -> onnx.ModelProto:

    # Create a neural network with a fully connected layer.

    # IO tensors (ValueInfoProto).
    model_input_name = "input"
    model_input_channels = weights.shape[1]
    X = onnx.helper.make_tensor_value_info(model_input_name,
                                           onnx.TensorProto.FLOAT,
                                           [1, model_input_channels])
    model_output_name = "output"
    model_output_channels = weights.shape[0]
    Y = onnx.helper.make_tensor_value_info(model_output_name,
                                           onnx.TensorProto.FLOAT,
                                           [1, model_output_channels])

    # Create a Conv node (NodeProto).
    # https://github.com/onnx/onnx/blob/rel-1.9.0/docs/Operators.md#conv
    # Dummy weights for conv.
    fc1_in_channels = model_input_channels
    fc1_out_channels = model_output_channels

    fc1_W = weights # np.ones(shape=(fc1_out_channels, fc1_in_channels)).astype(np.float32)
    fc1_B = bias # np.ones(shape=(fc1_out_channels)).astype(np.float32)
    # Create the initializer tensor for the weights.
    fc1_W_initializer_tensor_name = "fc.weight"
    fc1_W_initializer_tensor = create_initializer_tensor(
        name=fc1_W_initializer_tensor_name,
        tensor_array=fc1_W,
        data_type=onnx.TensorProto.FLOAT)
    fc1_B_initializer_tensor_name = "fc.bias"
    fc1_B_initializer_tensor = create_initializer_tensor(
        name=fc1_B_initializer_tensor_name,
        tensor_array=fc1_B,
        data_type=onnx.TensorProto.FLOAT)

    fc1_node = onnx.helper.make_node(
        name="fc",  # Name is optional.
        op_type="Gemm",
        # Must follow the order of input and output definitions.
        # https://github.com/onnx/onnx/blob/rel-1.9.0/docs/Operators.md#inputs-2---3
        inputs=[
            model_input_name, fc1_W_initializer_tensor_name,
            fc1_B_initializer_tensor_name
        ],
        outputs=[model_output_name],
        transB= 1
    )

    # Create the graph (GraphProto)
    graph_def = onnx.helper.make_graph(
        nodes=[fc1_node],
        name= model_name,
        inputs=[X],  # Graph input
        outputs=[Y],  # Graph output
        initializer=[
            fc1_W_initializer_tensor, fc1_B_initializer_tensor
        ],
    )

    # Create the model (ModelProto)
    model_def = onnx.helper.make_model(graph_def, producer_name="Hailo")
    model_def.opset_import[0].version = 13

    model_def = onnx.shape_inference.infer_shapes(model_def)

    onnx.checker.check_model(model_def)

    #onnx.save(model_def, "../onnx/fcnet.onnx")

    return model_def