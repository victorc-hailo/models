#!/usr/bin/env python3

# Description: This script retrains the fully connected layer of a model using the HEF file and new samples
# Algorithm used in weight imprinting based on https://github.com/YU1ut/imprinted-weights/blob/master/imprint.py
# Example of usage: python3 ./imprint_weights.py --hef ../hef/resnet_v1_18_featext.hef --onnx_input ../onnx/resnet_v1_18_fc.onnx --onnx_output ../onnx/retrained_resnet_v1_18_fc.onnx --input-images ../training_data/ > results.txt

import numpy as np
from feat_extractor_class import FeatureExtractor
from hailo_platform import __version__
import onnx
from onnx import numpy_helper
import onnxruntime
from multiprocessing import Process, Queue, Event, Lock, Value
from queue import Empty
from hailo_platform import (HEF, PcieDevice, HailoStreamInterface, ConfigureParams, InputVStreamParams, OutputVStreamParams, InputVStreams, OutputVStreams, FormatType)
from zenlog import log
import time
from PIL import Image
import os
import argparse
import cv2
import sys
from onnx_create_fc import create_fc_model

parser = argparse.ArgumentParser(description='Retrain the fully connected layer of a model using the HEF file and new samples')
parser.add_argument('--hef', help="HEF file path")
parser.add_argument('--onnx_input', help="Path to the ONNX file implementing the post-processing (ususally fully connected layer), that takes the output of the HEF as input")
parser.add_argument('--onnx_output', help="Path to the output retrained ONNX file implementing the post-processing (ususally fully connected layer), that takes the output of the HEF as input")
parser.add_argument('--input-images', help="Path to directory structure of samples of new classes to train the model on. The directory structure should be as follows: <input-images>/<#NUM#.class-name>/<image-name>.jpg where #NUM# is an integer (0, 1, ...) enumerating the class")  
parser.add_argument('--hw', help="Hardware hailo8 or hailo15", default="hailo8")
args = parser.parse_args()

# ----------------Pre-processing functions ------------------- #

def prepare_image(image_path, height, width):
    raw_image = cv2.imread(image_path)
    image_RGB = cv2.cvtColor(raw_image, cv2.COLOR_BGR2RGB)
    cv2.normalize(image_RGB, image_RGB, 0, 255, norm_type=cv2.NORM_MINMAX)
    image_RGB= cv2.resize(image_RGB, (height, width), interpolation = cv2.INTER_AREA)
    image = np.array(image_RGB, np.uint8)
    return image

def h15_call_cpp_imprint_weights(image_path):
    # Call external executable that performs inference on the image usin OS library
    os.system('./classifier_cpp/build/x86_64/classifier -hef={} -path={} -embed={} -verbose=off'.format(args.hef, image_path, "./embeddings.txt"))
    # Read the embeddings from the file
    embeddings = np.loadtxt("./embeddings.txt", dtype=np.float32)
    embeddings = np.expand_dims(embeddings, axis=0)
    # Perform post-processing using the ONNX model
    inference_dict = {"embeddings" : embeddings}

    # Increment the number of processed frames
    with numProcessedFrames.get_lock():
        numProcessedFrames.value += 1

    return inference_dict


# ---------------- Inferences threads functions -------------- #

def mainProcess(images_path, height, width, imageQueue, hw):
    
    if (images_path.endswith('.jpg') or images_path.endswith('.png')):
        if hw=="hailo8":
            imageQueue.put(prepare_image(images_path, height, width))
        else:
            imageQueue.put(h15_call_cpp_imprint_weights(images_path))

    elif (os.path.isdir(images_path)):
        for class_dir in os.listdir(images_path):
            full_dir_path= os.path.join(images_path, class_dir)
            for img in os.listdir(full_dir_path):
                if (img.endswith(".jpg") or img.endswith(".png")):
                    img_full_path= os.path.join(full_dir_path, img)
                    if hw=="hailo8":
                        imageQueue.put(prepare_image(img_full_path, height, width))
                    else:
                        imageQueue.put(h15_call_cpp_imprint_weights(img_full_path))

    # Send empty image to signal the end of the inference
    if hw=="hailo8":
        imageQueue.put(np.array([]))
    else:
        with isRunningFlag.get_lock():
            isRunningFlag.value= False

# ------------------------------------------------------------ #


# ---------------- Start of the example --------------------- #

if (not args.hef or not args.input_images):
    raise ValueError('You must define hef path and input images path in the command line. Run with -h for additional info')

images_path = args.input_images

image_names= []
image_class_ids= []

if (os.path.isdir(images_path)):
    # Traverse the directory structure and prepare the images for inference
    # Loop that iterates over the classes
    num_new_classes= 0
    min_class_id= sys.maxsize
    class_num_starts_at_zero= False
    for class_dir in os.listdir(images_path):
        # Extract the first character in the class_dir's name until the character '_' is reached
        # In case the class_dir's name is not valid print an error message and exit
        try:
            class_num = int(class_dir.split('.')[0])
            min_class_id= min(min_class_id, class_num)
            
            num_new_classes= num_new_classes + 1
        except:
            raise ValueError('The directory structure is not valid. The directory structure should be as follows: <input-images>/<#NUM#.class-name>/<image-name>.jpg where #NUM# is an integer (0, 1, ...) enumerating the class')
        # Loop that iterates over the images in the class_dir
        full_dir_path= os.path.join(images_path, class_dir)
        for img in os.listdir(full_dir_path):
            if (img.endswith(".jpg") or img.endswith(".png")):
                img_full_path= os.path.join(full_dir_path, img)
                image_class_ids.append(class_num)
                image_names.append(img_full_path)
     
    # Decrement the class id by min_class_id so that the class id will start at 0
    image_class_ids= [class_id - min_class_id for class_id in image_class_ids]
else:
    raise ValueError('You must define input images path to a specific image or to a folder containing images. Run with -h for additional info')        

num_images = len(image_names)

labels = []
with open('imagenet1000_clsidx_to_labels.txt','r') as f:
    labels = eval(f.read()) 

postproc_onnx_path = args.onnx_input
            
onnx_model = onnx.load(postproc_onnx_path)
initializers=onnx_model.graph.initializer
onnx_weights = {}
for initializer in initializers:
    W = numpy_helper.to_array(initializer)
    onnx_weights[initializer.name] = W

num_classes= onnx_weights['fc.weight'].shape[0]
num_features= onnx_weights['fc.weight'].shape[1]
log.info(f"The original model was trained for {num_classes} classes and the feature vectors have {num_features} dimensions")

# To test create_fc_model, uncomment the following lines. The model will be saved to ../onnx/fcTest.onnx and should be identical to the onnx model that was loaded
#onnx_test_model= create_fc_model(onnx_weights['fc.weight'], onnx_weights['fc.bias'], "fcTest")
#onnx.save(onnx_test_model, '../onnx/fcTest.onnx')

if (args.hw == "hailo8"):

    # Create an instance of FeatureExtractor class
    feat_extractor = FeatureExtractor()
    
    # Create an instance of HEF class
    hef = HEF(args.hef) 

    # isRunningFlag is used to signal to the send and recv processes to stop
    isRunningFlag= Value('i', True)
    # numProcessedFrames is used to count the number of frames that were processed
    numProcessedFrames= Value('i', 0)
    # imageQueue is used to pass the images from the main process to the send process
    imageQueue = Queue()
    postQueue = Queue()
    out_queue= Queue()

    with PcieDevice() as target:
            configure_params = ConfigureParams.create_from_hef(hef, interface=HailoStreamInterface.PCIe)
            network_group = target.configure(hef, configure_params)[0]
            network_group_params = network_group.create_params()
        
            [log.info('Input  layer: {:20.20} {}'.format(li.name, li.shape)) for li in hef.get_input_vstream_infos()]
            [log.info('Output layer: {:20.20} {}'.format(li.name, li.shape)) for li in hef.get_output_vstream_infos()]

            height, width, channels = hef.get_input_vstream_infos()[0].shape

            postproc_model = onnxruntime.InferenceSession(postproc_onnx_path)
            
            # mainProcess places the images in the imageQueue
            mainProcess = Process(target=mainProcess, args=(images_path, height, width, imageQueue, args.hw))
            send_process = Process(target=feat_extractor.send, args=(network_group, imageQueue, numProcessedFrames, isRunningFlag))
            recv_process = Process(target=feat_extractor.recv, args=(network_group, postQueue, postproc_model, numProcessedFrames, isRunningFlag))
            post_process = Process(target=feat_extractor.post_processing_imprint, args=(postQueue, image_names, image_class_ids, out_queue, numProcessedFrames, isRunningFlag)) 
            
            start_time = time.time()
            feat_extractor.start_imprint(num_new_classes, num_features)
            mainProcess.start()
            recv_process.start()
            send_process.start()
            post_process.start()
            with network_group.activate(network_group_params):
                mainProcess.join()
                recv_process.join()
                send_process.join()
                post_process.join()

else:
    # Create an instance of FeatureExtractor class
    feat_extractor = FeatureExtractor()
    # isRunningFlag is used to signal to the send and recv processes to stop
    isRunningFlag= Value('i', True)
    # numProcessedFrames is used to count the number of frames that were processed
    numProcessedFrames= Value('i', 0)
    postQueue = Queue()
    out_queue= Queue()
    mainProcess = Process(target=mainProcess, args=(images_path, 0, 0, postQueue, args.hw))
    post_process = Process(target=feat_extractor.post_processing_imprint, args=(postQueue, image_names, image_class_ids, out_queue, numProcessedFrames, isRunningFlag)) 
            
    start_time = time.time()
    feat_extractor.start_imprint(num_new_classes, num_features)
    mainProcess.start()
    post_process.start()
    mainProcess.join()
    post_process.join() 

# Get the new weights from the out_queue
new_weights= out_queue.get()
end_time = time.time()
log.info(f"New weights of shape {new_weights.shape} returned")
log.info(new_weights)
    
# Modify fully connected node of the ONNX model by concatenating the new weights
new_weights= np.concatenate((onnx_weights['fc.weight'], new_weights), axis=0)
new_bias= np.concatenate((onnx_weights['fc.bias'], np.zeros(num_new_classes)), axis=0)  
onnx_weights['fc.weight']= new_weights
onnx_weights['fc.bias']= new_bias

onnx_retrained_model= create_fc_model(onnx_weights['fc.weight'], onnx_weights['fc.bias'], "fcRetrained")
onnx.save(onnx_retrained_model, args.onnx_output)

print('Imprint weight was successful!\n')

# NOTICE: The average FPS can onlt be achieved by a large enough number of frames. The FPS that will be recieved from
# one image does not reflect the average FPS of the model
 
log.info('-------------------------------------')
log.info(' Infer Time:      {:.3f} sec'.format(end_time - start_time))
log.info(' Average FPS:     {:.3f}'.format(num_images/(end_time - start_time)))
log.info('-------------------------------------')







