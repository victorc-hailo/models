# Define class of feature extraction functions send, recv and post_processing

import numpy as np
from hailo_platform import __version__
import onnxruntime
from multiprocessing import Process, Queue, Event, Lock, Value
from queue import Empty
from hailo_platform import (HEF, PcieDevice, HailoStreamInterface, ConfigureParams, InputVStreamParams, OutputVStreamParams, InputVStreams, OutputVStreams, FormatType)
from zenlog import log
import time
from PIL import Image
import os
import argparse
import cv2

class FeatureExtractor:
    def __init__(self):
        self.num_new_classes = 0
        self.num_features = 0
        self.new_weights = None
        self.num_samples_per_class = None
        # Set the timeout for the queue.get() function and Hailo VStream send() and recv() functions
        # If there is a gap between frames that exceeds the timeout, then queue.get() will throw an exception or Hailo device will throw HAILO_TIMEOUT error 
        self.queue_timeout=1 # Second

    def start_imprint(self, num_new_classes, num_features):
        self.num_new_classes = num_new_classes
        self.num_features = num_features
        self.new_weights = np.zeros((num_new_classes, num_features), dtype=np.float32)
        self.num_samples_per_class = np.zeros(num_new_classes, dtype=np.int32)

    def end_imprint(self):
        # Normalize the weights using L2 norm
        for i in range(self.num_new_classes):
            #print(self.new_weights[i])
            self.new_weights[i] = self.new_weights[i]/np.linalg.norm(self.new_weights[i])

    def get_new_weights(self):
        return self.new_weights
        
    def send(self, configured_network, read_q, numProcessedFrames, isRunningFlag):
        vstreams_params = InputVStreamParams.make_from_network_group(configured_network, quantized=True, format_type=FormatType.UINT8)
        configured_network.wait_for_activation(100)
        log.info('\nPerforming inference on input images...\n')
        with InputVStreams(configured_network, vstreams_params) as vstreams:
            # Create dictionary of vstreams to buffers. Since we are passing the image data directly to vstream.send()
            # no need to create any buffer to fill, we can just use the same empty buffer of size [1,1] for all vstreams
            vstream_to_buffer = {vstream: np.empty([1,1]) for vstream in vstreams}
            while (True):
                try:
                    image= read_q.get(timeout= self.queue_timeout)
                except Empty:
                    with isRunningFlag.get_lock():
                        print ("Error send() timed-out, exiting")
                        isRunningFlag.value= False
                        return

                #if image is empty, it means that the main process has finished and we should stop the send process by returning
                if image.size==0:
                    #print("send() exiting")
                    with isRunningFlag.get_lock():
                        isRunningFlag.value= False
                    return
                for vstream, _ in vstream_to_buffer.items():
                    data = np.expand_dims(image, axis=0).astype(np.float32)
                    vstream.send(data)
                # Increment the number of processed frames
                with numProcessedFrames.get_lock():
                    numProcessedFrames.value += 1

                
    def recv(self, configured_network, write_q, postproc_model, numProcessedFrames, isRunningFlag):
        vstreams_params = OutputVStreamParams.make_from_network_group(configured_network, quantized=False, format_type=FormatType.FLOAT32)
        configured_network.wait_for_activation(100)
        with OutputVStreams(configured_network, vstreams_params) as vstreams:
            numFrames= 0
            while (True):
                values = []
                for vstream in vstreams:
                    data = vstream.recv()
                    expanded_tensor = np.expand_dims(data, axis=0)
                    values.append(expanded_tensor)
                    #print the shape of the received tensor
                    #print(expanded_tensor.shape)
                    #print the received tensor
                    #print(expanded_tensor)

                inp = postproc_model.get_inputs()
                curr_vstream_data_dict = {i.name : v for i, v in zip(inp, values)}
                #write_q.put(values)
                write_q.put(curr_vstream_data_dict)
                numFrames+= 1
                # If send() has set isRunningFlag to false and if we have processed all the frames sent by send(), we return
                with isRunningFlag.get_lock():
                    with numProcessedFrames.get_lock():
                        if isRunningFlag.value == False and numProcessedFrames.value == numFrames:
                            #print("recv() exiting ")
                            break

    def post_processing(self, postproc_model, read_q, image_names, labels, numProcessedFrames, isRunningFlag):
        i = 0
        while (True):
            if(read_q.empty() == False):
                #values = read_q.get(0)
                inference_dict = read_q.get(0)
                inference_output = postproc_model.run(None, inference_dict)
                image_name= image_names.pop(0)
                #print(f"{image_name}: {labels[np.argmax(values)]}")
                print(f"{image_name}: {labels[np.argmax(inference_output[0])]}")
                i = i + 1
                # If send() has set isRunningFlag to false and if we have processed all the frames sent by send(), we return
                with isRunningFlag.get_lock():
                    with numProcessedFrames.get_lock():
                        if isRunningFlag.value == False and numProcessedFrames.value == i:
                            #print("post_processing() exiting ")
                            break

    def post_processing_imprint(self, read_q, image_names, image_class_ids, write_q, numProcessedFrames, isRunningFlag):
        i = 0
        while (True):
            if(read_q.empty() == False):
                #values = read_q.get(0)
                inference_dict = read_q.get(0)
                # Get value of the first element in the dictionary
                weights = list(inference_dict.values())[0]
                # Reduce the dimension of the weights array
                weights = weights.squeeze()
                #print(weights.shape)
                image_class_id= image_class_ids.pop(0)
                n_minus_1= self.num_samples_per_class[image_class_id]
                n= n_minus_1 + 1
                self.num_samples_per_class[image_class_id]= n
                # Calculate the average of the weights recursively
                self.new_weights[image_class_id]= (self.new_weights[image_class_id]*n_minus_1 + weights)/n
                #print(weights)
                #print(self.new_weights[image_class_id])
                # Print information about the image and its class id
                image_name= image_names.pop(0)
                print(f"{image_name}: class {image_class_id}")
                i = i + 1
                # If send() has set isRunningFlag to false and if we have processed all the frames sent by send(), we return
                with isRunningFlag.get_lock():
                    with numProcessedFrames.get_lock():
                        if isRunningFlag.value == False and numProcessedFrames.value == i:
                            #print("post_processing_imprint() exiting ")
                            break
        self.end_imprint()
        new_weights= self.get_new_weights()
        write_q.put(new_weights)











