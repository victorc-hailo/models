#!/usr/bin/env python3

import numpy as np
from feat_extractor_class import FeatureExtractor
from hailo_platform import __version__
from multiprocessing import Process, Queue, Event, Lock, Value
from queue import Empty
import onnxruntime
from multiprocessing import Process, Queue
from hailo_platform import (HEF, PcieDevice, HailoStreamInterface, ConfigureParams, InputVStreamParams, OutputVStreamParams, InputVStreams, OutputVStreams, FormatType)
from zenlog import log
import time
from PIL import Image
import os
import argparse
import cv2

# Example of usage: python3 ./infer.py --hef ../hef/resnet_v1_18_featext.hef --onnx ../onnx/resnet_v1_18_fc.onnx --input-images ../data/ > results.txt

parser = argparse.ArgumentParser(description='Running a Hailo inference with OpenCV')
parser.add_argument('--hef', help="HEF file path")
parser.add_argument('--onnx', help="Path of ONNX file implementing the post-processing (ususally fully connected layer), that takes the output of the HEF as input")
parser.add_argument('--input-images', help="Images path to perform inference on. Could be either a single image or a folder containing the images")
parser.add_argument('--labels', help="File containing the labels of the classes. Default is imagenet1000_clsidx_to_labels.txt", default="imagenet1000_clsidx_to_labels.txt")
parser.add_argument('--hw', help="Hardware hailo8 or hailo15", default="hailo8")
args = parser.parse_args()

# ------------------------------------------------------------ #

# ----------------Pre-processing functions ------------------- #

def prepare_image(image_path, height, width):
    raw_image = cv2.imread(image_path)
    image_RGB = cv2.cvtColor(raw_image, cv2.COLOR_BGR2RGB)
    cv2.normalize(image_RGB, image_RGB, 0, 255, norm_type=cv2.NORM_MINMAX)
    image_RGB= cv2.resize(image_RGB, (height, width), interpolation = cv2.INTER_AREA)
    image = np.array(image_RGB, np.uint8)
    return image

def h15_call_cpp_inference(image_path, postproc_model):
    # Call external executable that performs inference on the image usin OS library
    os.system('./classifier_cpp/build/x86_64/classifier -hef={} -path={} -embed={} -verbose=off'.format(args.hef, image_path, "./embeddings.txt"))
    # Read the embeddings from the file
    embeddings = np.loadtxt("./embeddings.txt", dtype=np.float32)
    embeddings = np.expand_dims(embeddings, axis=0)
    # Perform post-processing using the ONNX model
    inp = postproc_model.get_inputs()
    inference_dict = {inp[0].name : embeddings}
    inference_output = postproc_model.run(None, inference_dict)
    # Extract the name of the file in image_path
    image_name = image_path.split("/")[-1]
    print(f"{image_name}: {labels[np.argmax(inference_output[0])]}")
    # print the size of the embeddings
    #print(embeddings.shape)
    # print the embeddings
    #print(embeddings)
    # wait for the user to press enter
    #input("Press Enter to continue...")

    
# ---------------- Inferences threads functions -------------- #

def mainProcess(images_path, height, width, imageQueue, hw):
    
    if (hw!="hailo8"):
        postproc_onnx_path = args.onnx
        postproc_model = onnxruntime.InferenceSession(postproc_onnx_path)
    
    if (images_path.endswith('.jpg') or images_path.endswith('.png')):
        if hw=="hailo8":
            imageQueue.put(prepare_image(images_path, height, width))
        else:
            h15_call_cpp_inference(images_path, postproc_model)

    elif (os.path.isdir(images_path)):
        for img in os.listdir(images_path):
            if (img.endswith(".jpg") or img.endswith(".png")):
                if hw=="hailo8":
                    imageQueue.put(prepare_image(os.path.join(images_path, img), height, width))
                else:
                    h15_call_cpp_inference(os.path.join(images_path, img), postproc_model)
    # Send empty image to signal the end of the inference
    if hw=="hailo8":
        imageQueue.put(np.array([]))
    
# ------------------------------------------------------------ #


# ---------------- Start of the example --------------------- #

if (not args.hef or not args.input_images):
    raise ValueError('You must define hef path and input images path in the command line. Run with -h for additional info')

images_path = args.input_images

image_names= []

if (os.path.isdir(images_path)):
    for img in os.listdir(images_path):
        if (img.endswith(".jpg") or img.endswith(".png")):
            image_names.append(img)
else:
    raise ValueError('You must define input images path to a specific image or to a folder containing images. Run with -h for additional info')        

num_images = len(image_names)

labels = []
with open(args.labels,'r') as f:
    labels = eval(f.read()) 

if (args.hw == "hailo8"):
    # Create an instance of FeatureExtractor class
    feat_extractor = FeatureExtractor()

    # Create an instance of HEF class
    hef = HEF(args.hef)

    # isRunningFlag is used to signal to the send and recv processes to stop
    isRunningFlag= Value('i', True)
    # numProcessedFrames is used to count the number of frames that were processed
    numProcessedFrames= Value('i', 0)
    # imageQueue is used to pass the images from the main process to the send process
    imageQueue = Queue()
    postQueue = Queue()

    with PcieDevice() as target:
            configure_params = ConfigureParams.create_from_hef(hef, interface=HailoStreamInterface.PCIe)
            network_group = target.configure(hef, configure_params)[0]
            network_group_params = network_group.create_params()

            [log.info('Input  layer: {:20.20} {}'.format(li.name, li.shape)) for li in hef.get_input_vstream_infos()]
            [log.info('Output layer: {:20.20} {}'.format(li.name, li.shape)) for li in hef.get_output_vstream_infos()]

            height, width, channels = hef.get_input_vstream_infos()[0].shape

            postproc_onnx_path = args.onnx
            postproc_model = onnxruntime.InferenceSession(postproc_onnx_path)
                    
            # mainProcess places the images in the imageQueue
            mainProcess = Process(target=mainProcess, args=(images_path, height, width, imageQueue, args.hw))
            send_process = Process(target=feat_extractor.send, args=(network_group, imageQueue, numProcessedFrames, isRunningFlag))
            recv_process = Process(target=feat_extractor.recv, args=(network_group, postQueue, postproc_model, numProcessedFrames, isRunningFlag))
            post_process = Process(target=feat_extractor.post_processing, args=(postproc_model, postQueue, image_names, labels, numProcessedFrames, isRunningFlag)) 
            start_time = time.time()
            mainProcess.start()
            
            recv_process.start()
            send_process.start()
            post_process.start()
            with network_group.activate(network_group_params):
                mainProcess.join()
                recv_process.join()
                send_process.join()
                post_process.join()

            end_time = time.time()

else:
    start_time = time.time()
    mainProcess(images_path, 0, 0, None, args.hw)
    end_time = time.time()  

print('Inference was successful!\n')
# NOTICE: The avrage FPS can onlt be achieved by a large enough number of frames. The FPS that will be recieved from
# one image does not reflect the average FPS of the model
 
log.info('-------------------------------------')
log.info(' Infer Time:      {:.3f} sec'.format(end_time - start_time))
log.info(' Average FPS:     {:.3f}'.format(num_images/(end_time - start_time)))
log.info('-------------------------------------')







