#!/usr/bin/env python3

##############################################################################
# This example is a generic Hailo + ONNXRuntime inference classification with logits output example.   
# In order for this example to work properly, please create the relevant 
# post-processing function. 
#
# Usage: 'python inference_generic.py --hef <HEF file> --dir <directory of *.jpg images to infer>
#
# Note that it uses the function os.walk() to look for all jpeg files present in the directory tree.
#
##############################################################################

from locale import normalize
import numpy as np
import onnxruntime
from hailo_platform import __version__
from multiprocessing import Process, Queue, Event, Lock, Value
from queue import Empty
from hailo_platform import (HEF, PcieDevice, HailoStreamInterface, ConfigureParams,
 InputVStreamParams, OutputVStreamParams, InputVStreams, OutputVStreams, FormatType)
from zenlog import log
import time
from PIL import Image, ImageDraw, ImageFont
import os
import re
import argparse
import cv2
import sys
from matplotlib import pyplot as plt

import string

def dprint(string):
  #__builtins__.print("%s" % string)
  pass

def ddprint(string):
  #__builtins__.print("%s" % string)
  pass

QUEUE_TIMEOUT=1 # Second

parser = argparse.ArgumentParser(description='Running a Hailo + ONNXRUntime inference')
parser.add_argument('--hef', help="HEF file path")
parser.add_argument('--dir', help="Directory tree of *.jpg images to infer")
parser.add_argument('--post-process-onnx', help="ONNX file of path, when the output of the HEF is the input of the ONNX")
args = parser.parse_args()


# ---------------- Post-processing functions ----------------- #

def post_processing(inference_output):
    print('Create your relevant post-processing functions')

# ------------------------------------------------------------ #

# ---------------- Inferences threads functions -------------- #

def infer_model_send(network_group, read_q, isRunningFlag,
                startEvent, syncEvent, numCameras):
    # Create input virtual streams params
    # Quantized argument signifies whether or not the incoming data is already quantized.
    # Data is quantized by HailoRT if and only if quantized == False .
    input_vstreams_params = InputVStreamParams.make(network_group, quantized=True, format_type=FormatType.UINT8)
    # Define dataset params
    input_vstream_info = hef.get_input_vstream_infos()[0]

    startEvent.wait()

    # print("How about here? 2")
    with InputVStreams(network_group, input_vstreams_params) as vstreams:
        vstream_to_buffer = {vstream: np.ndarray([1] + list(vstream.shape), dtype=vstream.dtype) for vstream in
                             vstreams}
        # print("How about here? 3")
        while (True):
            # print("I come here for plate_finder")
            #if (read_q.empty() == False):
            for i in range(numCameras):
                try:
                    image= read_q.get(timeout= QUEUE_TIMEOUT)
                except Empty:
                    with isRunningFlag.get_lock():
                        #print("Do I lock here?")
                        if isRunningFlag.value == False:
                            #print("Running flag is false")
                            return
                        else:
                            print ("Error infer_model_send timed-out")
                            isRunningFlag.value= False
                            return

                ddprint("infer_model_send")
                
                network_group.wait_for_activation(100)
                
                data = np.expand_dims(image, axis=0)
                for vstream, _ in vstream_to_buffer.items():
                    vstream.send(data)
                # If last camera, set sync event to signal to the infer_model_rcv thread 
                # that inference output data will be ready soon
                if (i== numCameras-1):
                    syncEvent.set()


def infer_model_recv(network_group, write_q, isRunningFlag, startEvent, syncEvent, numCameras):
   # Create input and output virtual streams params
    # Quantized argument signifies whether or not the incoming data is already quantized.
    # Data is quantized by HailoRT if and only if quantized == False .
    output_vstreams_params = OutputVStreamParams.make(network_group, quantized=False, format_type=FormatType.FLOAT32)

    startEvent.wait()

    # print("How about here? 2")
    with OutputVStreams(network_group, output_vstreams_params) as vstreams:        # print("How about here? 3")
        while (True):
            # print("I come here for plate_finder")
            # Wait from the sync event signal from the infer_model_send() thread
            #if syncEvent.is_set():
            syncEvent.wait(QUEUE_TIMEOUT)
            if syncEvent.is_set():

                network_group.wait_for_activation(100)
                
                for i in range(numCameras):
                    values = []

                    for vstream in vstreams:
                        data= vstream.recv()
                        ddprint("infer_model_recv")
                        #print("******** Infer Image received")
                        #switched_tensor = np.swapaxes(np.swapaxes(np.expand_dims(data, axis=0), 1, 3), 2, 3)  # 1 <--> 2, then 2 <--> 3
                        #values.append(switched_tensor)

                    #values.sort(key=lambda x: x.shape)
                    #inp = post_process_onnx.get_inputs()
                    #inp.sort(key=lambda x: x.shape)
                    #curr_vstream_data_dict = {i.name: v for i, v in zip(inp, values)}
                    ddprint("infer_model_recv q put")
                    write_q.put(data)
                # Clear the sync event so this thread can wait for it and resume when new data is available
                    if (i== numCameras-1):
                        syncEvent.clear()

            #print("******** Infer output sent to queue")
            with isRunningFlag.get_lock():
                if isRunningFlag.value == False:
                    # print("Running flag is false")
                    break

def do_nms(read_q, images, num_images, post_process_onnx):
    i = 0
    while (i < num_images):
        if(read_q.empty() == False):
            inference_dict = read_q.get(0)
            inference_output = post_process_onnx.run(None, inference_dict)
            image = images[i]
            post_processing(inference_output)
            i = i + 1

# ------------------ Main process function-------------------- #

def mainProcess(max_num_pics, picDir, modelImgSize, imageQueue, outQueue, isRunningFlag, startInfer, batchSize):
    
    inference_output=[]

    #img_list = os.listdir(picDir)
    img_list=[]

    for root, dirs, files in os.walk(picDir, topdown=False):
        for name in files:
            file_path= os.path.join(root, name)
            if(os.path.isfile(file_path) and ('.jpg' in file_path)):
                img_list.append(file_path)

    if max_num_pics!=0 :
        img_list = img_list[0:min(max_num_pics, len(img_list))]

    img_list.sort()

    startInfer.set()

    # If number of images is not an exact multiple of batchSize, padd it
    num_img= len(img_list)
    remainder= num_img % batchSize
    if remainder !=0:
        print("Adding dummy images to align the number of images to batchSize")
        for i in range(0, batchSize-remainder):
            img_list.append(img_list[-1])

    num_img= len(img_list)

    counter = 0
    averVal = 0

    for img_idx in range(0, num_img, batchSize):

        batch_img=[]

        for cam_idx in range(batchSize):
            img = Image.open(img_list[img_idx + cam_idx])
            img = img.resize(modelImgSize)
            #plt.imshow(np.array(img, np.uint8), interpolation='nearest')
            img= np.array(img)
            batch_img.append(img.astype(np.uint8))

        batch_img= np.array(batch_img)

        start_time = time.time()

        for img in batch_img:
            data =  img.astype(np.uint8)
            imageQueue.put(data)

        for img in batch_img:
            inference_output.append(outQueue.get())

        counter = counter + 1
        end_time = (time.time() - start_time)/batchSize
        averVal= averVal + end_time
        print(".",end='')
        sys.stdout.flush()
     
    with isRunningFlag.get_lock():
        isRunningFlag.value= False

    averVal= averVal/counter
    print("")

    print("Logits results are:")
    for img_idx in range(0, num_img):
        print("{0}: {1}".format(img_list[img_idx], inference_output[img_idx][0]))

    print("------> Average fps is: %5.1f fps ---" % (1/averVal))

    #plt.show() 
# ----------------------------------------------------------- #


# ---------------- Start of the example --------------------- #


max_num_pics = 0
batchSize= 1
pic_dir = "./calib_data"

startInfer= Event()
signalToRecv= Event()
isRunningFlag= Value('i', True)
imageQueue = Queue()
outQueue = Queue()

if (not args.hef):
    raise ValueError('You must pass a hef path in the command line. Run with -h for additional info')

if (not args.dir):
    raise ValueError('You must pass the path to the directory tree that contains all the *.jpg images to infer. Run with -h for additional info')

if (args.post_process_onnx):
    post_process_onnx_path = args.post_process_onnx
    post_process_onnx = onnxruntime.InferenceSession(post_process_onnx_path)
    outProcQueue= Queue()

hef = HEF(args.hef)

input_vstream_info = hef.get_input_vstream_infos()[0]
width= input_vstream_info.shape[1]
height= input_vstream_info.shape[0]
modelImgSize=[width, height]

with PcieDevice() as target:
        configure_params = ConfigureParams.create_from_hef(hef, interface=HailoStreamInterface.PCIe)
        keys = list(configure_params)
        configure_params[keys[0]].batch_size= batchSize
        network_group = target.configure(hef, configure_params)[0]
        network_group_params = network_group.create_params()
        
        # Note: If you need to normalize the image, choose and change the set_resized_input function to right values
        # resized_images = [set_resized_input(lambda size: image.resize(size, Image.Resampling.LANCZOS)) for image in images]
        # Main Process
        main_process = Process(target=mainProcess, args=(max_num_pics, pic_dir, modelImgSize, imageQueue, outQueue, isRunningFlag, startInfer, batchSize))
        send_process = Process(target=infer_model_send, args=(network_group, imageQueue, isRunningFlag, startInfer, signalToRecv, batchSize))
        recv_process = Process(target=infer_model_recv, args=(network_group, outQueue, isRunningFlag, startInfer, signalToRecv, batchSize))
        if (args.post_process_onnx):
            nms_process = Process(target=do_nms, args=(outProcQueue, post_process_onnx, batchSize)) 

        main_process.start()
        recv_process.start()
        send_process.start()
        if (args.post_process_onnx):
            nms_process.start()

        with network_group.activate(network_group_params):
            send_process.join()
            dprint("Infer send process exited")
            recv_process.join()
            dprint("Infer recv process exited")
            if (args.post_process_onnx):
                nms_process.join()
            main_process.join()
            dprint("main process exited")
            




