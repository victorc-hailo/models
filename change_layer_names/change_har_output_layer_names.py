#!/usr/bin/env python3

# Code inspired from https://hailotech.atlassian.net/wiki/spaces/~103305243/pages/1094615041/Change+HN+layers

from hailo_sdk_client import ClientRunner

import argparse
import ast

parser = argparse.ArgumentParser(description='Changing layer names in Har file using names mapping defined in a file')
parser.add_argument('--src_har', help="Source HAR file path")
parser.add_argument('--names_map', help="File path to layer names mapping from old to new names")
parser.add_argument('--dst_har', help="Destination HAR file path")
args = parser.parse_args()

if (not args.src_har):
    raise ValueError('You must define Source HAR path')

if (not args.names_map):
    raise ValueError('You must path to layer name files')

if (not args.dst_har):
    raise ValueError('You must define Destination HAR path')

with open(args.names_map, 'r') as f:
    old2newName_map= ast.literal_eval( f.read( ) )

#print(old2newName_map)

runner = ClientRunner(hw_arch='hailo8', har_path=args.src_har)

hn = runner.get_hn()

hn_layers = hn['layers']
#print(hn_layers.items())

#print("\n\n\n\n=========================\n\n\n\n")

items = list(hn_layers.items())
new_items=[]

for i in range(len(items)):

    # If not output layer then we check if is an earlier layer that refers to an output layer present in the list old2newName_map
    # If yes, then we need to replace the content of the field 'output' with the new name.
    if items[i][1]['type']!='output_layer':
        for j in range(len(old2newName_map)):
                if items[i][1]['output'][0]== old2newName_map[j][0]:
                    newName= old2newName_map[j][1]
                    print("Layer {0}: changed the output layer field from {1} to {2}".format(items[i][0], old2newName_map[j][0], newName))
                    items[i][1]['output'][0]= newName

    # If output layer and name matches the name in the list old2newName_map then change name of the layer
    else:
        for j in range(len(old2newName_map)):
            if items[i][0]==old2newName_map[j][0]:
                newName= old2newName_map[j][1]
                print("Layer {0} changed to {1}".format(old2newName_map[j][0], newName))
                #Since tuple is immutable, convert to list first
                item_list= list(items[i])
                item_list[0]= newName
                #print(item_list)
                #Convert back to tuple before writing
                items[i]= tuple(item_list)

    new_items.append(items[i])

new_layers = dict(new_items)
hn['layers'] = new_layers

#hn_layers = hn['layers']
#print(hn_layers.items())

runner.set_hn(hn)
runner.save_har(args.dst_har)