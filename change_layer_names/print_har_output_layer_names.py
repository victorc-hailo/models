#!/usr/bin/env python3

from hailo_sdk_client import ClientRunner

import argparse

parser = argparse.ArgumentParser(description='Print layer name <-> original names maping into a file')
parser.add_argument('--har', help="HAR file path")
parser.add_argument('--names_map', help="Output file path to layer names_map file that maps layer name to their original name")

args = parser.parse_args()

if (not args.har):
    raise ValueError('You must define HAR path')

if (not args.names_map):
    raise ValueError('You must define an output path')

runner = ClientRunner(hw_arch='hailo8', har_path=args.har)

hn = runner.get_hn()
#npz = runner.get_params()

hn_layers = hn['layers']
items = list(hn_layers.items())

out_list=[]
for i in range(len(items)):
    if items[i][1]['type']=='output_layer':
        #print (items[i][1])
        layer_name=items[i][1]['input'][0]
        #Extract the first part of the old name, which is the context name
        split_name = layer_name.split('/')
        context_name= split_name[0]
        orig_layer_name= items[i][1]['original_names'][0]
        origName= context_name + '/' + orig_layer_name
        out_list.append(tuple((layer_name, origName)))
        #print('({0}, {1}), '.format(layer_name, orig_layer_name), end="")

with open(args.names_map, 'w') as f:
    print(out_list, file=f)