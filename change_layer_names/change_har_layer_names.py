#!/usr/bin/env python3

# Code inspired from https://hailotech.atlassian.net/wiki/spaces/~103305243/pages/1094615041/Change+HN+layers

from hailo_sdk_client import ClientRunner

import argparse
import ast

parser = argparse.ArgumentParser(description='Changing layer names in Har file using names mapping defined in a file')
parser.add_argument('--src_har', help="Source HAR file path")
parser.add_argument('--names_map', help="File path to layer names mapping from old to new names")
parser.add_argument('--dst_har', help="Destination HAR file path")
args = parser.parse_args()

if (not args.src_har):
    raise ValueError('You must define Source HAR path')

if (not args.names_map):
    raise ValueError('You must path to layer name files')

if (not args.dst_har):
    raise ValueError('You must define Destination HAR path')

with open(args.names_map, 'r') as f:
    old2newName_map= ast.literal_eval( f.read( ) )

map_list= dict(old2newName_map)

#print(old2newName_map)

runner = ClientRunner(hw_arch='hailo8', har_path=args.src_har)

hn = runner.get_hn()
npz = runner.get_params()

hn_layers = hn['layers']
#print(hn_layers.items())

#print("\n\n\n\n=========================\n\n\n\n")

items = list(hn_layers.items())
new_items=[]
npz=dict(npz) # weights dictionary
list_keys= list(npz.keys())
for i in range(len(items)):

    # Iterate through old2newName_map
    # If layer name matches the name in the list old2newName_map then change name of the layer and in the npz weight dictionay, replace the corresponding layer's key name
    # If output layer of that layer matches the name in the list old2newName_map then change output layer name
    # If input layer of that layer matches the name in the list old2newName_map then change input layer name
    for j in range(len(old2newName_map)):
        if items[i][0]==old2newName_map[j][0]:
            newName= old2newName_map[j][1]
            print("Layer {0} changed to {1}".format(old2newName_map[j][0], newName))
            #Since tuple is immutable, convert to list first
            item_list= list(items[i])
            item_list[0]= newName
            #print(item_list)
            #Convert back to tuple before writing
            items[i]= tuple(item_list)
            # In the npz weight dictionay, look for any entry whose key contains the corresponding layer's name and replace the key with the new name
            for k in range(len(list_keys)):
                if old2newName_map[j][0] in list_keys[k]:
                    old_key_name= list_keys[k]
                    new_key_name= old_key_name.replace(old2newName_map[j][0], old2newName_map[j][1])  
                    npz[new_key_name]= npz.pop(old_key_name)
                    print("In npz file, replaced {0} with {1}".format(old_key_name, new_key_name))
            break # break the 'for j' loop since we have found a name match

        if items[i][1]['output']!=[]:
            for o_idx in range(len(items[i][1]['output'])):
                if items[i][1]['output'][o_idx]== old2newName_map[j][0]:
                    newName= old2newName_map[j][1]
                    print("Layer {0}: changed the output layer field from {1} to {2}".format(items[i][0], old2newName_map[j][0], newName))
                    items[i][1]['output'][o_idx]= newName
                    break # break the 'for j' loop since we have found a name match

        if items[i][1]['input']!=[]:
            for i_idx in range(len(items[i][1]['input'])):
                if items[i][1]['input'][i_idx]== old2newName_map[j][0]:
                    newName= old2newName_map[j][1]
                    print("Layer {0}: changed the input layer field from {1} to {2}".format(items[i][0], old2newName_map[j][0], newName))
                    items[i][1]['input'][i_idx]= newName
                    break # break the 'for j' loop since we have found a name match

    new_items.append(items[i])

new_layers = dict(new_items)
hn['layers'] = new_layers

print("\nChanged hn['net_params']['output_layers_order'] from:\n")
print(hn["net_params"]["output_layers_order"])
hn["net_params"]["output_layers_order"] = list(map(dict(map_list).get, hn["net_params"]["output_layers_order"]))
print("\nto:\n")
print(hn["net_params"]["output_layers_order"])
print("\n")

#hn_layers = hn['layers']
#print(hn_layers.items())

runner.set_hn(hn)

runner.load_params(npz)
runner.save_har(args.dst_har)